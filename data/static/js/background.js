// Open up a port to the Native application provided by LiveMarkup.
var port = chrome.runtime.connectNative('com.livetechnology.livemarkup_chromesnip');
port.onMessage.addListener(function(msg) {
  // console.log("Received" + msg);
});
port.onDisconnect.addListener(function() {
  // console.log("Disconnected");
});

// Send the start up message so the app knows we are ready.
port.postMessage({ text: "Chrome Startup." });

// Send updates whenever the window focus or tab focus changes.
chrome.windows.onFocusChanged.addListener(function() 
{
    chrome.tabs.query({currentWindow: true, active: true}, function(tab)
    {
        URLChanged(tab[0].url)
    }); 
});

// This is called when a chrome tab is activated.
chrome.tabs.onActivated.addListener(
    function (activeInfo) {
            chrome.tabs.get(activeInfo.tabId, function(tab){
                URLChanged(tab.url); 

        });     
    }
);

// This is called when a tab is updated.
chrome.tabs.onUpdated.addListener(
    function checkHosts(tabId, changeInfo, tab) {
        URLChanged(tab.url); 
}
);

// this function posts the message to the native interface
function URLChanged(url){
	port.postMessage({url:url});
}

chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
	var vers = request.versions;		
	if(vers == 'openlogin'){				
		chrome.tabs.create({ url:'data/popup.html'});
		localStorage._ispopuplogin  = 1;
	}else if(vers == 'logout'){				
		localStorage._dri  = '';
		localStorage._code = '';
		localStorage._name = '';
	} else{		
		vers = vers.split('~');
		localStorage._stfit_ver  = vers[0];
		localStorage._stfit_fver = vers[1];
		localStorage._stfit_typ  = vers[2];
		localStorage._stfit_dom  = vers[3];
	}
});

chrome.tabs.onUpdated.addListener(function(tabId, changeInfo, tab){	
    if(changeInfo.status == "loading") { 			
	   if(tab.url.match('//livestuff.com/#_=_') || tab.url.match('//prerelease.livestuff.com/#_=_') || tab.url.match('//qa.livestuff.com/#_=_') || tab.url.match('//dev.livestuff.com/#_=_') || tab.url.match('//livestuff.com/#') || tab.url.match('//prerelease.livestuff.com/#') || tab.url.match('//qa.livestuff.com/#') || tab.url.match('//dev.livestuff.com/#')){
			chrome.tabs.remove(tab.id, function() {}); 												
	   }
    }	 
	if (changeInfo.status == 'complete') {					
		getSession();
	}	
});
chrome.tabs.onActivated.addListener(function(evt){ 
    chrome.tabs.get(evt.tabId, function(tab){ 			
		getSession();
	}); 
});
function getSession(){		
	if(localStorage['_stfit_dom']){
		var _url = "https://"+localStorage['_stfit_dom']+"/SessionExport.do";			
	}else{
		var _url = "https://livestuff.com/SessionExport.do";		
	}
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (xhttp.readyState == 4 && xhttp.status == 200) {			 
			var data		= JSON.parse(xhttp.responseText);
			var logins 		= '';
			logins 			= data.Result;			
			if(logins){
				
				localStorage._dri  = logins["Direct Resource Identifier"];
				localStorage._code = logins["ObjectCode"];
				localStorage._name = logins["Formatted Name"];				
			
			} else {
				localStorage._dri  = '';
				localStorage._code = '';
				localStorage._name = '';				
			}
		}
	};
	xhttp.open("GET", _url, true);
	xhttp.send();  	
}
	 