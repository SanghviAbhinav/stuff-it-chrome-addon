(function() {
    var graph = 'https://graph.facebook.com/',
        app_id = false,
        scope = '';
    
    var FB = {
        _uid: '',
        init: function(id, perms) {
            app_id = id;
            scope = perms || '';
        },
        getUser: function(fn) {
            /* Get user ID immediately */
            FB.api('/me', function(res) {
                if(res && res.id) {
                    localStorage._uid = res.id;
					localStorage._uname = res.name;
					localStorage._uemail = res.email;
                    FB._uid = res.id;
                    fn && fn(res);
                }
            });
        },
        /*getMissingPermissions: function(fn) {
            var self = this, missing = [];
            FB.api('me/permissions', function(res) {
                if(res && !res.error && res.data && res.data[0]) {
                    var perms = scope.replace(/\s/g, '').split(','), i = 0;
                    for(;i< perms.length;i++) {
                        if(!(perms[i] in res.data[0])) {
                            if(!(perms[i] in {'friends_checkins': 1, 'user_checkins': 1, 'offline_access': 1})) {
                                missing.push(perms[i]);
                            }
                        }
                    }
                    fn && fn(missing);
                }
            });
        },*/
        getAccessToken: function() {
            return localStorage._fbtoken || false;
        },
        setAccessToken: function(token) {
            if(token === false) {
                localStorage.removeItem('_fbtoken');
            } else {
                localStorage._fbtoken = token;
            }
        },
        getUserID: function() {
            return localStorage._uid || FB._uid;
        },
        login: function() {
            var url = graph + "oauth/authorize?client_id=" + app_id + "&redirect_uri=https://www.facebook.com/connect/login_success.html&type=user_agent&display=page&scope=" + scope;
				window.open(url, 'fbauth');
        },
        api: function(path ) {
            var args = Array.prototype.slice.call(arguments, 1),
                fn = false,
                params = {},
                method = 'get';
            
            for(var i in args) {
                switch(typeof args[i]) {
                    case 'function':
                        fn = args[i];
                    break;
                    case 'object':
                        params = args[i];
                    break;
                    case 'string':
                        method = args[i].toLowerCase();
                    break;
                }
            }
            
            /* Make sure there is a path component */
            if(!path && !params.batch) {
                return fn && fn(false);
            }
            
            /* Use the passed method i.e. get, post, delete */
            params.method = method;
            params.access_token = this.getAccessToken();
            
            /* Make call */
            $.get(graph + path, params, function(res) {
                /* If there is an auth error, don't continue and make them login */
                if(res && res.error && res.error.type && (res.error.message == 'Invalid OAuth access token.' || res.error.message.indexOf('Error validating access token') > -1)) {
                    FB.login();
                    return;
                }
                if(typeof fn == 'function') {
                    fn(res);
                }
            }, 'json');
        },
        objectFromURL: function(url) {
            if(url.indexOf('facebook.com') == -1) {
                return url;
            }
            
            
        }
    };
    
    window.FB = FB;

})();