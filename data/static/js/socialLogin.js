(function() {
    var graph = 'https://graph.facebook.com/',
        app_id = false,
        scope = '';
	
    var FB = {
        _uid: '',
        init: function(id, perms) {
            app_id = id;
            scope = perms || '';
        },
        getUser: function(fn) {
            /* Get user ID immediately */
            FB.api('/me', function(res) {
                if(res && res.id) {
                    localStorage._uid = res.id;
					localStorage._uname = res.name;
					localStorage._uemail = res.email;
                    FB._uid = res.id;
                    fn && fn(res);
                }
            });
        },
        getAccessToken: function() {
            return localStorage._fbtoken || false;
        },
        setAccessToken: function(token) {
            if(token === false) {
                localStorage.removeItem('_fbtoken');
            } else {
                localStorage._fbtoken = token;
            }
        },
        getUserID: function() {
            return localStorage._uid || FB._uid;
        },
        login: function() {
            var url = graph + "oauth/authorize?client_id=" + app_id + "&redirect_uri=https://www.facebook.com/connect/login_success.html&type=user_agent&display=page&scope=" + scope;
				window.open(url, 'fbauth');
        },
        api: function(path ) {
            var args = Array.prototype.slice.call(arguments, 1),
                fn = false,
                params = {},
                method = 'get';
            
            for(var i in args) {
                switch(typeof args[i]) {
                    case 'function':
                        fn = args[i];
                    break;
                    case 'object':
                        params = args[i];
                    break;
                    case 'string':
                        method = args[i].toLowerCase();
                    break;
                }
            }
            
            /* Make sure there is a path component */
            if(!path && !params.batch) {
                return fn && fn(false);
            }
            
            /* Use the passed method i.e. get, post, delete */
            params.method = method;
            params.access_token = this.getAccessToken();
            
            /* Make call */
            $.get(graph + path, params, function(res) {
                /* If there is an auth error, don't continue and make them login */
                if(res && res.error && res.error.type && (res.error.message == 'Invalid OAuth access token.' || res.error.message.indexOf('Error validating access token') > -1)) {
                    FB.login();
                    return;
                }
                if(typeof fn == 'function') {
                    fn(res);
                }
            }, 'json');
        },
        objectFromURL: function(url) {
            if(url.indexOf('facebook.com') == -1) {
                return url;
            }
        }
    };
	
    window.FB = FB;

	/* BELOW CODE IS FOR GOOGLE PLUS LOGIN */
	var graphGoogle = 'https://accounts.google.com/',
        app_idGoogle = false,
        scopeGoogle = '';
		
	var GP = {
		_uid: '',
		init: function(id, params){
			app_idGoogle = id;
            scopeGoogle = params || '';
		},
		getUser: function(fn) {
            /* Get user ID immediately */
            
        },
		login: function() {			
			/*-@vv-google + login user data---old-*/
			chrome.identity.getAuthToken({'interactive':true}, function(token){
				//console.log("59 - ", token);				
				GP.setAccessToken(token);				
                $.get("https://www.googleapis.com/oauth2/v1/userinfo?access_token="+ token,{}, function(res) {
					chrome.identity.getProfileUserInfo(function (userInfo){						
						localStorage._uemail = userInfo.email;
					});					
					if(res && res.id) {
						localStorage._uid = res.id;
						localStorage._uname = res.name;						
						GP._uid = res.id;						
					}
                     setTimeout(function(){checkCookie();},500);					
                 }, 'json');
			  });          			  
		},
		getAccessToken: function() {
            return localStorage._gptoken || false;
        },
        setAccessToken: function(token) {
            if(token === false) {
                localStorage.removeItem('_gptoken');
            } else {
                localStorage._gptoken = token;
            }
        },
		signOut: function(){
			chrome.identity.removeCachedAuthToken({'token':localStorage._gptoken},function(){
				console.log("Signed out.");
			});
		},
        getUserID: function() {
            return localStorage._uid || GP._uid;
        },
        api: function(path ) {
            var args = Array.prototype.slice.call(arguments, 1),
                fn = false,
                params = {},
                method = 'get';
            
            for(var i in args) {
                switch(typeof args[i]) {
                    case 'function':
                        fn = args[i];
                    break;
                    case 'object':
                        params = args[i];
                    break;
                    case 'string':
                        method = args[i].toLowerCase();
                    break;
                }
            }
            
            /* Make sure there is a path component */
            if(!path && !params.batch) {
                return fn && fn(false);
            }
            
            /* Use the passed method i.e. get, post, delete */
            params.method = method;
            params.access_token = this.getAccessToken();
            
            /* Make call */
            $.get(graph + path, params, function(res) {
                /* If there is an auth error, don't continue and make them login */
                if(res && res.error && res.error.type && (res.error.message == 'Invalid OAuth access token.' || res.error.message.indexOf('Error validating access token') > -1)) {
                    FB.login();
                    return;
                }
                if(typeof fn == 'function') {
                    fn(res);
                }
            }, 'json');
        },
        objectFromURL: function(url) {
            if(url.indexOf('facebook.com') == -1) {
                return url;
            }
            
            
        }
	};
	window.GP = GP;	
})();