var dom = {}, events = {}, cache = {}, logger = {}, utils = {}, ajax = {}, LO = {}, modal = {};


// convenience utilities
utils.noop = function() {};

utils.forEach = Function.prototype.call.bind(Array.prototype.forEach);

utils.slice = Function.prototype.call.bind(Array.prototype.slice);

utils.objHasProp = function(obj, prop) {
  return {}.hasOwnProperty.call(obj, prop);
};

utils.copy = function(obj, simple) {
  if (typeof obj !== 'object') return obj;

  var o = {},
      k;

  if (simple === false) {
    for (k in obj) {
      if (obj[k]) {
        if (Array.isArray(obj[k])) {
          o[k] = obj[k].map(function(el) {
            return utils.copy(el, false);
          });
        } else {
          o[k] = utils.copy(obj[k], false);
        }
      } else {
        o[k] = obj[k];
      }
    }
  } else {
    o = JSON.parse(JSON.stringify(obj));
  }

  return o;
};

utils.extend = function(obj) {
  // function can take any number of objects to extend the original object by
  obj = obj || {};

  var key;

  utils.slice(arguments, 1).forEach(function (dataObj) {
    if (typeof dataObj === 'object') {
      for (key in dataObj) {
        if (utils.objHasProp(dataObj, key)) obj[key] = dataObj[key];
      }
    }
  });

  return obj;
};

utils.objectToArray = function(obj) {
  var arr = [],
      key;

  for (key in obj) {
    arr.push(obj[key]);
  }

  return arr;
};

utils._UNIX_START = 25569;
utils._MS_PER_DAY = 86400000;

utils.unixToFloat = function(ms, offset) {
  offset = 0;
  
  ms = ms - offset;
  
  var d = Math.floor(ms / utils._MS_PER_DAY),
      t, strFloatDate;

  ms -= d * utils._MS_PER_DAY;
  d += utils._UNIX_START;

  t = (ms / utils._MS_PER_DAY).toPrecision(20);
  strFloatDate = d.toString();

  if (parseFloat(t) > 0) strFloatDate += '.' + t.substring(2);

  return strFloatDate;
};

utils.floatToUnix = function(strFloatDate, offset) {
    offset = 0;
    
    var parts = strFloatDate.split('.'),
        d = parseInt(parts[0], 10),
        t = parseFloat(parts[1] ? '0.' + parts[1] : '0');

    d -= utils._UNIX_START;

    return ((d * utils._MS_PER_DAY) + (t * utils._MS_PER_DAY)) + offset;
};

utils.formatNumWithCommas = function(n) {
  return n.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
};

utils._typeConversionFuncs = {
  array: function (val) {
    return [val];
  },
  boolean: function (val) {
    return val == 'true';
  },
  'function': function (val) {
    return function () { return val; };
  },
  'null': function () {
    return null;
  },
  number: function (val) {
    return Number(val);
  },
  object: function (val) {
    return new Object(val);
  },
  string: function (val) {
    return val + '';
  },
  'undefined': function () {
    return undefined;
  }
};

utils.convertType = function(val, to) {
  return utils._typeConversionFuncs[to](val);
};

utils.sanitizeDRI = function(dri){
  var hostArr = location.host.split('.'),
      rx = new RegExp(hostArr[0], 'i');
		
  // TEMP: remove once OQL returns proper @DRI results
  if (location.host.indexOf('livestuff') === -1) dri = dri.replace(rx, '');
    
  if (dri.indexOf('/') !== 0 && dri.indexOf('http') === -1) dri = '/' + dri;

  return dri;
};



/*
 * showLoader - shows loading gif 
 *
 */
utils.showLoader = function() { 
  var el = document.body,
      overlay = dom.getByClass('loader__overlay', el)[0],
      spinner = dom.getByClass('loader__spinner', el)[0];
 
  if (!overlay) {
    dom.create('div', el, { className: 'loader__overlay' });
    dom.create('div', el, { className: 'loader__spinner' });
  } else {
    dom.removeClass(overlay, '-hidden');
    dom.removeClass(spinner, '-hidden');
  }
};
 
utils.hideLoader = function() {
  var el = document.body,
      overlay = dom.getByClass('loader__overlay', el)[0],
      spinner = dom.getByClass('loader__spinner', el)[0];
 
  if (overlay) dom.addClass(overlay, '-hidden');
  if (spinner) dom.addClass(spinner, '-hidden');
};


// create and init default caches
// method to create a simple map for caching/persisting data
cache.create = function(persist, ns) {
  var c = Object.create(null);

  c.items = Object.create(null);
  c.count = 0;
  c.keys = [];

  c.persist = persist;
  if (persist) c.ns = ns ? 'LT.cache.' + ns + '.' : 'LT.cache._.';

  c.get = function(key) {
    return this.items[key];
  };

  c.set = function(key, value) {
    if (value === undefined) return false;

    if (this.items[key] === undefined) this.keys.push(key);

    if (this.persist) {
      window.localStorage[this.ns + key] = JSON.stringify(value);
      window.localStorage[this.ns + '_keys'] = JSON.stringify(this.keys);
    }

    this.items[key] = value;
    this.count++;

    return true;
  };

  c.remove = function(key) {
    if (this.items[key] === undefined) return false;

    this.keys = this.keys.filter(function(el) { return el !== key; });

    if (this.persist) {
      delete window.localStorage[this.ns + key];
      window.localStorage[this.ns + '_keys'] = JSON.stringify(this.keys);
    }

    delete this.items[key];
    this.count--;

    return true;
  };

  c.clear = function() {
    if (persist) {
      this.keys.forEach(function(key) {
        delete window.localStorage[this.ns + key];
      });

      delete window.localStorage[this.ns + '_keys'];
    }

    this.items = Object.create(null);
    this.count = 0;
    this.keys = [];
  };

  c.size = function() {
    return this.count = this.keys.length;
  };

  // if persist, attempt to rebuild the cache
  if (persist) {
    var ls = window.localStorage[c.ns + '_keys'],
        keys = ls ? JSON.parse(ls) : null;

    if (keys) {
      keys.forEach(function(key) {
        c.set(key, JSON.parse(window.localStorage[c.ns + key]));
      });
    }
  }

  return c;
};

// create DOM caches
cache.nodes = Object.create(null);
cache.nodes.els = cache.create();
cache.nodes.lists = cache.create();

// create widget caches
cache.widgets = Object.create(null);
cache.widgets.calendars = cache.create();
cache.widgets.forms = cache.create(true, 'flds');

// create AJAX cache
cache.requests = cache.create();

// create LT cache
cache.LT = cache.create(true, 'lt');

// create FormBuilder cache (this should be TEMP, remove once dev complete)
cache.formBuilder = Object.create(null);
cache.formBuilder.forms = cache.create(true, 'form');
cache.formBuilder.fields = cache.create(true, 'flds');


// DOM library
// Create data warehouse on element from all attributes
dom._initElementData = function(el) {
  if (el._init) return;

  var elAttrs = el.attributes,
      attrs = Object.create(null),
      data = Object.create(null),
      k, v, i;

  for(i = 0; i < elAttrs.length; i++) {
    k = elAttrs[i].name;
    v = elAttrs[i].value;

    if (v === 'true') v = true;
    if (v === 'false') v = false;

    if (k.indexOf('data-') >= 0) {
      k = k.replace('data-','');
      data[k] = v;
    } else {
      attrs[k] = v;
    }
  }

  if (el.id) cache.nodes.els.set('#' + el.id, el);

  el._attrs = attrs;
  el._data = data;
  el._events = [];
  el._subscriptions = [];
  el._init = true;
};

dom._initElementsData = function(els) {
  els.forEach(function(el) {
    dom._initElementData(el);
  });
};

// Methods to fetch elements
dom._get = function(selector, root, type, key, fn) {
  var el;

  if (root && root === document) el = cache.nodes[type].get(key);

  if (!el) {
    root = root || document;
    el = root[fn](selector);
    if (el) {
      if (type === 'els') {
        dom._initElementData(el);
      } else {
        el = utils.slice(el);
        dom._initElementsData(el);
      }

      if (root === document) cache.nodes[type].set(key, el);
    }
  }

  return el;
};

dom.getById = function(id, root) {
  return dom._get(id, root, 'els', '#' + id, 'getElementById');
};

dom.getByClass = function(className, root) {
  return dom._get(className, root, 'lists', '.' + className, 'getElementsByClassName');
};

dom.getByTag = function(tag, root) {
  return dom._get(tag, root, 'lists', tag, 'getElementsByTagName');
};

dom.getEl = function(selector, root) {
  return dom._get(selector, root, 'els', selector, 'querySelector');
};

dom.getEls = function(selector, root) {
  return dom._get(selector, root, 'lists', selector, 'querySelectorAll');
};


// Testing methods
dom.previousSibling = function(el) {
  var prev = el.previousSibling;

  while(prev && prev.nodeType != 1) {
    prev = prev.previousSibling;
  }

  return prev;
};

dom.nextSibling = function(el) {
  var next = el.nextSibling;

  while(next && next.nodeType != 1) {
    next = next.nextSibling;
  }

  return next;
};

dom.isInTree = function(el, ancestor, ceiling) {
  ceiling = ceiling || document;
  
  while (el && el !== ceiling) {
    if (el === ancestor) return true;
    el = el.parentNode;
  }

  return false;
};

dom.findInTree = function(el, ancestor, selector, ceiling) {
  if (!selector) return null;

  ancestor = ancestor || ceiling || document;

  var match = dom.getEls(selector, ancestor).filter(function(res) {
    return dom.isInTree(el, res, ancestor);
  });

  if (match.length) return match[0];

  return null;
}


// DOM-manipulation functions
dom._create = function(el, parent, opts) {
  if (opts) {
    var prop;

    for (prop in opts) {
      if (opts[prop] && typeof opts[prop] !== 'object') {
        if (prop.indexOf('-') >= 0) {
          el.setAttribute(prop, opts[prop]);
        } else {
          el[prop] = opts[prop];
        }
      } else if (typeof opts[prop] === 'object' && opts[prop].ns && opts[prop].attr && opts[prop].value) {
        el.setAttributeNS(opts[prop].ns, opts[prop].attr, opts[prop].value);
      }
    }
  }

  dom._initElementData(el);

  if (parent) dom.append(parent, el);

  if (opts && opts.events) events.add(el, opts.events);

  return el;
};

dom.create = function(type, parent, opts) {
  return dom._create(document.createElement(type), parent, opts);
};

dom.createNS = function(ns, type, parent, opts) {
  return dom._create(document.createElementNS(ns, type), parent, opts);
};

dom.createSVG = function(parent, ID, opts) {
  var svg = dom.createNS('http://www.w3.org/2000/svg', 'svg', parent, opts);
  dom.addClass(svg, 'icon');

  dom.createNS('http://www.w3.org/2000/svg', 'use', svg, {
    xlink: {
      ns: 'http://www.w3.org/1999/xlink',
      attr: 'xlink:href',
      value: '#' + ID
    }
  });

  return svg;
};

dom.append = function(parent, child) {
  parent.appendChild(child);
};

dom.insertBefore = function(newNode, target) {
  if (!target) target = null;
  if (target) target.parentNode.insertBefore(newNode, target);
};

dom.insertAfter = function(newNode, _target) {
  var target;

  if (!_target || (_target && !_target.nextSibling)) target = null;
  if (_target) target = _target.nextSibling;

  if (target) {
    target.parentNode.insertBefore(newNode, target);
  } else {
    dom.append(_target.parentNode, newNode);
  }
};

dom.remove = function(child) {
  if (!child) return false;

  var parent = child.parentNode;
  if (!parent) return false;

  if (parent.id) cache.nodes.els.remove('#' + parent.id);
  if (child.id) cache.nodes.els.remove('#' + child.id);

  return parent.removeChild(child);
};

dom.removeChildren = function(parent) {
  while (parent.firstChild) {
    dom.remove(parent.firstChild);
  }
};

dom.removeSiblings = function(el, target) {
  // target is a string, valid values are 'all', 'before', 'after'
  target = target || 'all';
  if (target === 'all' || target === 'before') {
    while (el.previousSibling) {
      dom.remove(el.previousSibling);
    }
  }
  if (target === 'all' || target === 'after') {
    while (el.nextSibling) {
      dom.remove(el.nextSibling);
    }
  }
};

dom.copyAttrs = function(source, target) {
  var attrs = source.attributes,
      len = attrs.length,
      i;

  for(i = 0; i < len; i++) {
    target.setAttribute(attrs[i].name, attrs[i].value);
  }
};

dom.moveChildren = function(source, target) {
  while (source.firstChild) {
    target.appendChild(source.firstChild);
  }
};

dom.replace = function(source, target, opts) {
  opts = opts || Object.create(null);

  if (!target && opts.tagName) target = dom.create(opts.tagName);

  if (!target) {
    logger.error('dom.copy error: You must supply either a target object or a valid tagName in the options object.');
    return;
  }

  if (!opts.noAttrs) dom.copyAttrs(source, target);
  if (!opts.noChildren && source.firstChild) dom.moveChildren(source, target);

  source.parentNode.replaceChild(target, source);

  dom._initElementData(target);
};

dom.addText = function(el, text) {
  dom.append(el, document.createTextNode(text));
};

dom.smoothScroll = function(el, targetPos, duration) {
  targetPos = Math.round(targetPos);
  duration = Math.round(duration);

  if (duration < 0) return Promise.reject('bad duration');

  if (duration == 0) {
    element.scrollTop = targetPos;
    return Promise.resolve();
  }

  var start = Date.now(),
      end = start + duration,
      startPos = el.scrollTop,
      distance = targetPos - startPos,
      smoothStep = function(start, end, point) {
        if (point <= start) return 0;
        if (point >= end) return 1;
        var x = (point - start) / (end - start);
        return x*x * (3 - 2*x);
      };

  return new Promise(function(resolve, reject) {
    var prevPos = el.scrollTop,
        scrollFrame = function() {
          if (el.scrollTop !== prevPos) {
            reject('interrupted');
            return;
          }

          var now = Date.now(),
              point = smoothStep(start, end, now),
              frameTop = Math.round(startPos + (distance * point));

          el.scrollTop = frameTop;

          if (now >= end) {
            resolve();
            return;
          }

          if (el.scrollTop === prevPos && el.scrollTop !== frameTop) {
            resolve();
            return;
          }

          prevPos = el.scrollTop;

          setTimeout(scrollFrame, 0);
        };

        setTimeout(scrollFrame, 0);
  });
};


// DOM element class modification convenience functions
dom.addClass = function(el, className) {
  el.classList.add(className);
};

dom.addClasses = function(el, classList) {
  classList.forEach(function(className) {
    dom.addClass(el, className);
  });
};

dom.removeClass = function(el, className) {
  el.classList.remove(className);
};

dom.removeClasses = function(el, classList) {
  classList.forEach(function(className) {
    dom.removeClass(el, className);
  });
};

dom.toggleClass = function(el, className) {
  el.classList.toggle(className);
};

dom.toggleClasses = function(el, classList) {
  classList.forEach(function(className) {
    dom.toggleClass(el, className);
  });
};

dom.hasClass = function(el, className) {
  return el.classList.contains(className);
};

dom.hasClasses = function(el, classList) {
  return !!classList.filter(function(className) {
    return dom.hasClass(el, className);
  }).length;
};


// events library
events._addEvent = (function() {
  if (document.addEventListener) {
    return function(el, type, fn, capture) {
      el.addEventListener(type, fn, capture);
      return fn;
    };
  } else if (document.attachEvent) {
    return function(el, type, fn) {
      var boundFn = function() {
        return fn.apply(el, arguments);
      };
      el.attachEvent('on' + type, boundFn);
      return boundFn;
    };
  } else {
    // should hopefully never hit this
    return function() { return false; };
  }
})();

events._removeEvent = (function() {
  if (document.removeEventListener) {
    return function(el, type, fn, capture) {
      el.removeEventListener(type, fn, capture);
      return true;
    };
  } else if (document.attachEvent) {
    return function(el, type, fn) {
      el.detachEvent('on' + type, fn);
      return true;
    };
  } else {
    // should hopefully never hit this
    return function() { return false; };
  }
})();


events.addEvent = function(el, type, fn, capture) {
  capture = capture || false;
  var addedFn = events._addEvent(el, type, fn, capture);
  if (!el._events) el._events = [];
  el._events.push({type:type, fn:addedFn, capture:capture});
};

events.addEvents = function(el, type, es, capture) {
  es.forEach(function(e) {
    events.addEvent(el, type, e, capture);
  });
};

events.addEventsFromObject = function(el, es, capture) {
  var e;
  for(e in es) {
    if (Array.isArray(es[e])) {
      events.addEvents(el, e, es[e], capture);
    } else {
      events.addEvent(el, e, es[e], capture);
    }
  }
};

// overload method to call one of the two methods previous methods
events.add = function() {
  if (arguments[0]) {
    var method = typeof arguments[1] === 'object' ? 'addEventsFromObject' : (Array.isArray(arguments[2])) ? 'addEvents' : 'addEvent';
    events[method].apply(this, arguments);
  } else logger.error('Trying to add an event to a null element. Event(s): ' + arguments[1]);
};


events.removeEvent = function(el, type, fn, capture) {
  capture = capture || false;
  events._removeEvent(el, type, fn, capture);
};

events.removeEvents = function(el, type, es, capture) {
  es.forEach(function(e) {
    events.removeEvent(el, type, e, capture);
  });
};

events.removeEventsFromObject = function(el, es, capture) {
  var e;
  for (e in es) {
    if (Array.isArray(es[e])) {
      events.removeEvents(el, e, es[e], capture);
    } else {
      events.removeEvent(el, e, es[e], capture);
    }
  }
};

// overload method to call one of the two methods previous methods
events.remove = function() {
  if (arguments[0]) {
    var method = (typeof arguments[1] === 'object') ? 'removeEventsFromObject' : (Array.isArray(arguments[2])) ? 'removeEvents' : 'removeEvent';
    events[method].apply(this, arguments);
  } else logger.error('Trying to remove an event from a null element. Event(s): ' + arguments[1]);
};

events.removeByType = function(el, type) {
  var arr = [];
  el._events.filter(function(e) {
    return e.type === type;
  }).forEach(function(e) {
    el.removeEventListener(e.type, e.fn, e.capture);
  });

  el._events.filter(function(e) {
    return e.type !== type;
  }).forEach(function(e) {
    arr.push(e);
  });

  el._events = arr;
};

events.removeAll = function(el) {
  el._events.forEach(function(e) {
    el.removeEventListener(e.type, e.fn, e.capture);
  });

  el._events = [];
};


events.stop = function(e) {
  if (typeof e.preventDefault === 'function') e.preventDefault();
  if (typeof e.stopPropagation === 'function') e.stopPropagation();

  return false;
};


events.trigger = function(type, el, _eventType) {
  _eventType = _eventType || 'Event';
  
  var eventType = window[_eventType],
      event;

  try {
    event = new eventType(type);
  } catch (e) {
    event = document.createEvent(_eventType);
    event.initEvent(type, false, false);
  }

  el.dispatchEvent(event);
};


events._throttle = function(el, eventType, eventName) {
  el = el || window;
  
  var running = false,
      fn = function() {
        if (running) return;
        running = true;
        requestAnimationFrame(function() {
          events.trigger(eventName, el, 'CustomEvent');
          running = false;
        });
      };
      
  events.add(el, eventType, fn);
};


events.addCustom = function(el, eventType, eventName, fn) {
  if (eventType) events._throttle(el, eventType, eventName);
  events.add(el, eventName, fn);
};


events.getTargetElement = function(e) {
  var target = e.target;

  while (target.nodeType !== 1) {
    target = target.parentNode;
  }

  return target;
};


// logger library
logger.levelMethods = [
  [],
  ['error'],
  ['warn'],
  ['time','timeEnd','profile','profileEnd','trace','debug','count','assert','dir'],
  ['log','info','table']
];

logger.levels = { 'SILENT': 0, 'ERROR': 1, 'WARN': 2, 'DEBUG': 3, 'LOG': 4 };

// level can be either the key or the value from logger.levels
logger.setLevel = function(level) {
  if (typeof level === 'string' && logger.levels[level.toUpperCase()] !== undefined) level = logger.levels[level.toUpperCase()];

  if (typeof level === 'number' && level >= 0) {
    this.level = level;

    window.localStorage['LT.Logger'] = level;

    this.replaceLoggingMethods(level);
  } else {
    logger.error('logger.setLevel() called with invalid level: ' + level);
  }
};

logger.replaceLoggingMethods = function(level) {
  var i;

  if (typeof console === 'undefined') level = 0;

  for (i = 0; i <= level; i++) {
    logger.levelMethods[i].forEach(function(methodName) {
      this[methodName] = console[methodName] !== undefined ? console[methodName].bind(console) : console.log.bind(console);
    }, this);
  }

  for (; i < logger.levelMethods.length; i++) {
    logger.levelMethods[i].forEach(function(el) { this[el] = utils.noop; }, this);
  }
};

// init the logger
logger.setLevel(parseInt(window.localStorage['LT.Logger'], 10) || 0);


// AJAX library
ajax._requests = {};

ajax.types = {
  html: 'text/html',
  json: 'application/json',
  xml: 'application/xml',
  form: 'application/x-www-form-urlencoded',
  javascript: 'text/javascript',
  js: 'text/javascript',
  svg: 'image/svg+xml'
};

ajax.serialize = {
  'text/html': utils.noop,
  'application/xml': utils.noop,
  'application/json': JSON.stringify,
  'application/x-www-form-urlencoded': ajax.encode
};

ajax.parse = {
  'text/html': utils.noop,
  'application/xml': utils.noop,
  'application/json': JSON.parse,
  'application/x-www-form-urlencoded': ajax.decode
};

ajax.normalizeURL = function(url) {
  ~url.indexOf('/?') && (url = url.replace('/?', '?'));
  url[0] === '/' && (url = url.slice(1));
  url[url.length - 1] === '/' && (url = url.slice(0, -1));

  return url;
};

ajax.decode = function(params) {
  var obj = Object.create(null),
      pairs = params.split('&'),
      parts;

  pairs.forEach(function(pair) {
    parts = pair.split('=');
    obj[decodeURIComponent(parts[0])] = decodeURIComponent(parts[1]);
  });

  return obj;
};

ajax.encode = function(params) {
  if (typeof params !== 'object') return params;

  var arr = [], key;

  for (key in params) {
    arr.push(encodeURIComponent(key) + '=' + encodeURIComponent(params[key]));
  }

  return arr.join('&');
};

ajax._afterComplete = function(e) {
  var ID = e.target._ID,
      xhr = ajax._requests[ID];

  delete ajax._requests[ID];

  events.remove(xhr, {
    load: ajax._afterComplete,
    error: ajax._afterComplete,
    abort: ajax._afterAbort
  }, false);
};

ajax._afterAbort = function(ID) {
  cache.requests.remove(ID);
  ajax.afterComplete(ID);
};

ajax._abort = function(ID) {
  ajax._requests[ID].abort();
  ajax.afterAbort(ID);
};

ajax._defaultEvents = function() {
  return {
    "load": [],
    "error": [],
    "abort": [],
    "progress": []
  };
};

ajax._constructURL = function(opts) {
  var params = opts.params ? ajax.encode(opts.params) : null;
  opts.url = params ? opts.url + '?' + params : opts.url;
};

ajax._createXHR = function(method, opts, el) {
  var xhr = new XMLHttpRequest();

  xhr._el = el;
  xhr._ID = opts.url;

  xhr.open(method, opts.url, true);

  if (opts.type) xhr.setRequestHeader('Content-Type', ajax.types[opts.type]);

  if (opts.accept) xhr.setRequestHeader('Accept', ajax.types[opts.accept]);

  if (!opts.events) opts.events = ajax._defaultEvents();

  opts.events.load.push(ajax._afterComplete);
  opts.events.error.push(ajax._afterComplete);
  opts.events.abort.push(ajax._afterAbort);

  if (opts.events.progress.length) events.add(xhr, 'progress', opts.events.progress);
  events.add(xhr, 'load', opts.events.load);
  events.add(xhr, 'error', opts.events.error);
  events.add(xhr, 'abort', opts.events.abort);

  ajax._requests[opts.url] = xhr;

  return xhr;
};

ajax.get = function(opts, el) {
  // allow for simple get requests when all we need is the URL to fetch
  if (typeof opts !== 'object') opts = {url: opts};
  
  return new Promise(function(resolve, reject) {
    var xhr;

    ajax._constructURL(opts);

    xhr = opts.cache ? cache.requests.get(opts.url) : null;

    if (xhr) {
      events.trigger('load', xhr);
      resolve(xhr);
    } else {
      xhr = ajax._createXHR('GET', opts, el);

      xhr.onreadystatechange = function(opts) {
        if (this.readyState === this.DONE) {
          if (this.status > 399) {
            cache.requests.remove(opts.url);
            logger.error('ajax.get failed for URL: `' + opts.url + '`; status: [' + this.status + ']');
            if (reject) reject(this);
          } else {
            resolve(this);
          }
        }
      }.bind(xhr, opts);

      xhr._modified = Date.now();
      cache.requests.set(opts.url, xhr);

      xhr.send();
    };
  });
};

ajax.post = function(opts, el) {
  return new Promise(function(resolve, reject) {
    var xhr, ID;

    ajax._constructURL(opts);
    
    opts.data = opts.data ? (opts.type ? ajax.serialize[ajax.types[opts.type]](opts.data) : opts.data) : '';

    xhr = ajax._createXHR('POST', opts, el);

    xhr.onreadystatechange = function() {
      if (this.readyState === this.DONE) {
        if (this.status > 399) {
          logger.error('ajax.post failed for URL: `' + opts.url + '`; status: [' + this.status + ']');
          if (reject) reject(this);
        } else {
          resolve(this);
        }
      }
    };

    xhr.send(opts.data);
  });
};

ajax.loadScript = function(opts) {
  // allow for simple get requests when all we need is the URL to fetch
  if (typeof opts !== 'object') opts = {url: opts};

  var _opts = {
    type: 'text/javascript'
  };

  if (opts.async) _opts.async = 'async';
  if (opts.defer) _opts.defer = 'defer';
    
  return new Promise(function(resolve, reject) {
    var loadedScript = dom.getEl('[src="' + opts.url + '"]');

    if (loadedScript) {
      resolve(this);
    } else {
      var script = dom.create('script', null, _opts);

      script.onload = function() {
        resolve(this);
      };

      script.src = opts.url;
      dom.append(document.body, script);
    }
  });
}
/*VV: 05302016: LO Library */
LO.getSessionInfo = function() {		
	if(localStorage._stfit_dom){
		global_domain = localStorage._stfit_dom;				
	}
	
	if(localStorage._dri && localStorage._ispopuplogin !=1 ){		
		LO.callNativeAddon();
	} else {
		localStorage._ispopuplogin = 0;	
		ajax.get({	  
			  url: "http://"+global_domain+'/GetSessionInfo.do',
			}).then(function(xhr) {
			  var data		= JSON.parse(xhr.responseText),login;
			  login 		= data.Results[0].session.user;
			  if(login)	{		
				localStorage._dri  = login.dri;
				localStorage._code = login.code;
				localStorage._name = login.name;		
				LO.callNativeAddon();	
				loader("N");			
			  }else{
				  loader("N");		 
			  }	  
		});
	}	
};

LO.callNativeAddon = function() {						
		var temp				=	encodeURIComponent(document.getElementById("CopyActiveLink").textContent),
		currentTabTitle			=	decodeURIComponent( temp.split('~')[0] ),
		currentUrlTemp 			=	temp.split('~')[1],
		encodeurl				=	currentUrlTemp, 
		_url					=	"/StuffUrl.do?URL="+encodeurl+"&SiteTitle="+encodeURIComponent(currentTabTitle);		
		if(localStorage._stfit_ver && localStorage._stfit_typ) {
			fVersion = localStorage._stfit_ver;
			fVertype = localStorage._stfit_typ;
		}
		var clrtime = setInterval(function(){
			try{
				clearInterval(clrtime);
				chrome.tabs.executeScript({code:'global_domain ="livestuff.com";stuffitModule.init("'+_url+'","'+encodeURIComponent(currentTabTitle)+'","'+global_domain+'","'+fVersion+'","'+fVertype+'","'+localStorage._name+'","'+localStorage._dri+'","'+localStorage._code+'");'});			
				window.close();		
			}catch(e){				
			}
		}, 500);		
};
LO.onLoginSubmit = function() {
	var user = dom.getEl(".login__userfield"),
			pass = dom.getEl(".login__passfield"),
      params = ajax.decode(window.location.search),
      redirect = window.location ,errormsg = dom.getByClass('login__error__msg')[0];
	  loader("Y");
	
  if(window.location.pathname.toLowerCase().indexOf("login.html") !== -1)
    redirect = params["?Redirect"] || params.Redirect || "";
      
	if(user.value === "" || pass.value === ""){
		loader("N");
		return;
	}	  
	ajax.post({
		url: 'http://'+ global_domain+'/login.do',
		params: {
			Username: user.value,
			Password: pass.value,
			Redirect: redirect || ""
		}
	}).then(function(xhr) {
		var txt = xhr.responseText;
		if(txt && txt !== "") {
			var data = JSON.parse(txt);					
			if(data && data.Result) {			 	
				localStorage._dri 	 = data.Result['Direct Resource Identifier'];
				localStorage._code	 = data.Result['ObjectCode'];
				localStorage._name	 = data.Result['Formatted Name'];						
				LO.callNativeAddon();				
			}else{						
				dom.removeClass(errormsg,'gu-hide');
			}
		}
		loader("N");
	});  
};
LO.privacyClose_Click = function() {
	alert("privacy");
	var popup = dom.getById('privacyModal');	
	dom.remove(popup); 
}
 
LO.createAccount_Click = function() { 

    var options = dom.getByClass("login__options")[0],
        newacct = dom.getByClass("login__newaccount")[0],
        active = options.getElementsByClassName("active")[0],
        ctrls = dom.getByClass("login__controls")[0],
        addtctrls = dom.getByClass("login__additionalcontrols")[0],
        form = dom.getByClass("login__form")[0],
        button = dom.getByClass("login__button")[0],
        user = dom.getEl(".login__userfield"),
		sbutton 	= dom.getByClass("signup__button")[0],
        confirm = dom.getEl(".login__confirmpassfield"),
        name = dom.getEl(".login__name"),
		checkop = dom.getEl(".keeplogin");	
	
    if(!name) {      
	  name = dom.create("input", null, {
        className: "login__name grid__column",
        name: "Name",
        type: "text",
        placeholder: "Name",
        required: true
      });
    }    

    if(!confirm) {
      confirm = dom.create("input", null, {
        className: "login__confirmpassfield grid__column",
        name: "Confirm Password",
        type: "password",
        placeholder: "Confirm Password",
        required: true
      });
    }
	  
	user.setAttribute("Name", "id");	
	dom.addClass(button,'gu-hide');
	dom.removeClass(sbutton,'gu-hide');
	form.insertBefore(confirm, ctrls);
	form.insertBefore(name, user);
	/*	
	form.onsubmit = function(){
		return LO.createAccount(form);
	}*/
	
	options.appendChild(newacct);
	dom.removeClass(active, "active");
	dom.addClass(newacct, "active");
	dom.addClass(addtctrls, "gu-hide");
	dom.addClass(checkop,"gu-hide");
};

LO.createAccount = function(form) {

  if(typeof form === "undefined")
    form = dom.getByClass("login__form")[0];

  var data 		= new FormData(form),
      pass 		= dom.getByClass("login__passfield")[0],
      confirm 	= dom.getByClass("login__confirmpassfield")[0],
      errormsg 	= dom.getByClass("login__error__msg")[0];
      
  data.append("AutoLogin", "Y");  
  if(confirm.value === pass.value && confirm.value.length > 0) {	  
   ajax.post({
      url: 'https://'+ global_domain+'/CreateUser.do',
      data: data
    }).then(function(xhr) {
      var data = JSON.parse(xhr.responseText); 	  
	  LO.getSessionInfo();      	  
      //LO.loginWithEmail();      
    });	 
  } else {
    errormsg.innerText = "Passwords do not match.";
    dom.removeClass(errormsg,'gu-hide');
  }
  return false;
};
 LO.loginWithEmail = function(event) {
    var options = dom.getByClass("login__options")[0],
		opemail   = dom.getByClass("options__email")[0],
        newacct = dom.getByClass("login__newaccount")[0],
        active = newacct.getElementsByClassName("active")[0],
        ctrls = dom.getByClass("login__controls")[0],
        addtctrls = dom.getByClass("login__additionalcontrols")[0],
        form = dom.getByClass("login__form")[0],
        button = dom.getByClass("login__button")[0],
        user = dom.getEl(".login__userfield"),
        confirm = dom.getEl(".login__confirmpassfield"),
		sbutton 	= dom.getByClass("signup__button")[0],
        name = dom.getEl(".login__name"),
		checkop = dom.getEl(".keeplogin"),	
        t = (event) ? event.target : null;
      
    if(window.UserAgents){
	 	if(window.UserAgents.isIOS()){
			(user.style.borderColor == "red") ? user.style.borderColor="" : null;
							
			var pass = dom.getByClass("login__passfield")[0];	
			(pass.style.borderColor == "red") ? pass.style.borderColor="" : null;
		}  
	 }		
    dom.removeClass(newacct, "active");
    user.setAttribute("Name", "Username");
    button.innerText = "Login";

    if(t)
      dom.addClass(t, "active");

    if(confirm) {
      confirm.remove();
    }
    if(name) {
      name.remove();
    }

    addtctrls.appendChild(newacct);
    dom.removeClass(addtctrls, "gu-hide");
	dom.addClass(opemail, "active");
    dom.removeClass(button,'gu-hide');
	dom.addClass(sbutton,'gu-hide');
	dom.removeClass(checkop,"gu-hide");
	
    form.onsubmit = function(event) {
      return widgets.login.onLoginSubmit();
    }
    dom.addClass(dom.getByClass("login__error__msg")[0],"gu-hide");  
  }
LO.showPrivacy = function(e) {
  events.stop(e);
  ajax.get({
    url: 'http://'+ global_domain+'/PrivacyPolicy.htm'
  }).then(function(xhr){
    var data = xhr.responseText,
        mod = dom.getEl("#privacyModal");
    if(!mod) {
      var modalParent = document.body;
      mod = modal.create({ id: "privacyModal", title: "Privacy Policy"});
    }

    var body = dom.getByClass('modal__body', mod)[0];
    body.innerHTML = data;
    body.style.overflowY = "auto";

    if(window.loginModal)
      modal.close(window.loginModal);
      
    modal.open(mod);
  });
};

LO.loginWithFacebook = function() { 
  var state = (location.pathname.toLowerCase().indexOf("login.html") === -1) ? encodeURIComponent("{ \"redirect\": \"" + "http://"+ global_domain + "\" }") : "",
  id = 1627457787506539,
  redirect = 'http://'+ global_domain +'/loginwithfacebook.do', /*location.protocol + "//" + location.host + "/loginwithfacebook.do",*/
  url = "https://www.facebook.com/dialog/oauth?client_id=" + id + "&redirect_uri=" + redirect + "&state=" + state;  
  //location.href = url;
  chrome.tabs.create({ url: url });
};

LO.loginWithGoogle = function() {  
  var state 	= (location.pathname.toLowerCase().indexOf("login.html") === -1) ? encodeURIComponent("{ \"redirect\": \"" + "http://"+ global_domain + "\" }") : "",
  id 			= "56545473939-4gguhruput323ab8e33hhkfvco6qe61d.apps.googleusercontent.com",
  redirect 		= 'http://'+ global_domain +'/LoginWithGoogle.do', //location.protocol + "//" + location.host + "/LoginWithGoogle.do",
  url 			= "https://accounts.google.com/o/oauth2/v2/auth?client_id=" + id + "&redirect_uri=" + redirect + "&state=" + state + "&response_type=code&scope=profile%20email";
  //location.href = url;
  chrome.tabs.create({ url: url });
};

modal.init = function(modalContainer, el) {
  if (!modalContainer && el && el._data) modalContainer = dom.getById(el._data.target);

  if (!modalContainer) {
    logger.error('A DOM element must be supplied when initializing a modal');
    return false;
  } 
  dom.getEls('[data-dismiss="modal"]', modalContainer).forEach(function(el) {
    events.add(el, 'click', function(e) {
      var modalContainer = el._modal ? el._modal : el._modal = dom.getById(el._data.target);
      modal.close(modalContainer, el);
    });
  });
  
  modalContainer._init = true;

  return modalContainer;
};

modal.create = function(opts) {
  opts = opts || Object.create(null);
  opts.id = opts.id || Date.now().toString();
  opts.parent = opts.parent || dom.getById('main');

  var checkModal = dom.getById(opts.id);
  if (checkModal) return checkModal;
  
  var modalContainer = dom.create('div', null, {
        id: opts.id,
        className: 'modal-container',
        'aria-hidden': 'true',
        'aria-labelledby': opts.id + 'ModalLabel',
        role: 'dialog',
        tabindex: '-1'
      }),
      modalMain = dom.create('div', modalContainer, {
        className: 'modal grid -left-to-right -middle-aligned',
        role: 'document'
      }),
      container = dom.create('section', modalMain, {className: 'grid -top-to-bottom -nowrap'}),
      header = dom.create('header', container, {className: 'grid__row grid -left-to-right -nowrap'}),
      body = dom.create('div', container, {className: 'grid__row -resize modal__body'}),
      footer;

  if (!opts.notitle) {
    dom.create('h4', header, {
      id: opts.id + 'ModalLabel',
      className: 'grid__column -resize',
      innerHTML: opts.title || ''
    });

    dom.create('button', header, {
      className: '-close',
      type: 'button',
      'aria-label': 'Close',
      'data-target': opts.id,
      'data-dismiss': 'modal'
    });
  } else {
    dom.addClass(header, '-no-border');
  }
  
  if (opts.closeText || opts.saveText) {
    footer = dom.create('footer', container, {className: 'grid__row'});
    
    if (opts.closeText) {
      dom.create('button', footer, {
        type: 'button',
        'data-target': opts.id,
        'data-dismiss': 'modal',
        innerHTML: opts.closeText || 'Close'
      });
    }

    if (opts.saveText) {
      dom.create('button', footer, {
        type: 'button',
        'data-target': opts.id,
        'data-dismiss': 'modal',
        innerHTML: opts.saveText || 'Save'
      });
    }
  }

  dom.append(opts.parent, modalContainer);

  modal.init(modalContainer);

  return modalContainer;
};

modal.open = function(modalContainer, el) {
  if (!el && !modalContainer) {
    logger.error('A DOM element must be supplied when opening a modal');
    return false;
  }

  if (!modalContainer && el && el._data) modalContainer = dom.getById(el._data.target);

  if (modalContainer.style.display = 'block') return;

  if (el && el._data && el._data.saveFn) events.add(dom.getByClass('js-save', modalContainer)[0], 'click', el._data.saveFn.bind(this, modalContainer), false);

  modalContainer._source = el;

  modalContainer.style.display = 'block';

  var form = dom.getByTag('form', modalContainer);
  if (form.length && form[0][0]) form[0][0].focus();
};

modal.close = function(modalContainer, el) {
  if (!el && !modalContainer) return false;

  if (!modalContainer && el && el._data) modalContainer = dom.getById(el._data.target);
  el = el || modalContainer._source;

  if (!el && !modalContainer) return false;
  var btn = dom.getByClass('-js-save', modalContainer)[0];
  if (btn) events.removeAll(btn);

  modalContainer._source = null;
  modalContainer.style.display = 'none';
};

modal.destroy = function(modalContainer, el) {
  if (!modalContainer && el && el._data) modalContainer = dom.getById(el._data.target);

  if (!modalContainer) return false;

  modalContainer._subscriptions.forEach(function(sub) {
    sub.dispose();
  });

  modalContainer._subscriptions = null;
};

