var MyFavoritesHpos, tabUrlTemp, quickSearchRequest, clickedOnRefine = false;
var global_domain = "livestuff.com", global_type = "Live",fVertype = "Live", fVersion = '51608300', fLivePlatformVersion; 

document.addEventListener('DOMContentLoaded', function (){		
	getAllVersions();	
	initScript();	
}); 

function initScript(){
chrome.tabs.query({active: true, currentWindow: true}, function(arrayOfTabs) {
	var activeTab 			= arrayOfTabs[0];
	activeTabUrl			= arrayOfTabs[0].url;
	tabUrlTemp 				= arrayOfTabs[0].url;
	var activeTabTitle 		= arrayOfTabs[0].title;
	var fillContent 		= document.getElementById("CopyActiveLink");
	fillContent.textContent = activeTabTitle+"~"+activeTabUrl;							
});	

chrome.tabs.executeScript(null,{file: "data/js/jquery-1.10.2.min.js"}, function(){
	chrome.tabs.executeScript(null,{file: "data/js/nf/jquery-ui-1.10.2.min.js"});			
	chrome.tabs.executeScript(null,{file: "data/js/nf/contentscript.js"});
	chrome.tabs.executeScript(null,{file: "data/js/rx.all.min.js"});
	chrome.tabs.executeScript(null,{file: "data/js/nf/moment.min.js"});
	chrome.tabs.executeScript(null,{file: "data/js/nf/framework.js"});
	chrome.tabs.executeScript(null,{file: "data/js/nf/mapfunction.js"});
	chrome.tabs.executeScript(null,{file: "data/js/nf/builduiparser.js"});		
	chrome.tabs.executeScript(null,{file:"data/js/nf/contentfunctions.js"});
	LO.getSessionInfo();
	
	var loginbtn	=	document.getElementById("LoginButton");
	loginbtn.addEventListener('click',function(){LO.onLoginSubmit();});
	
	var newactbtn	=	document.getElementById("createAccount");
	newactbtn.addEventListener('click',function(){LO.createAccount_Click()});

	var loginicon	=	document.getElementsByClassName("options__email")[0];
	loginicon.addEventListener('click',function(){LO.loginWithEmail()});	

 	var terms		=	document.getElementById("terms");
	terms.addEventListener('click',function(){LO.showPrivacy(event);});		
	
	var createNewAccount		=	document.getElementById("createNewAccount");
	createNewAccount.addEventListener('click',function(){LO.createAccount();});	

	var signInFb	=	document.getElementById("SignInFacebook");
	signInFb.addEventListener('click',function(){LO.loginWithFacebook();});
	
	var signInGp	=	document.getElementById("SignInGoogle");
	signInGp.addEventListener('click',function(){LO.loginWithGoogle();});	
		
	var privacyclose	=	document.getElementsByClassName("-close")[0];
	privacyclose.addEventListener('click',function(){LO.privacyClose_Click()});	

	});	
	chrome.tabs.insertCSS(null,{file: "data/js/nf/popup.css"});	
	
	$(".login__passfield").on("keydown", function(e){
		if(e.keyCode	==	13){
			LO.onLoginSubmit();
		}
	});
	
	$("#createNewAccount").on("keydown", "input", function(e){
		if(e.keyCode	==	13){
			signUpUser('fullname','signupusername','signuppassword', 'confirmpassword');
		}
	});	
	
	$('.login__header').on('click', '.cogimage', function(){
		$('.cogoptions').slideToggle("fast");
	});	 
} 
function getAllVersions(){	
	var _url = "https://" + global_domain + "/GetReleases.do";	
	var str = "";
	$.ajax({
		url : _url,
		type : "POST",
		dataType : "JSON",
		error : function (error){			
		},
		success : function (data){
			var response 	= data["Results"];							
			if(response!=''){
				for(var m = 0; m < response.length; m++){
					var thisver  	= response[m]["Version"],
						thisfver 	= response[m]["LivePlatform Version"],
						thisname 	= response[m]["Name"],
						thisid	 	= response[m]["Id"],
						thisdomain 	= response[m]["PrimaryDomain"];
						if(typeof response[m]["Name"] === 'undefined' || thisname == '' || thisname == null){
							thisname = response[m]["Environment"];
						}	
						
						if(localStorage._stfit_typ == thisname) {																					
							var verselected = localStorage._stfit_ver+"";						
							fVersion =	localStorage._stfit_ver ;
							if(verselected.length==7){
								var pointver = verselected.substring(0,3)+"."+verselected.substring(3,4)+"."+verselected.substring(4,7);							
							}else{
								var pointver = verselected.substring(0,3)+"."+verselected.substring(3,5)+"."+verselected.substring(5,8);
							}
							var vercontainer = document.getElementById("version");
							vercontainer.textContent = thisname+": "+pointver;	
							
							var showVerDet			= document.getElementById("showVersionDetail");
							showVerDet.textContent	= localStorage._stfit_typ+'-'+localStorage._stfit_ver;	
						} else if(fVertype == thisname && !localStorage._stfit_typ){
								var verselected = thisver+"";						
								fVersion =	thisver ;
							if(verselected.length==7){
								var pointver = verselected.substring(0,3)+"."+verselected.substring(3,4)+"."+verselected.substring(4,7);							
							}else{
								var pointver = verselected.substring(0,3)+"."+verselected.substring(3,5)+"."+verselected.substring(5,8);
							}
							var vercontainer = document.getElementById("version");
							vercontainer.textContent = thisname+": "+pointver;								
						}
					str += '<div class="select_version" ver-type="'+thisname+'" ver-val="'+thisver+'" fver-val="'+thisfver+'" ver-domain="'+thisdomain+'">'+thisname+' : '+thisver+'</div>';					
				}					
				$(".other_versions").html(str);
				$(".version_container").on("click", ".current_version", function(){
					$(".other_versions").slideToggle();
				});
				
				$(".other_versions").on("click", ".select_version", function(){
					$(".current_version").click();
					$(".cogimage").click();
					var verselected 	= fVersion = $(this).attr("ver-val"),
						fverselected 	= fLivePlatformVersion = $(this).attr("fver-val"),
						vertype 		= $(this).attr("ver-type"),
						verdomain 		= $(this).attr("ver-domain");						
						
					if(verselected.length==7){
						var pointver = verselected.substring(0,3)+"."+verselected.substring(3,4)+"."+verselected.substring(4,7);							
					}else{
						var pointver = verselected.substring(0,3)+"."+verselected.substring(3,5)+"."+verselected.substring(5,8);
					}
					
					var vercontainer = document.getElementById("version");					
					vercontainer.textContent = vertype+": "+pointver;				 
					var showVerDet = document.getElementById("showVersionDetail");
					showVerDet.textContent	 =	vertype+'-'+verselected;

					localStorage._stfit_ver  = fVersion = verselected;
					localStorage._stfit_fver = fLivePlatformVersion = fverselected;
					localStorage._stfit_typ  = vertype;
					localStorage._stfit_dom  = verdomain;
					
					global_domain = verdomain;					
					$('#LogoutButton').hide();
					$('.ItemDetails').hide();					
					loader("Y");
					LO.getSessionInfo();
				});
			}			
		}
	});
}
/*loader function */
function loader(type) {
	if (type=="Y") {				
		$('.inlineBlockUIOverlay').show();
		$('.inlineBlockUIMsg').show();			
	}else {				 		 
		$('.inlineBlockUIOverlay').hide();
		$('.inlineBlockUIMsg').hide();		 
	}
}