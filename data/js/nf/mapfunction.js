var scriptdata = "var marker;function initMap() {"+
    "console.error = function() {};	"+
	"var elem = document.querySelectorAll('[data-inputtype=\"Address\"')[0];"+			
	"var elemparent	=	elem.parentNode.parentNode,"+
	"mapdiv 		= document.getElementsByClassName('map_canvas_address'),"+				
	"elemVal		= elem.value,"+
	"latitude ,longitude;"+
"	if ( mapdiv.length > 0 ) {"+	
"			mapdiv[0].classList.remove('-hidden');"+
"			mapdiv[0].classList.remove('-show');"+					
"	} else {"+
"		var addressmap = document.createElement('div');"+
"addressmap.id = 'map_canvas_address';addressmap.className = 'Row clear map_canvas_address -show';elemparent.appendChild(addressmap);"+	
"		elem.addEventListener('keyup', function(event) {"+
"			var elem = event.target; var elemvalue	=	elem.value;			"+	
"			if(elemvalue.length > 5){"+
"				var keycode	=	event.keyCode;"+
"				if(keycode	== 13 || keycode == 37 || keycode == 38 || keycode == 39 || keycode == 40 || keycode == 9){"+
"				}else{"+
"					navigator.geolocation.getCurrentPosition(function (position) {"+
"						codeAddress(elem);"+
"					},function (e){			"+					
"						codeAddress(elemvalue);	"+
"					});"+
"				}"+
"			}		"+		
"		});				"+															
"	}	"+
"	if(elemVal == '') {			"+
"			if (navigator.geolocation) {"+
"					navigator.geolocation.getCurrentPosition(function (position) {"+
"							latitude	 =  position.coords.latitude;"+
"							longitude =  position.coords.longitude;"+
"							GetCurrentLocation(latitude,longitude,elem);					"+
"							UpdateAddress(latitude,longitude,elem );							"+																			
"					},function (e){					"+
"						latitude	  =  39.992;"+
"						longitude     =  -100.374;"+
"						GetCurrentLocation(latitude,longitude,elem);					"+
"						UpdateAddress(latitude,longitude,elem );							"+					
"					});"+
"			}	"+
"	} else {"+
"	}"+
"}"+
"function GetCurrentLocation (latitude,longitude,elem) {"+
"	geocoder 		= new google.maps.Geocoder();		"+	
"	var position 	= new google.maps.LatLng(latitude, longitude);"+
"	var myOptions 	= {"+
"		zoom		: 15,"+
"		center	: position"+
"	};"+
"	map = new google.maps.Map(document.getElementById('map_canvas_address'), myOptions);"+
"	var location = new google.maps.LatLng(latitude, longitude);	"+
"	map.setCenter(location);"+
"	marker = new google.maps.Marker({"+
"		position		: location,"+
"		map			: map,"+
"		draggable	: true,"+
"		title		: 'Your location! Drag the marker to the desired location'			"+
"	});		"+
"	google.maps.event.addListener(marker, 'dragend', function() {				"+
"		UpdateAddress(this.position.lat(), this.position.lng(),elem);"+
"		infowindow.close();"+
"	});"+
"	var infowindow = new google.maps.InfoWindow({"+
"		content:'Your location! Drag the marker to the desired location'"+
"	});"+
"	google.maps.event.addListener(marker, 'click', function() {"+
"		 infowindow.setContent(elem.value);"+
"		 infowindow.open(map,marker);"+
"	});				"+
"	map.addListener('click', function(event){"+
"	marker.setPosition(event.latLng);"+
"		UpdateAddress(event.latLng.lat(), event.latLng.lng(),elem);"+
"	   infowindow.close();"+
"	});		   		"+
"}"+
"function codeAddress (elem) {"+
"			var address = elem.value;"+
"			if(!address) address = elem;"+
"			geocoder.geocode( { 'address': address}, function(results, status) {"+
"				if (status == google.maps.GeocoderStatus.OK) {"+
"					map.setCenter(results[0].geometry.location);"+
"					marker = new google.maps.Marker({"+
"						map			: 	map,"+
"						position		: 	results[0].geometry.location,"+
"						draggable	:	true,"+
"						title			:	'Your location! Drag the marker to the desired location'"+
"					});					"+
"					var location	=	results[0].geometry.location;					"+
"					google.maps.event.addListener(marker, 'dragend', function() {			"+			
"						UpdateAddress(this.position.lat(), this.position.lng(),elem); "+
"						infowindow.close();"+
"					});					"+
"					var infowindow = new google.maps.InfoWindow({"+
"  						content:elem.value"+
"					});"+
"					google.maps.event.addListener(marker, 'click', function() {"+
"						 infowindow.setContent(address);"+
"						 infowindow.open(map,marker);"+
"					});					"+
"				} else {"+
"					console.log('Geocode was not successful for the following reason: ' + status);"+
"				}"+
"			});"+
"}"+
"function UpdateAddress(lat,lng,elem) {"+
"	var latlng = new google.maps.LatLng(lat, lng);"+
"	var geocoder = geocoder = new google.maps.Geocoder();"+
"	geocoder.geocode({ 'latLng': latlng }, function (results, status) {"+
"		if (status == google.maps.GeocoderStatus.OK) {"+
"			if (results[0]) {"+
"				elem.value	= results[0].formatted_address;"+
"			}else if(results[1]){"+
"				elem.val = results[1].formatted_address;"+
"			}"+
"		}"+
"	});"+
"}";
var mapfunc  = dom.getById("mapfunc");
if(!mapfunc){					
	var script = document.createElement("script");
	script.id = "mapfunc";
	script.innerHTML = scriptdata;
	document.body.appendChild(script);					
}