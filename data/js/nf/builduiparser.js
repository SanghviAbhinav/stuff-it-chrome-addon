// logger.setLevel(parseInt(window.localStorage['LT.Logger'], 10) || 4);
//VV:addon changes

var RunAjaxCall = {}
RunAjaxCall.runTheHTTPRequest = 	function(o){
	
	var method		=	o.method || "GET",
		 _url			=	o.url,
		 _params		=	o.params || {},
		 _data		=	o.data || "",
		 _type		=	o.type;
	
	if(typeof _url === "undefined") return false;
	
	if(method.toLowerCase() == "get") {
		return ajax.get({
			url	:	_url,
			type	:	_type
		});
	} else {
		return ajax.post({
			url		:	_url,
			type		:	_type,
			params	:	_params,
			data		:	_data
		});
	}
};



BuildUIParser = function(o){
	
	this.url		=	o.url || "";
	this.header	=	o.header || o.Header || "Y";
	this.redirection	=	o.Redirection || o.redirection || "Y";
	this.cb				=	o.Callback;
	this.catobj			=	o.catobj;
	if(this.url == "") {
		return;
	}
		
 	//VV:05202016: Add Container 
	this.container		  =	o.container || "";
	this.device			  =	o.device 	|| '';
	this.title			  =	o.title 		|| '';
	this.fileImage		  =	o.file  		|| '';
	this.contentType   =   o.contentType || '';
	this.hasformData		 = false; 	
   
	// Logic to pass the icon size according to the curent window
 	this.IconSize	=	128;
	
	//VV:05202016: Add Condition for Device 
 	if(this.device=='desktop') {
	 	var containerWidth   = 400;
 	} else {
	 	var containerWidth   = window.innerWidth;
 	} 	
 	
 	if(containerWidth > 0 && containerWidth <= 320) {
 	 	  this.IconSize= 256;
 	} else if(containerWidth > 320 && containerWidth <= 520) {
 	 	  this.IconSize= 256;
 	} else if(containerWidth > 520) {
	 	  this.IconSize= 512;
 	} else {
	 	  this.IconSize= 512;
 	}
		
 	if(this.url.indexOf("[#IconSize]") > 0) {
 		
 		this.url	=	this.url.replace("[#IconSize]",this.IconSize);
 	}
 	

		this.main_ui_container	=	dom.create('div',null,{
			className			:  "-UIContainer -full-width",
			id						:	"-UIContainer"
	});
	
	// logger.log("this.header",this.header);
	if(this.header == "Y") {
		var header		=	dom.create("div",this.main_ui_container,{
			className	:	"-UIContainer-header primary-border-bottom grid"
		});
		
		var headerlogo		=	dom.create("div",header,{
			className	:	"-UIContainer-header-logo-div"
		});


		var logo			=	dom.create("img",headerlogo,{
			className	:	"-UIContainer-header-logo",
			src			:	'http://assets2.livecache.net/L6/3/IOGLO/856886161/203687543/LiveStuff_BlueRed_noTagline.png'
		});
		
		this.popupclose			=	dom.create('div',header,{
				className			:  "-UIContainer-close -absolute -click"
		});
		
		 
	}

	this.main_ui_content		=	dom.create('div',this.main_ui_container,{
			className			:  "-UIContainer-Content"
	});
	
	
	
	this.ajaxrequest			=	null;
	
	this.enviornment 			=	o.enviornment || "Web";
	//this.fileFormData			=	new FormData();
	if(typeof FormData == "undefined") {
		var data = [];
	} else {
		this.fileFormData			=	new FormData();
	}
	
	this.LocalFiles;
	this.LocalFileCount		=	0;
	this.globaldomain			=	o.globaldomain || "";
	
	if(this.enviornment == "extension") {
		this.globaldomain		=	o.globaldomain;
	}
	
	this.init();		
};

BuildUIParser.prototype = {
	init: function(){
		var that  	=	this;
		var newurl	=	that.replacetheBBCode(encodeURIComponent(that.url));
		this.runUrlAndgetresponse(newurl);
		this.attachevents();
		/*this.Loader();*/
	},
	
	// Attaching the events
	attachevents:	function(){
		var that = this;
		
		// events.add(that.popupclose, 'click', that.closePopup);
		
		/*window.onpopstate = function(e) {
			var popup = dom.getById("-ui-parser");
			if(popup) {
				that.closePopup();
				return;
			}
		};*/
		
		events.add(document.body, 'click', that.closeDropdown);
		if(that.device == "Desktop" || that.device == "desktop") {
		} else {
			events.add(window, 'resize', that.AdjustMatrixaccordingToWidth);
		}
		
		//VV:addon changes		
		if(that.popupclose) that.popupclose.addEventListener("click", function(event){	that.closePopup();});
	},
	
	// Method to close the popup
	closePopup:	function() {
		var that  = this;
		var popup = dom.getById("-ui-parser");
	
		// VV:05202016: Need to improve code here			
		if(popup) {
			// VV:06132016: second time popup are not open in addon			
			if(that.enviornment == "extension") {
				popup.style.display	=	"none";
			} else {
				dom.remove(popup);									
				
				document.body.style.overflow = "unset";
				if(typeof Bridge !== "undefined") {
					/*Bridge.ScrollEventForSwipe(0);*/
					Bridge.disableScrollCheck(false);
				}
			}
		} else {	
			
			if(that.enviornment == "External Apps") {
			
			}else	if(that.enviornment == "External Mobile Apps") {
				Bridge.triggerBack();
			} else {		
			
					if(dom.getById('addToModal')) {
						dom.getById('addToModal').style.display = 'none';
					} 
					
					if(dom.getById('addToCloudModal')) {
						dom.getById('addToCloudModal').style.display = 'none';					
					} 
					that.container.style.height = "auto";	
			}
		}		
		/*history.pushState({"id":101}, window.title, CurrentObjectDRI+"/index.mhtml");*/
	},
	
	// Method to close the dropdowns on outside click
	closeDropdown:	function(evt){
		var that						=	this;
		var target						=	evt.target;
		var dropelem 					= 	dom.getByClass("dropdown-option-div",that.main_ui_container);
		/*VV:08262016: Comment and Update code if multiple dropdown find
			var dropdown					=	dom.getByClass("dropdown-option-div",that.main_ui_container)[0];
		*/	
		dropelem.forEach(function(dropdown) {
			var refinebutton				=	dom.getByClass("dropdown-option-li-refine",dropdown)[0];
			
			if(typeof dropdown !== "undefined") {
				var dropdowninput_level1	=	dropdown.parentNode;
				var dropdowninput_level2	=	dropdowninput_level1.parentNode;
				var dropdowninput_level3	=	dropdowninput_level2.parentNode;
				
				var dropdowninput				=	dom.getByTag("input",dropdowninput_level3)[0];
							
				if(target !== dropdown && target !== dropdowninput) {
					if(dom.hasClass(target,"dropdown-option-li-refine") ) {
					} else {
						dom.remove(dropdown);
					}
				}
			}
		});		
	},
	
	// run the buildUI action
	runUrlAndgetresponse: function(url,updatehtml,container){
		var that = this;
		
		if(!container) {
			utils.showLoader();
		}
		RunAjaxCall.runTheHTTPRequest({
			method	:	"get",
			url		: url
		}).then(function(data){	
			 
			if(!container) {	
				utils.hideLoader();
			}	
			data		=	JSON.parse(data.responseText);
			data		=	data["Results"] || data["Result"] || data;
			
		
			if(data) {		
				if(container) {					
					that.parseTheResponseAndUpdateHTML(data,container);
				} else {	
					that.parseTheResponse(data,updatehtml,container);					
				}
			}
			

			/*VV:06102016: addon loader hide after action complete*/
			if(that.enviornment == "extension") {
				utils.hideLoader();
			}	

		}).catch(function(e){			 
			if(!container) {
				utils.hideLoader();
			}	
			that.showErrorMessage("Some error occured.");
		});
	},
	
	// Parse the response and draw the UI based on the keys.
	parseTheResponse:	function(data, updatehtml,container){
		var that	=	this;		
		if(container) {
			container.innerHTML	=	"";
			var maincontenttoinsert		=	container;
		} else {
			that.main_ui_content	.innerHTML	=	"";
			var maincontenttoinsert		=	that.main_ui_content;
		}
	
		var isVisible,formvisibility;
		for(var i=0; i<data.length; i++) {
			var thistype	=	data[i]["Type"];
			if(typeof thistype	!= 'undefined') {
				
				if(thistype 	== 'Form') {
					var formname	=	data[i]["Name"];
						 isVisible	=	data[i]["Visible"] || true;
					if(isVisible == "False") {
						formvisibility		=	"-hidden";
					} else {
						formvisibility		=	"-show";
					}
												
					var fclassName	=	formname.replace(/\s+/g,"-");
					 	 fclassName	=	fclassName.replace(/([~!@#$%^&*()_+=`{}\[\]\|\\:;'<>,.\/? ])+/g, '-');
					var form			=	dom.create('div',maincontenttoinsert	,{
						className		:  "-UIContainer-area-forms grid "+fclassName+" "+formvisibility
					});
					
					form.setAttribute("Code","");
					if(formname) {
						form.setAttribute("form-name",formname);
					}
			
					var borderclass	=	"";
					var thisarea		=	data[i]["Areas"];
					
					var arearow			=	dom.create('div',null,{
						className		:  "-UIContainer-area-row grid"
					});
												
					for(var j=0; j<thisarea.length; j++) { // there should be an "Area" key inside it - if it is building the UI not updating th eUI.
						var thisview			=	thisarea[j]["View"]; // if area contains "View" then we draw table tag
						var areaname			=	thisarea[j]["Name"];
						var borderkey			=	thisarea[j]["Border"]; // tells whether to add border on that index or not.
						var borderkeyclass	=	"-no-border";
						var thisnewarea		=	thisarea[j]["Areas"];
						var thisshowas      =  thisarea[j]["Show As"];
						if(borderkey) {
							borderkeyclass			=	"-row-border";
						}
						if(typeof thisview != "undefined") {
							var uidata			=	thisview;
							var viewtable		=	dom.create('table',arearow,{
								className				:  "-UIContainer-table"
							});
							
							var viewtbody		=	dom.create('tbody',viewtable,{
								className		:  "-UIContainer-table-body"
							});
							var viewtr			=	dom.create('tr',viewtbody,{
								className		:  "-UIContainer-table-tr"
							});
							form.appendChild(arearow);
							var conttoinsert	=	viewtr;
							
							var view				=	"Y";
							for(var k=0; k<thisview.length; k++) {
																var name			=	thisview[k]["Name"] || "";
								var type			=	thisview[k]["Type"];
								if(type == "Button") {
									dom.addClass(viewtable, borderkeyclass);
								}
								
																var element	=	that.drawTheUI(uidata,k,view);
								conttoinsert.appendChild(element);
							}
							
							if(thisshowas == "Row") {
							  var tds = dom.getByTag("td",conttoinsert);
							  
							  for(var h=0; h<tds.length; h++) {
							    tds[h].style.padding = "0px";
							    tds[0].style.width = "35%";
							  }
							}
						} else if (typeof thisnewarea != "undefined") {
							var formname	=	thisarea[j]["Name"];
								 isVisible	=	thisarea[j]["Visible"] || true;
							if(isVisible == "False") {
								formvisibility		=	"-hidden";
							} else {
								formvisibility		=	"-show";
							}
							
							var fclassName	=	formname.replace(/\s+/g,"-");
							 	 fclassName	=	fclassName.replace(/([~!@#$%^&*()_+=`{}\[\]\|\\:;'<>,.\/? ])+/g, '-');
							var subform			=	dom.create('div',form	,{
								className		:  "-UIContainer-area-forms grid "+fclassName+" "+formvisibility
							});
							
							subform.setAttribute("Code","");
							if(formname) {
								subform.setAttribute("form-name",formname);
							}
							
							var arearow			=	dom.create('div',null,{
								className		:  "-UIContainer-area-row grid"
							});
							
							for(var k=0; k<thisnewarea.length; k++) {
								var thisnewview			=	thisnewarea[k]["View"]; // if area contains "View" then we draw table tag
								var areanewname			=	thisnewarea[k]["Name"];
								var newborderkey			=	thisnewarea[k]["Border"]; // tells whether to add border on that index or not.
								var newborderkeyclass	=	"-no-border";
								var thisshowas      =  thisnewarea[k]["Show As"];

								if(newborderkey) {
									var newborderkeyclass			=	"-row-border";
								}
								
								if(typeof thisnewview != "undefined") {
									var uidata			=	thisnewview;
									var viewtable		=	dom.create('table',arearow,{
										className				:  "-UIContainer-table"
									});
									
									var viewtbody		=	dom.create('tbody',viewtable,{
										className		:  "-UIContainer-table-body"
									});
									var viewtr			=	dom.create('tr',viewtbody,{
										className		:  "-UIContainer-table-tr"
									});
									
									subform.appendChild(arearow);
									var conttoinsert	=	viewtr;
									
									var view				=	"Y";
									for(var l=0; l<thisnewview.length; l++) {
																				var name			=	thisnewview[l]["Name"] || "";
										var type			=	thisnewview[l]["Type"];
										if(type == "Button") {
											dom.addClass(viewtable, borderkeyclass);
										}
										
																				var element	=	that.drawTheUI(uidata,l,view);
										conttoinsert.appendChild(element);
									}
									
									if(thisshowas == "Row") {
									  var tds = dom.getByTag("td",conttoinsert);
									  
									  for(var h=0; h<tds.length; h++) {
										tds[h].style.padding = "0px";
										tds[0].style.width = "35%";
									  }
									}
																		
								} else {

									var uidata		=	thisnewarea;
									var conttoinsert	=	arearow;
									subform.appendChild(arearow);
									var view		=	"N";
									var element	=	that.drawTheUI(uidata,k,view,formname);
									if(element) conttoinsert.appendChild(element);
								}
									
							}
							
							
						} else {
						
							/*VV:06112016:
							if(typeof data[j]["Areas"] != "undefined") {							 
								for(var k=0; k<data[j]["Areas"].length; k++) {
										var uidata		=	data[j]["Areas"];
										var conttoinsert	=	arearow;
										form.appendChild(arearow);
										var view		=	"N";
										var element	=	that.drawTheUI(uidata,l,view,formname);
										conttoinsert.appendChild(element);
									
								}
							} else {
							 
								var uidata		=	thisarea;
								var conttoinsert	=	arearow;
								form.appendChild(arearow);
								var view		=	"N";
								var element	=	that.drawTheUI(uidata,j,view,formname);
								conttoinsert.appendChild(element);
							}
						*/								
							var uidata		=	thisarea;
							var conttoinsert	=	arearow;
							form.appendChild(arearow);
							var view		=	"N";
							var element	=	that.drawTheUI(uidata,j,view,formname);							
							conttoinsert.appendChild(element);							
						}						
						
					}
				} else if (thistype == "Widget") {
					
					var formname	=	data[i]["Name"],
					source	=	data[i]["Source"],
					rateStar;
				   
				   isVisible	=	data[i]["Visible"] || true;
					if(isVisible == "False") {
						formvisibility		=	"-hidden";
					} else {
						formvisibility		=	"-show";
					}
												
					var fclassName	=	formname.replace(/\s+/g,"-");
					 	 fclassName	=	fclassName.replace(/([~!@#$%^&*()_+=`{}\[\]\|\\:;'<>,.\/? ])+/g, '-');
					var form			=	dom.create('div',maincontenttoinsert	,{
						className		:  "-UIContainer-area-forms grid "+fclassName+" "+formvisibility
					});
					
					form.setAttribute("Code","");
					if(formname) {
						form.setAttribute("form-name",formname);
					}	
					var ratcont			=	dom.create('div',form,{
						className		:  "-UIContainer-area-row grid -right-aligned"
					});
																
					RunAjaxCall.runTheHTTPRequest({
						method	:	"GET",
						url		: source
					}).then(function(result){
						var htmldata = result.responseText;	
						if(htmldata) {					
							ratcont.innerHTML = htmldata;						
							rateStar = new runGetRatingsPlugin();
						}	
					});
													
				}
			}					 
			
			// Added for iOS only.
			if(typeof Bridge !== "undefined") {
				Bridge.Keyboardbuiduipaser(); 
			}
		}
				 
		//VV: 05202016:  Add Condition for If Container are found
		if(that.container) {			
			if(updatehtml == "Y") {
				if(that.enviornment != "extension") {
					that.container.style.height = "92vh";
				}	
			} else {
				dom.append(that.container , that.main_ui_container);
				that.container.style.height = "auto";
				that.AdjustMatrixaccordingToWidth();
			}
		} else {
			if(updatehtml == "Y") {
				var popup	=	dom.getById("-ui-parser");
				popup.style.height	=	"100%";
			} else {			
				LO._mCommonPopup("-ui-parser",that.main_ui_container,"N");
				/*history.pushState({"id":101}, window.title, CurrentObjectDRI+"/popup");*/
				that.AdjustMatrixaccordingToWidth();
			}
	
		}	
		
		// MD: Added these lines of code to set focus on first field.
		var inputs	=	dom.getByTag("input",that.main_ui_container).length;
		if(inputs > 0 || inputs > "0") {
			for(var a=0; a<inputs;a++) {
				if(typeof dom.getByTag("input",that.main_ui_container)[a] != "undefined") {
					var firstinput		=	dom.getByTag("input",that.main_ui_container)[a];
					if(firstinput.getAttribute("type") == "text") {
						firstinput.focus();
						return false;
					}
				}
			}
		}
	},
	
		// this only calls when event has update form action
	parseTheResponseAndUpdateHTML:	function(data,container){
		var that			=	this;

		container.innerHTML	=	"";
		var arearow			=	dom.create('div',container,{
			className		:  "-UIContainer-area-row grid"
		});
		
		for(var j=0; j<data.length; j++) {
			var thistype			=	data[j]["Type"];
			var thisview			=	data[j]["View"];
			var thisarea			=	data[j]["Areas"];
			var areaname			=	data[j]["Name"];
			var borderkey			=	data[j]["Border"];
			var background			=	data[j]["Background"] || '';
			var borderkeyclass	=	"-no-border";
			var thisshowas      =  data[j]["Show As"];
			
			
			if(borderkey) {
				borderkeyclass		=	"-row-border";
			}
						
			if(typeof thisview != "undefined") {
				var uidata			=	thisview;
				var viewtable		=	dom.create('table',arearow,{
					className				:  "-UIContainer-table"
				});
				
				var viewtbody		=	dom.create('tbody',viewtable,{
					className		:  "-UIContainer-table-body"
				});
				
				var viewtr			=	dom.create('tr',viewtbody,{
					className		:  "-UIContainer-table-tr"
				});
				
				var conttoinsert	=	viewtr;
				
				var view				=	"Y";
				for(var k=0; k<thisview.length; k++) {
										var name			=	thisview[k]["Name"] || "";
					var type			=	thisview[k]["Type"];
					if(type == "Button") {						
						dom.addClass(viewtable, borderkeyclass);
						if(background) {			 
							dom.addClass(viewtable, "secondary-background");			  
						} 																								
					}
										var element	=	that.drawTheUI(uidata,k,view);
					conttoinsert.appendChild(element);
				}
				
				if(thisshowas == "Row") {
				  var tds = dom.getByTag("td",conttoinsert);
				
				  for(var h=0; h<tds.length; h++) {
				    tds[h].style.padding = "0px";
				    tds[0].style.width = "35%";
				  }
				}
			}  else {
				var uidata		=	data;
				var conttoinsert	=	arearow;
				var view		=	"N";
				var element	=	that.drawTheUI(uidata,j,view);
				conttoinsert.appendChild(element);
			}
			
			
		}

	},
	
	// this is the most important method, which draw the areas (fields/buttons/rows etc..) accoring to the "Type" key
	/*VV:06012016: for the matrix icon change comment and add parameter 
		drawTheUI:	function(data,k,view){		
	*/
	
	drawTheUI:	function(data,k,view,dataName){		
		var that 					=	this;
		var name						=	data[k]["Name"] || data[k]["Write to"] || data[k]["Label"] || "", // this is the unique key or each index.
			 type						=	data[k]["Type"], // this the very important key - as all the indexes draws based on this key - field/button/image/static text etc..
			 style					=	data[k]["Style"] || "",
 			 color					=	data[k]["Color"] || "",// this is for the color 
			 icon						=	data[k]["Icon URL"],
			 icontype				=	data[k]["Icon Type"] || "Image",
			 iwidth					=	data[k]["Width"],
			 iheight					=	data[k]["Height"],
			 value					=	data[k]["Value"] || "",
			 eventss					=	data[k]["Events"],
			 label					=	data[k]["Label"] || data[k]["Formatted Name"], // label of the field
			 prompt					=	data[k]["Prompt"] || data[k]["Unit"] || "" , // placeholder
			 allowblank				=	data[k]["Allow Blank"], // is required			 
			 entrytype				=	data[k]["Entry Type"],
			 fieldtype				=	data[k]["Field Type"], // type of the field - address/date/distance/number etc..
			 supports				=	data[k]["Supports"],
			 className				=	name.replace(/\s+/g,"-"),
			 className				=	className.replace(/([~!@#$%^&*()_+=`{}\[\]\|\\:;'<>,.\/? ])+/g, '-'),
			 crossbutton_vi		=	"N",
			 checkboxclass			=	"",
			 extraclass				=	"",
			 dynamic_class			=	"",
			 fieldunit				=	data[k]["Unit"], // unit of that field
			 fieldsourcetype		=	data[k]["Source Type"], // source type of the field - static || LiveStuff Cloud || LiveStuff Object.
			 fieldsource			=	data[k]["Source"], // source of the field to draw the dropdown options - if static - should have || separated values init. if LS cloud must have url of a ls cloud
			 fieldedittype			=	data[k]["Edit Type"], // defines the edit type of the field
			 fieldallowedit		=	data[k]["Allow Edit"], // defines whether the field is editable or not
			 fieldallowadd			=	data[k]["Allow Add"], // defines whether we can add option into the the source cloud defined
			 fieldisselection		=	data[k]["Is Selection"],
			 fieldselectionstype	=	data[k]["Selection Type"], // defines the selection type of field - dropdown/default
			 fieldinterfacetype	=	data[k]["Interface Type"],
			 fieldselectionlines	=	data[k]["Selection Lines"], // defins the selection lines - height of th edropdown
			 fieldmaxselections	=	data[k]["Maximum Selections"], // defines the max selection - if multiselection field
			 fielddatetype			=	data[k]["Date Type"] || "date", // defines the date type of the fiels if field type is date - month only/year only/date + month etc..
			 fieldentrylines		=	data[k]["Entry Lines"] || 1, // Defines that field would be a textarea if > 1.
			 fieldmaxinput			=	data[k]["Max Input"] || 250, // defines the max character int he input - mostly for text areas
			 fieldusage				=	data[k]["Field Usage"], // defines the field usage - core/standard/item/owner.
			 fieldcode				=	data[k]["Code"] || data[k]["ID"] || data[k]["Id"] || "",
			 fielddri				=	data[k]["DRI"] || data[k]["URL"] || "",
			 alignment				=	data[k]["Alignment"],
			 nameclassname			=	"",
			 actualfieldtype    =  data[k]["Field Type"],
			 delimiter          =  data[k]["Delimeter"] || {},
			 delimiterfor       =  delimiter ? delimiter.For : "";
			 delimiterreplacewith  =  delimiter ? delimiter["Replace With"] : "";

			 
		if(label) {	
			nameclassname			=	label.replace(/\s+/g,"-");
	   	nameclassname			=	nameclassname.replace(/([~!@#$%^&*()_+=`{}\[\]\|\\:;'<>,.\/? ])+/g, '-');	 
	   }
	   
		// Added this line of code as from server getting "" for null values and 0 for dates	 
		if(value == "" || value == 0) {
			value	=	"";
		}	 
		if(dataName == undefined){
		   dataName = "";
		}
 
		if(fieldtype == "Dropdown") {
			var tagtype		=	"select";
			var inputtype	=	"";
		} else if(fieldtype == "Entry" || fieldtype == "input" || fieldtype == "Quicksearch" || fieldtype == "Text" || fieldtype == "Address") {
			if(fieldentrylines > 1) {
				var tagtype		=	"textarea";
				var inputtype	=	"";
				crossbutton_vi		=	"Y";			   
			} else {
				var tagtype		=	"input";
				var inputtype	=	"text";
				crossbutton_vi		=	"Y";
			}
		} else if(fieldtype == "flag" || fieldtype == "Flag") {
			var tagtype		=	"input";
			var inputtype	=	"checkbox";
			checkboxclass	=	"-checkbox";
			extraclass		=	"-collapse";
		} else if(fieldtype == "Multi-Line Text") {
			var tagtype		=	"textarea";
			var inputtype	=	"";
		} else if(fieldtype == "Uploader") {
			var tagtype		=	"div";
			var inputtype	=	"";
		} else if(fieldtype == "File") {
			var tagtype		=	"div";
			var inputtype	=	"";
		} else {
			var tagtype		=	"input";
			var inputtype	=	"";
			crossbutton_vi		=	"Y";
		}
		
		var datearray;
		var timearray;
		// For date and time fields
		if(fieldtype == "Date") {
			if(fielddatetype == "date" || fielddatetype == "Date" || "") {
				var inputtype	=	"date";

			} else if(fielddatetype == "Date and Time") {
				var inputtype	=	"datetime-local";
			} 
			
			/*value	=	"42530.386806";*/
			if(value == 0 || typeof value == "undefined" || value == "") {
			} else {
				value	=	utils.floatToUnix(value);
				if(fielddatetype == "date" || fielddatetype == "Date" || "") {
					value	=	moment(value).format("YYYY-MM-DD");
					datearray	=	value;							
				} else {				
					value   	 =	moment(value).format("YYYY-MM-DD HH:MM");
					datearray = value;
				}								
			}
		} else if(fieldtype == "Time") {			
			var inputtype	=	"time";			
			/*value	=	"42530.386806";*/
			if(value == 0 || typeof value == "undefined" || value == "") {
			} else {
				value	=	utils.floatToUnix(value);
				if(fielddatetype == "Time" || fielddatetype == "Date and Time" || "") {
					value	=	moment(value).format("HH:MM");
					datearray	=	value;										
				}			
				
			}			
			
		}
		
		if(fieldtype == "Date" || fieldtype == "Number" || fieldtype == "Distance" || fieldtype == "Length" || fieldtype == "Currency") {
			var textalignclass	=	"-text-align-right";
		}
		
		
		/* Logic:
			case 1 : if "Source Type" == "LiveStuff Cloud" - will draw field as QuickSearch - and will call SearchItems.do - to draw the options
			case 2 : if "Source Type" == "LiveStuff Object" - will draw field as QuickSearch - and will call SearchObjects.do - to draw the options
			case 3 : if "Source Type" == "LiveStuff Object" / "Livestuff Object" and "Selection Type" == "Dropdown" - will draw field as Select - and will call getitems.do - to draw the options
			case 4 :	if "Source Type" == "Static" - draw the option from || separated values in source.
		*/
		
		if(fieldsource) {
			if(fieldsourcetype == "Static" || fieldsourcetype == "URL") { // if source type is static draw the select
				var tagtype		=	"select";
				var inputtype	=	"";
			} else { // if source type is ls cloud or ls object - field should be quicksearch - as we will call searchitems/searchobjects accordingly on source cloud
				if(fieldselectionstype == "Dropdown" || fieldinterfacetype == "Dropdown") {
					var tagtype			=	"select";
					var inputtype		=	"";
				} else {
					var tagtype		=	"input";
					var inputtype	=	"text";
					crossbutton_vi	=	"Y";
					fieldtype 		=	"Quicksearch";
				}

			}

		}
		
		
		if(view == "Y") {
			var elementtocreate	=	"td";
		} else {
			var elementtocreate	=	"div";
		}
		
		/*MD: Commented
					if(type == "Core" || type == "Standard" || type == "Item" || type == "Owner"){
				type	=	"Field";
			}
				*/
		if(nameclassname == className) {
			className = "";
		}
		var viewtd		=	dom.create(elementtocreate,null,{
			className	:	"-UIContainer-table-td "+className+" "+nameclassname
		});
		
		if(alignment) {
			if(alignment == "Right") {
				dom.addClass(viewtd,"-text-align-right");
			} else if(alignment == "Left") {
				dom.addClass(viewtd,"-text-align-left");
			} else {
				dom.addClass(viewtd,"-text-align-center");
			}
		}
				
		viewtd.setAttribute("data-datatype",name);
		viewtd.setAttribute("data-fieldusage",fieldusage);
		viewtd.setAttribute("field-name",label);
		// MDubey : 08/05/2016 Added the below code to add support for delimiter
		
		if (delimiter) {
		   viewtd.setAttribute("delimiter",true);
		   viewtd.setAttribute("delimiterfor",delimiterfor);
		   viewtd.setAttribute("delimiterreplacewith",delimiterreplacewith);
		}
		
		
		if(iwidth || iheight) {
			viewtd.style.width		=	iwidth+"px";
			viewtd.style.height		=	iheight+"px";
		}

		viewtd.setAttribute("Code",fieldcode);
		viewtd.setAttribute("DRI",fielddri);
		
		viewtd.setAttribute("id",fieldcode);
		viewtd.setAttribute("url",fielddri);
		viewtd.setAttribute("field-value",value);
		viewtd.setAttribute("field-type",actualfieldtype);
		
		if(type == "Image") {
			var imagediv	=	dom.create("div",viewtd,{
				className	:	"-icon-img-div"
			});
			
			if(iwidth || iheight) {
				imagediv.style.width		=	iwidth+"px";
				imagediv.style.height	=	iheight+"px";
			}
			
			if(icontype == "SVG" || icontype == "Svg" || icontype == "svg") {
				var iconname 	= icon.split('_');
				var view2		=	dom.createSVG(imagediv, iconname[0]);
				view2.style.width		=	iwidth+"px";
				view2.style.height	=	iheight+"px"; 				
			} else {
				var view2		=	dom.create("img",imagediv,{
					className	:	"-UIContainer-row-column-img",
					src			:	that.globaldomain+icon
				});
			}
		}
		

		if(type == "Static Text") {
			if(style == "label" || style == "Label") {
				style	=	"";
			}
			
			var view2		=	dom.create("div",viewtd,{
				className	:	"-UIContainer-row-column-text "+style,
				innerHTML	:	value
			});
		}
		
		if(style) {
			dynamic_class	=	style.toLowerCase();
		}
		
	
		if(type == "Button" ) {
			if(name == "" || name == undefined || typeof name == "undefined") {
				var borderclass	=	"";
			} else {
				var borderclass	=	"tertiary-border";
			}
			
			var imagediv	=	dom.create("div",viewtd,{				 
				className	:	"-icon-img-div -click -right-aligned",				 
			});
			
			if(iwidth || iheight) {
				imagediv.style.width		=	iwidth+"px";
				imagediv.style.height	=	iheight+"px";
			}
			
			if(icontype == "SVG") {
				var iconname 	= icon.split('_');	 
				var view2		=	dom.createSVG(imagediv, iconname[0]);
				view2.style.width		=	iwidth+"px";
				view2.style.height	=	iheight+"px";				
			} else {
				var view2		=	dom.create("img",imagediv,{
					className	:	"-UIContainer-row-column-img",
					src			:	that.globaldomain+icon,
					"alt"			:	name
				});
			}
			
			//VV: 06012016: Matrix icon Change 
			if(dataName == 'Matrix Form' ) {
				/*VV:08172016; Remove background color classes -background-color,-primary*/
				dom.addClasses(imagediv,['primary-border','-icon-matrix-div']);
				var titlediv	=	dom.create("div",imagediv,{				 
					className	:	"-color -primary bold",
					innerHTML	:	name				 
				});
			} 
			
			if(eventss) {
								viewtd.addEventListener("click", function(event){
					that.handleEvents(eventss,event);
				});
				
							}
		}
			
		if(type == "Label") {
		
			var view2		=	dom.create("div",viewtd,{
				className	:	"-UIContainer-row-column-label-column -only-label "+dynamic_class,
				innerHTML	:	label || name
			});
			
			dom.addClass(viewtd,"-only-label-div");

		}
		
		if(color) view2.style.color = color;
				
		if(typeof type != "undefined") {

			//VV:06272016: Link type support
			if(type == "Link") {
			
				var alignclass = "-"+alignment+"-aligned";
				var inputtag	=	dom.create("span",viewtd,{
					className	:	"primary-color -link-button "+alignclass,
				});
				inputtag.innerHTML = name;
				
								inputtag.addEventListener("click", function(event){
					that.handleEvents(eventss,event);
				});
			
			} 
			
			if(type == "Field") {
				
				/*
				if(fieldtype == "Quicksearch") {
					var backgroundicon	=	"search-icon";
				} else if(fieldtype == "Address") {
					var backgroundicon	=	"map-icon";
				} else {
					var backgroundicon	=	"";
				}
				*/
				
				if(fieldtype == "Quicksearch") {
					var backgroundicon	=	"Search";
				} else if(fieldtype == "Address") {
					var backgroundicon	=	"Location";				
				}	/*else if(fieldtype == "Date") {
					var backgroundicon	=	"Calendar";
				}*/ else {
					var backgroundicon	=	"";
				}
				
				if(typeof label != "undefined") {
					var nolabelclass	=	"";
					var labelview	=	dom.create("label",viewtd,{
						className	:	"-UIContainer-row-column-label "+dynamic_class,
						innerHTML	:	label || name
					});
				} else {
					var nolabelclass	=	"-onlyfield";
				}	
				

				var inputdiv	=	dom.create("div",viewtd,{
					className	:	"-UIContainer-row-column-input-div "+nolabelclass+" "+checkboxclass,
				});
				
				var inputcontainer	=	inputdiv;
				
				if(fieldtype == "flag" || fieldtype == "Flag") {
					var checkboxlabel	=	dom.create("label",inputdiv,{
						className		:	"",
					});
					
					
					inputcontainer	=	checkboxlabel;
				}
				/*VV:06172016: Old date field format
				if(fieldtype == "Date") {
					dom.addClasses(inputcontainer,['grid','-left-to-right']);
					dom.addClass(viewtd,'date-field');
					var inputtag		=	dom.create(tagtype,inputcontainer,{
						className	:	"-hidden"
					});
					
					if(fielddatetype == "Date and Time") {
						dom.addClass(viewtd,'time-field');
						var inputtagdd		=	dom.create(tagtype,inputcontainer,{
							className	:	"-UIContainer-row-column-input -date-input grid__column -two-xs"+extraclass+" "+textalignclass,
							placeholder	:	"hh",
							type			:	"text"
						});
						
						inputtagdd.setAttribute("maxlength",2);
						if(timearray) {
							inputtagdd.value	=	timearray[0];
						}
						var dashdd		=	dom.create("span",inputcontainer,{
							className	:	"grid__column -one-xs -middle-aligned -date-splitter",
							innerHTML	:	"-"
							
						});
						
						
						var inputtagmm		=	dom.create(tagtype,inputcontainer,{
							className	:	"-UIContainer-row-column-input -date-input grid__column -two-xs"+extraclass+" "+textalignclass,
							placeholder	:	"mm",
							type			:	"text"
						});
						
						inputtagmm.setAttribute("maxlength",2);
						if(timearray) {
							inputtagmm.value	=	timearray[1];
						}
						var dashmm		=	dom.create("span",inputcontainer,{
							className	:	"grid__column -one-xs -middle-aligned -date-splitter",
							innerHTML	:	"-"
						});
						
						var inputtagyyyy		=	dom.create(tagtype,inputcontainer,{
							className	:	"-UIContainer-row-column-input -date-input grid__column -six-xs"+extraclass+" "+textalignclass,
							placeholder	:	"AM/PM",
							type			:	"text"
						});
						
						inputtagyyyy.setAttribute("maxlength",2);
						if(timearray) {
							inputtagyyyy.value	=	timearray[2];
						}

					} else {
						
						var inputtagdd		=	dom.create(tagtype,inputcontainer,{
							className	:	"-UIContainer-row-column-input -date-input grid__column -two-xs"+extraclass+" "+textalignclass,
							placeholder	:	"mm",
							type			:	"text"
						});
						
						inputtagdd.setAttribute("maxlength",2);
						if(datearray) {
							inputtagdd.value	=	datearray[0];
						}
						var dashdd		=	dom.create("span",inputcontainer,{
							className	:	"grid__column -one-xs -middle-aligned -date-splitter",
							innerHTML	:	"-"
							
						});
						
						
						var inputtagmm		=	dom.create(tagtype,inputcontainer,{
							className	:	"-UIContainer-row-column-input -date-input grid__column -two-xs"+extraclass+" "+textalignclass,
							placeholder	:	"dd",
							type			:	"text"
						});
						
						inputtagmm.setAttribute("maxlength",2);
						if(datearray) {
							inputtagmm.value	=	datearray[1];
						}
						var dashmm		=	dom.create("span",inputcontainer,{
							className	:	"grid__column -one-xs -middle-aligned -date-splitter",
							innerHTML	:	"-"
						});
						
						var inputtagyyyy		=	dom.create(tagtype,inputcontainer,{
							className	:	"-UIContainer-row-column-input -date-input grid__column -six-xs"+extraclass+" "+textalignclass,
							placeholder	:	"yyyy",
							type			:	"text"
						});
						
						inputtagyyyy.setAttribute("maxlength",4);
						if(datearray) {
							inputtagyyyy.value	=	datearray[2];
						}
					}
					
				} else {
					var inputtag		=	dom.create(tagtype,inputcontainer,{
						className	:	"-UIContainer-row-column-input -background-icon "+extraclass
					});
				}
				*/
				var inputtag		=	dom.create(tagtype,inputcontainer,{
					className	:	"-UIContainer-row-column-input -background-icon "+extraclass,
				});
				
							
				if(backgroundicon !== "") {
					var inputtagicon		=	dom.create("div",inputcontainer,{
						className			:	"-UIContainer-row-column-input-icons -input-icons grid -middle-aligned"
					});
					
					var inputtagicon		=	dom.createSVG(inputtagicon, backgroundicon);
					dom.addClass(inputtag, "-input-with-icon");
				}
				
				if(fieldentrylines > 1){
					inputtag.setAttribute("rows",fieldentrylines);
					inputtag.setAttribute("maxlength",fieldmaxinput);
				}
				
				if(fieldtype == "flag" || fieldtype == "Flag") {
					var tempspan		=	dom.create("span",checkboxlabel,{
						className		:	"",
					});
				}
				
				if(fieldtype == "Uploader" || fieldtype == "File") {
					var isfile = false;
					if(fieldtype == "File"){
						isfile = true;
					}
					var inputt		=	dom.create("div",inputtag,{
						className	:	"-UIContainer-row-column-upload secondary-background -click",
					});
					
					var labelfile		=	dom.create("label",inputt,{
						className	:	"-UIContainer-row-column-upload-label -click",
					});
					
					var inputt_text	=	dom.create("div",labelfile,{
						className		:	"-vertical-align -upload-text",
						innerHTML		:	prompt
					});
					
					var fileinput	=	dom.create("input",labelfile,{
						className	:	"-UIContainer-row-column-upload-input -hidden",
					});
					
					fileinput.setAttribute("type","file");
					fileinput.setAttribute("data-inputtype","file");
					fileinput.setAttribute("accept","image/&#42");
					
					
					fileinput.addEventListener("change", function(event){											
						that.getTheChangeEventData(event,false,eventss,fieldtype);
					});
					
					dom.addClass(inputtag,"-uploader");
					dom.addClass(inputdiv,"-uploader-container");
					
					// MD: Added drop functionality - temporarily used addeventlistener
					inputtag.addEventListener("dragover", function( event ) {
				      event.preventDefault();
				   }, false);
				   
					inputtag.addEventListener("drop", function(event){
						event.preventDefault();														
						if(event && event.dataTransfer && event.dataTransfer.files && event.dataTransfer.files.length > 0){							
							that.getTheChangeEventData(event,true,eventss,fieldtype);
						} else {
							that.handleFileSelectFromURL(event,true,eventss,fieldtype);
						}	
											
					},false);

				} 
				
				/*VV:06172016: Remove static close icon
				if(crossbutton_vi == "Y") {
					var crossicon	=	dom.create("div",viewtd,{
						className	:	"-UIContainer-row-column-cross -click",
					});
					
					// MD: Cross icons
					var inputtagcrossicon		=	dom.createSVG(crossicon, "X");
					
					
					crossicon.addEventListener("click", function(event){
						that.clearFieldData(eventss,event);						
					});

				}				
				*/
								inputtag.setAttribute("data-inputtype",fieldtype);
				inputtag.setAttribute("type",inputtype);
				inputtag.setAttribute("allow-blank",allowblank);


				if(allowblank == "N") {
					inputtag.setAttribute("required",true);
				}
				
				if(fieldtype == "Dropdown") {
					if(value){
						var optionvalue	=	value;						
					}else{
						var optionvalue	=	prompt;
					}
					
					var option		=	dom.create("option",inputtag,{
						className	:	"dropdown-option",
						innerHTML	:	optionvalue						
					});
					option.setAttribute("selected","selected");
			
				} else if(fieldtype == "Date" || fieldtype == "Time") {					
					if(datearray) inputtag.value = datearray;
				} else {
					inputtag.setAttribute("placeholder",prompt);
					if(fieldtype == "Flag" && value == "Y") {
					
						inputtag.checked = true;
					} else {
						inputtag.value = value;
					}
				}
								if(fieldsource && fieldsourcetype) {
					if(fieldsourcetype == "Static" || fieldsourcetype == "URL" || fieldsourcetype == "Url") {
						that.CreateTheSourceDropdownOptions(inputtag,fieldsource,fieldsourcetype,value,false,fieldallowedit);	
					} else {
						if(fieldselectionstype == "Dropdown" || fieldinterfacetype == "Dropdown") {
							that.CreateTheSourceDropdownOptions(inputtag,fieldsource,fieldsourcetype,value,true,fieldallowedit);
						}
					}
				}
				
				if(eventss) {					 
										inputtag.addEventListener("click", function(event){
						that.handleEvents(eventss,event);
					});
					
					inputtag.addEventListener("keyup", function(event){
					 		
														clearTimeout(this.ajaxrequest);
							this.ajaxrequest = setTimeout ( function() { 
									that.handleEvents(eventss,event,supports);								
							}, 400 );

					});
					/*VV:07222016: Update this code below
					inputtag.addEventListener("blur", function(event) {						
						that.handleEvents(eventss,event);
					});
					*/
					
									}	else	{				
										if(fieldtype == "Multi-Line Text") {
						inputtag.addEventListener("keyup", function(event){
							that.tallyItemCount(event);
						});
					} else if(fieldsourcetype == "LiveStuff Cloud" || fieldsourcetype == "LiveStuff Object" || fieldsourcetype == "Enterprise Repository") {
						inputtag.addEventListener("keyup", function(event){							
							that.CreateTheSourceDropdownOptions(inputtag,fieldsource,fieldsourcetype,value,false,fieldallowedit);
						});
						
						/*VV:08192016: Support Allow Add*/
						if(fieldallowadd == "Y") {							
						  inputtag.addEventListener("blur", function(event){
						  		that.fieldallowadd(inputtag,fieldsource,fieldsourcetype,value,false,fieldallowedit,label);
						  });
						}
					}
						
					inputtag.addEventListener("keyup", function(event) {
						var elem					=	event.target;
						var elemparent			=	elem.parentNode;
						var errormessagediv	=	dom.getByClass("-error-msg-div",elemparent)[0];
						if(typeof errormessagediv !== "undefined") { 
							//VV:06222016: IE Support errormessagediv.remove();
								dom.remove(errormessagediv);							
						}
					});
					
					
									}				

				inputtag.addEventListener("blur", function(event) {						
					
					if(eventss)	that.handleEvents(eventss,event);
					var ftype = this.getAttribute("data-inputtype");					
					if(this.type == 'text' && fieldunit && this.value && ftype !="Date" && ftype !="Time") {										
						var inputVal = this.value;
						inputVal 	 = inputVal.replace(" "+fieldunit,"");
						this.value   = inputVal +" "+fieldunit; 
					}	
										
				});
					
									if(fieldtype != "Dropdown" && fieldtype != "Date" && fieldtype != "Time" && fieldinterfacetype != "Dropdown" && fieldtype != "Multi-Line Text" && fieldedittype !="Multiple Selection" && fieldselectionstype != "Dropdown" && fieldsourcetype !="Static") {

						events.add(inputtag,'keyup', function(event) {
							var elem					=	event.target;
							that.clearFields(elem,event.keyCode,eventss);							
						});
						
						events.add(inputtag.parentNode,'mouseenter', function(event) {						   
							 var elem = dom.getEl('input[type="text"]',this);
							 if(elem  == null || !elem){
								 elem = dom.getEl('textarea',this)								 
							 }
							 that.clearFields(elem,event.keyCode,eventss);							 
						});
						
						events.add(inputtag.parentNode,'mouseleave', function(event) {
						  var elem = event.target;							
						  var iconelem = dom.nextSibling(elem);						 
						  dom.getByClass('-cross-icon',iconelem).forEach(function(el) {												
								var prevelem = dom.previousSibling(el);
								if(prevelem) dom.removeClass(prevelem,'gu-hide');
								dom.remove(el);
							});
							
						});						
					}
					
			 						events.add(inputtag,'focus', function(event) {				 
				 	var elem					=	event.target;
				 	/*Bridge.virtualKeyboard();*/
				 	if( fieldtype =='Address') {
				 	
			 			 if(that.enviornment == "extension") {							 			 			 
 							var gapiId  	= "gapi"; 							 							
					 		if (!document.getElementById(gapiId)){														
								}else{									
									var gapichild = document.getElementById(gapiId);
									for ( var i = 0;i < 10;i++ ) {
										var ochild = gapichild.nextSibling;
										if	(ochild) {
											ochild.parentNode.removeChild(ochild);	
										}
									}
									gapichild.parentNode.removeChild(gapichild);									 
								} 
						  var head		= document.getElementsByTagName('head')[0];
							var script	= document.createElement('script');
							script.type	= 'text/javascript';
							script.id 	= gapiId;
							script.src	= '//maps.google.com/maps/api/js?key=AIzaSyCvjy88ojuo5b1mJdx7qvxTO0gRBJHqCoU&callback=initMap';
							head.appendChild(script);
																														 	    																				
						} else {							
							that.CreateMapandStaticFields(elem);
						}				 									 		
 
				 	}else{
		 						 				var mapelem = dom.getByClass('map_canvas_address');
						if(mapelem.length > 0) {
							dom.removeClass(mapelem[0],'-show');
							dom.addClass(mapelem[0],'-hidden');
						}
						
						
				 	}					 	 					 	
				 	
					if(fieldtype != "Dropdown" && fieldtype != "Date" && fieldtype != "Time" && fieldinterfacetype != "Dropdown" && fieldtype != "Multi-Line Text" && fieldedittype !="Multiple Selection" && fieldselectionstype != "Dropdown" && fieldsourcetype !="Static") {					 	
					 	that.clearFields(elem,event.keyCode,eventss);
					}	
					
				 });
				 
				// To set the browser's tab name as name in input for extension
				if(that.enviornment == "extension" || that.enviornment == "External Apps") {
					if(name == "Name") {
						if(value || value != "") {							
						} else {							
							inputtag.value		=	decodeURIComponent(that.title);														
						}
						dom.addClass(inputtag,'padright');
						that.clearFields(inputtag,'',eventss);
					}
					
					if(name == "Category") {
						if(that.catobj) {
							var catid	=	that.catobj.CategoryId;
							var catname	=	that.catobj.CategoryName;
							var catdri	=	that.catobj.CategoryDRI;
							inputtag.value		=	decodeURIComponent(catname);
							viewtd.setAttribute("id",catid);
							viewtd.setAttribute("code",catid);
							viewtd.setAttribute("dri",catdri);
							viewtd.setAttribute("url",catdri);
						}
					}
				}
				 
			}
		}
		
		return viewtd;
	},
	fieldallowadd: function(elem,source, sourcetype,elemvalue,isDropdown,isAllowEdit,label){
		var that			=	this;
		var elemval		=	elem.value;
		var _searchUrl ="",_addUrl="";
		if(sourcetype== "LiveStuff Cloud" || sourcetype== "Enterprise Repository") {			
			if(source.indexOf("http") > -1 || source.indexOf("https") > -1) {
				source	= source.replace("http://","").replace("https://","");
			} else {
				source	= source;
			}
			source		= source.substring(source.indexOf("/"), source.length);					
		
			if(sourcetype == "LiveStuff Cloud") {			
			
				_addUrl    = source+"/CreateItem.do?Name="+elemval;
				_searchUrl = source+"/SearchItems.do?Search="+elemval;
			
			} else if(sourcetype == "Enterprise Repository") {
			
				_addUrl		 =	source+"/CreateObject.do?Name="+elemval;
				_searchUrl	 = source+"/SearchItems.do?Search="+elemval;
			
			}
			
			if(_searchUrl !="") {				
				RunAjaxCall.runTheHTTPRequest({
					method	:	"GET",
					url		: _searchUrl
				}).then(function(data){
					data					=	JSON.parse(data.responseText);
					data					=	data["Results"] || data["Result"] || data;				
					if(data == "") {
											
							var message = '<span class="primary-color">'+elemval+'</span> does not exist,</br> would you like to add it to <span class="primary-color">'+label+'</span> options?'; 
												
							var modalParent = dom.getById('mainContent') || document.body,
						        msgModal = modal.create({
						          id: 'addfieldmsg',
						          title: 'Add',
						          parent: modalParent
						        }),					        
						        body = dom.getByClass('modal__body', msgModal)[0],
						        container, options, no, yes, message;

						    body.innerHTML = '';
						    
						    container = dom.create('div', body, {className: 'grid -top-to-bottom'});					    

						    dom.create('div', container, {
						      className: 'grid__column',
						      innerHTML : message 
						    });												 
						 options = dom.create('div', container, {className: 'grid -left-to-right -middle-aligned'});   
						 no = dom.create('button', options, {
						    className: 'reorder__no grid__column',
						    innerText: 'No'
						 });    
						 yes = dom.create('button', options, {
						    className: 'reorder__yes grid__column',
						    innerText: 'Yes'
						 });
						
						events.add(yes, 'click', function(_Url,msgModal, e) {
							utils.showLoader({"message":"Adding..."});							
							if(_Url) {					
								RunAjaxCall.runTheHTTPRequest({
									method	:	"POST",
									url		: _Url
								}).then(function(data){
										data					=	JSON.parse(data.responseText);
										data					=	data["Results"] || data["Result"] || data;										
										modal.close(msgModal);
										utils.hideLoader();
								});
							}	
																																																						
						}.bind(this, _addUrl,msgModal));
						
					   events.add(no, 'click', function(msgModal, e) {
					      modal.close(msgModal);
					   }.bind(this, msgModal));						

						modal.open(msgModal); 
					 	
					}
				});
				
			}
			
		}
	},
	 
	clearFields: function(elem,keycode,eventss) {	 			 			
	   	var that = this;
	      var elems 			= elem;	      
	      
	      if(!elems || !elems.value) return false;
	      
   		var chalength 		= elems.value.length;
			var elemdatatype	= elems.getAttribute("data-inputtype");	
			
			dom.getByClass('-cross-icon',dom.getByClass('-UIContainer')[0]).forEach(function(el) {				
				var prevelem = dom.previousSibling(el);				
				if(prevelem) dom.removeClass(prevelem,'gu-hide');
				dom.remove(el);
			});			
			
			if(elemdatatype !='Date' || elemdatatype !== 'Multi-Line Text')  {
			
				if(keycode==0 && chalength == 0) { 
					chalength +=1; 
				} else if (keycode==8 && chalength == 1) {
					chalength -=1;
				}				
				if(chalength > 0) {
					var subelem		= dom.nextSibling(elems);
										
					if(!subelem || dom.hasClass(subelem, '-error-msg-div')) {	  			 				 
						var subelem	  	= dom.create("div",elems.parentNode,{
							className			:	"-UIContainer-row-column-input-icons -input-icons grid -middle-aligned"
						}); 				  		
					}
					  		
					if (dom.hasClass(elems , '-input-with-icon')) {
						dom.addClass(subelem.childNodes[0],'gu-hide');
						dom.remove(subelem.childNodes[1]);  		 				  			 				
					}else{
						dom.remove(subelem.childNodes[0]);  		 				  			 				  		 				  
					}
				   
				   var closeicon		=	dom.createSVG(subelem, 'X');
				   dom.addClass(closeicon,"-cross-icon");
				   dom.addClass(closeicon,"-click");
					
					events.add(closeicon,'click',function(e) {
						elems.value = '';
						that.clearFieldData(eventss,e);					   
						if (dom.hasClass(elems , '-input-with-icon')) {							
							dom.removeClass(subelem.childNodes[0],'gu-hide');
							dom.remove(subelem.childNodes[1]);
						}else{
						   dom.remove(subelem.childNodes[0]);  	
						}							
					});			
					
				} else {
					var subelem		= dom.nextSibling(elems);
					if(subelem && subelem.childNodes[0] && subelem.childNodes[1]) {	 												
						dom.removeClass(subelem.childNodes[0],'gu-hide');
						dom.remove(subelem.childNodes[1]);
					}	
				}
			}	
	},
		CreateTheSourceDropdownOptions:	function(elem,source, sourcetype,elemvalue,isDropdown,isAllowEdit){
		var that			=	this;
		var elemval		=	elem.value;
		var elemparent	=	elem.parentNode;
		var errormessagediv	=	dom.getByClass("-error-msg-div",elemparent)[0];
		if(typeof errormessagediv !== "undefined") {			
			dom.remove(errormessagediv);
		}	
		
		if(sourcetype == "Static") {
			var data	=	source.split("||");
			if(!elemvalue) {
				var islabel = dom.previousSibling(elem.parentNode);				 
				//VV:addon changes
				if(!dom.hasClass(islabel,"-UIContainer-row-column-label")) {					
					var options		=	dom.create("option",elem,{
						className	:	"dropdown-option",
						innerHTML	:	"Select Option"
					});
				}
			}
			
			var namee,options;
			for(var i=0; i<data.length; i++) {
				namee				=	data[i];
				
				options			=	dom.create("option",elem,{
					className	:	"dropdown-option",
					innerHTML	:	namee
				});
				
				if(elemvalue == namee) {
					options.setAttribute("selected","selected");
				}

			}
		}else if( sourcetype == "URL" ) {
			RunAjaxCall.runTheHTTPRequest({
				method	:	"GET",
				url		: source
			}).then(function(data){
				data					=	JSON.parse(data.responseText);
				data					=	data["Results"] || data["Result"] || data;
				if(data && data != "") {

					if(!elemvalue) {
						if(isAllowEdit == "Y") {
							var options		=	dom.create("option",elem,{
								className	:	"dropdown-option",
								innerHTML	:	"Select Option"
							});
						}
					}
					
					var namee,options,id,dri,fieldindex;

					for(var i=0; i<data.length; i++) {
					   
					  /*VV:06282016: Add || for the get names*/					  
						if(data[i]["Object"]) {
							fieldindex				=  data[i]["Object"] || data[i]["Intersection"];
							fieldindexfield		= fieldindex["Fields"] ? fieldindex["Fields"] : fieldindex;
						} else {
							fieldindex		=  data[i];
						}	
						namee				=	data[i]["Name"] || fieldindex["Formatted Name"] || fieldindex["Name"] || fieldindexfield["Formatted Name"] || '';						
						id					=	data[i]["Code"] || data[i]["Id"] || fieldindex["Id"] || fieldindexfield["id"] || '';
						dri				=	data[i]["Direct Resource Identifier"] || data[i]["dri"] || data[i]["URL"] || fieldindex["Direct Resource Identifier"] || '';
						
						options			=	dom.create("option",elem,{
							className	:	"dropdown-option",
							innerHTML	:	namee
						});
						
						options.setAttribute("id",id);
						options.setAttribute("code",id);
										
						options.setAttribute("dri",dri);
						options.setAttribute("url",dri);
										
						if(elemvalue == namee) {
							options.setAttribute("selected","selected");
						}						
					}										
					events.add( elem, "change", function(event) { 
							that.changeSelected(event.target);
					});
				}

			});

		} else if(sourcetype == "LiveStuff Cloud" || sourcetype == "LiveStuff Object" || sourcetype == "Enterprise Repository" ) {
			
			if(source.indexOf("http") > -1 || source.indexOf("https") > -1) {
				source	= source.replace("http://","").replace("https://","");
			} else {
				source	= source;
			}
			source		= source.substring(source.indexOf("/"), source.length);
			
			if(sourcetype == "LiveStuff Cloud" || sourcetype == "Enterprise Repository") {
				var actionname		=	"SearchItems.do";
			} else if(sourcetype == "LiveStuff Object") {
				var actionname		=	"SearchObjects.do";
			} else if(sourcetype == "URL") {
				var actionname		=	"";
			} else {
				var actionname		=	"GetItems.do";
			}
			
			if(isDropdown) {
				var actionname		=	"GetItems.do";
			}
			
			if(sourcetype == "URL") {
				source	=	source+"/"+actionname;
			} else {
				if(isDropdown) {
					source	=	source+"/"+actionname;
				} else {
					source	=	source+"/"+actionname+"?Search="+elemval;
				}
			}
			var parentnodee	=	elem.parentNode;
			var data_type		=	"Formatted Name";
			if(isDropdown) {
				optionul	=	elem;
			} else {

				if(typeof dom.getByClass("dropdown-option-div", parentnodee)[0] == "undefined") {
					/*className	:	"dropdown-option-div -border -primary -background-color -white -hidden"*/
					var optiondiv	=	dom.create("div",parentnodee,{
						className	:	"dropdown-option-div tertiary-background primary-border -hidden"
					});
					
					var optionul	=	dom.create("ul",optiondiv,{
						className	:	"dropdown-option-ul"
					});
					
					
				} else {
					dom.getByClass("dropdown-option-ul", parentnodee)[0].innerHTML = "";
					var optionul	=	dom.getByClass("dropdown-option-ul", parentnodee)[0];
					var optiondiv	=	dom.getByClass("dropdown-option-div", parentnodee)[0];
					dom.removeClass(optiondiv,"-show");
					dom.addClass(optiondiv,"-hidden")
				}
			}
			
			if(isDropdown) {
			
			} else {
				if(elem.value == "") {
					dom.remove(optiondiv);					
					return false;	
				}
				
				if(elem.value.length < 2) {
					return false;
				}
			}
			
			if(this.enviornment == "extension") {
				source	=	that.globaldomain+source;
			}
			
			RunAjaxCall.runTheHTTPRequest({
				method	:	"GET",
				url		: source
			}).then(function(data){
				data					=	JSON.parse(data.responseText);
				data					=	data["Results"] || data;				
				if(data && data != "") {
					var thisindex,thisindexfield,namee,id,dri,optionli,optionlidiv,optiondivs;
					if(isAllowEdit == "Y") {
						optionli			=	dom.create("option",optionul,{
							className	:	"dropdown-option",
							innerHTML	:	"Select Option"								
						});
					}

					for(var i=0; i<data.length; i++) {
						thisindex			=	data[i]["Object"] || data[i]["Intersection"];
						thisindexfield	=	thisindex.Fields;
						namee				=	thisindexfield["Formatted Name"];
						id					=	thisindex["Code"] || thisindex["Id"] || thisindex["Code"] || thisindex["code"];
						dri					=	thisindexfield["Direct Resource Identifier"] || data[i]["dri"] || data[i]["URL"] || data[i]["url"];
						if(isDropdown) {
							if(namee){
								/*
								if(!elemvalue) {
									var options		=	dom.create("option",elem,{
									    className	:	"dropdown-option"
									});
								}
								*/
								
								var optionli			=	dom.create("option",optionul,{
									className	:	"dropdown-option",
									innerHTML	:	namee								
								});
								if(elemvalue == namee) {
									if(options) options.setAttribute("selected","selected");
								}
							}

						} else {

							optionli			=	dom.create("li",optionul,{
								className	:	"dropdown-option-li -click "								
							});
							
							optionlidiv	=	dom.create("div",optionli,{
								className	:	"dropdown-option-li-div -click item"								
							});

						
							optiondivs	=	dom.create("div",optionlidiv,{
								className	:	"dropdown-option-li-name -click content -truncate",
								innerHTML	:	namee
							});
							
							
							
							if(i==0) {
								dom.addClass(optionli,"-selected-option");
							}
							
							optiondivs.setAttribute("id",id);
							optiondivs.setAttribute("code",id);
							
							optiondivs.setAttribute("dri",dri);
							optiondivs.setAttribute("url",dri);
							
							optiondivs.addEventListener("click", function(event){
								that.optionselected(event);
							});	
						}													

					
					}
					
					if(isDropdown) {
					} else {
						dom.toggleClass(optiondiv,"-show");
					}
					
				}
				
			});
		}
	},
	
	getTheChangeEventData:	function(evt,isdropped,eventss,fieldtype){
		var that				=	this;
		var elem				=	evt.target;
		var el = elem;  		
		if(isdropped) {							   
			if(dom.getByTag("input",elem)[0]) {
				el	=	dom.getByTag("input",elem)[0];
			} else {				
				el = dom.nextSibling(elem);			 				
				if(!el) {
					el =	that.findAncestor(elem,'.-UIContainer-row-column-upload-label');
					el	=	dom.getByTag("input",el)[0];
				}	
			}
		}
		that.initializeTheUpload(el,evt,eventss,fieldtype);	
		
		var par = that.findAncestor(el,'.-UIContainer-table-td');							
		if(par) par.setAttribute("url","");							
	},
	
	initializeTheUpload:	function(elem,eventdata,eventss,fieldtype){
		var that	=	this;
		that.handleFileSelect(elem,eventdata,eventss,fieldtype);
		
	},
	handleFileSelectFromURL: function(evt,isdropped,eventss,fieldtype){
		var that				=	this;
		var elem				=	evt.target;
		var el = elem,  		  	   
		dropurl ='',
		file = {};
		
		if(isdropped) {							   
			if(dom.getByTag("input",elem)[0]) {
				el	=	dom.getByTag("input",elem)[0];
			} else {				
				el = dom.nextSibling(elem);			 				
				if(!el) {
					el =	that.findAncestor(elem,'.-UIContainer-row-column-upload-label');
					el	=	dom.getByTag("input",el)[0];
				}	
			}
		}
		var container			=	el.parentNode ;
		var url = evt.dataTransfer.getData('text/plain');						
		if(url.indexOf("youtu.be") > 0 || url.indexOf("youtube") > 0) {			
			dropurl = url;							
		} else {
			var image = evt.dataTransfer.getData('text/html');
			if(image.indexOf('src="') >= 0 || image.indexOf("src='")  >= 0){
				var div = document.createElement('div');
				div.innerHTML = image;
				var src = div.childNodes[0].getAttribute("src");
				
				if(!src)
				src = div.childNodes[0].childNodes[0].getAttribute("src");
				if(!src)
				src = div.childNodes[0].childNodes[0].childNodes[0].getAttribute("src");
				
				if (src && src.toLowerCase().indexOf("data:") >= 0 && src.toLowerCase().indexOf("data:") >= 0) {	
					dropurl = src;					
				} else {
					dropurl = src;
				}				
			} else {										
				dropurl = url;	
			}	
		}	
		if(dropurl) {		  
			if(dropurl.indexOf("http") <= -1 && dropurl.indexOf("https") <= -1) {				
				dropurl = "http://"+dropurl;
			}
											
			var par = that.findAncestor(el,'.-UIContainer-table-td');							
			if(par) par.setAttribute("url",dropurl);	
			file.src  = dropurl;
			file.IsUrl  = true;
			file.name = dropurl.match(/.*\/(.*)$/)[1];												
			that.getAnalyzeURL(file,container);							
		}
	},
	handleFileSelect:	function(elem,params,eventss,fieldtype){
		var that					=	this;
		if(typeof FormData == "undefined") {
			var data = [];
		} else {
			var fileFormData			=	new FormData();
		}
		
	   //var fileFormData 		=	new FormData();	   
	 
	   var container			=	elem.parentNode,
			 files				=	[],
			 namefield	      =	dom.getByClass("Name",that.main_ui_container)[0];
			 catfield         = dom.getByClass("Category",that.main_ui_container)[0];
			 that.LocalFileCount	=	0,
			 fileext = "image/jpeg";
		
		var nameinput,catinput;
		if(namefield) {
			nameinput	=	dom.getByTag("input",namefield)[0];
		}
		
		if(catfield) {
			catinput	=	dom.getByTag("input",catfield)[0];
		}				
								
		if(params.dataTransfer && params.dataTransfer.files) {
			files = params.dataTransfer.files;			
		} else if(params.TrueTarget && params.TrueTarget.files) {
			files = params.TrueTarget.files;
		} else if(params.target && params.target.files) {
			files = params.target.files;
		} else if(params) {
			files[files.length] = params.src;
		} else {
			return;
		}	
		
		for (var i = 0, f; f = files[i]; i++) {
			if (!f.name)
				f.name = "Asset" + Date.now();

			fileName = f.name;
			
			var reader = new FileReader();
			reader.Area = that;
			
			reader.onload = (function(file) {
				
				return function(e) {
					if(!file.src)
						file.src = e.target.result;

					that.populateOutputForImage( file,container );
					that.populateFormData( file , eventss, params);
					
					if(fieldtype == 'Uploader') {						

						if(nameinput) {
							nameinput.value	=	file.name;
						}
										
						fileext = file.type;							
						if(fileext == "image/jpeg" || image/jpg || image/png || image/gif) {
							var catvalue = 'Photo';
							var catdri   = '/Category/Photo';
							var catid    = '831541643';							
						} else if(fileext == "video/x-ms-wmv") {
							var catvalue = 'Video';
							var catdri   = '/Category/Video';
							var catid    = '825215732';
						}/*VV: All Files should be document
						 else if(fileext == "application/pdf" || fileext == "application/vnd.openxmlformats-officedocument.wordprocessingml.document") {
							var catvalue = 'Document';
							var catdri   = '/Category/Document';
							var catid    = '832325045';
						}*/ else {
						   var catvalue = 'Photo';
						   var catdri   = '/Category/Photo';
							var catid    = '831541643';
						}
						
						catinput.value =  catvalue;
						catfield.setAttribute("id",catid);         
						catfield.setAttribute("code",catid);
						catfield.setAttribute("dri",catdri);
						catfield.setAttribute("url",catdri);
						catfield.setAttribute("field-value",catvalue);
												
					} else {							
						if(eventss) {
								that.hasformData = false;								
							} else {
								that.hasformData = true;
							}							
					}

					done = true;
				};
				
			})(f);

			reader.readAsDataURL(f);
			that.LocalFileCount++;			
		}		
	},
	
	populateFormData:	function(file,eventss,params){
		var that	=	this;										
		that.LocalFiles	=	file;
		if(eventss && eventss.length > 0 ) that.handleEvents(eventss,params);		
	},
	
	populateOutputForImage:	function(file,container){
		var that										=	this;
		var parentcontainer						=	container.parentNode;
		//parentcontainer.style.background	=	"none";
		parentcontainer.style.opacity			=	1;
		var textdiv									=	dom.getByClass("-upload-text",container)[0];
		if(textdiv) textdiv.style.display					=	"none";
		
		if(typeof dom.getByClass("-upload-text",container)[0] !== "undefined"){
			dom.remove(dom.getByClass("-upload-text",container)[0]);
		}

		if(typeof dom.getByClass("-upload-output",container)[0] !== "undefined"){
			dom.remove(dom.getByClass("-upload-output",container)[0]);
		}
				
		var span		=	dom.create("span",container,{
			className	:	"-upload-output"
		});
		
		if(fileName) {
			var ext =  file.name.substr((fileName.lastIndexOf('.') + 1));		
		} else{
		  var fileName = file.name;
			var ext =  file.name.substr((fileName.lastIndexOf('.') + 1));
		}
		
		if(file.IsUrl) { 
		
			var type = file.filetype;
			if(type == 'Image') {
				var fileicon = file.src ;				
			} else if(type =='Document') {
				var fileicon = 'Icons/document/Icon64x64.png';
			} else if( type =='File') {
				var fileicon = '/Icons/File/Icon64x64.png';
			}
						
		} else {
					
			if(ext == 'jpg' || ext == 'jpeg' || ext == 'png' || ext == 'gif' || ext == 'bmp') {
				var fileicon = file.src;
			} else {
				var fileicon = '/Icons/File/Icon64x64.png';
			}		
			
		}
		span.innerHTML = ['<figure class="-upload-figure"><span class="-figure-span"><img class="-thumb" src="', fileicon, '" title="', file.name, '" style="max-width:100%; max-height:100%; "/></span></figure>'].join('');		
		dom.append(container,span);
					
	},
	getAnalyzeURL: function (file,container) {
	 var that = this;
	 if(file.src) {
		  var _url = "/AnalyzeURL.do?URL="+encodeURIComponent(file.src);	
		   RunAjaxCall.runTheHTTPRequest({
				method	:	"get",
				url		: _url
		   }).then(function(data){		
		 		 data					=	JSON.parse(data.responseText);
		 		 if(data.Success) {
					file.filetype = data.Results;
					that.populateOutputForImage(file,container);

					var namefield	      =	dom.getByClass("Name",that.main_ui_container)[0];
					var catfield        = dom.getByClass("Category",that.main_ui_container)[0];			 
					
					var nameinput,catinput;
					if(namefield) {
						nameinput	=	dom.getByTag("input",namefield)[0];
					}
					
					if(catfield) {
						catinput	=	dom.getByTag("input",catfield)[0];
					}
					var fileext = file.filetype;
					
					if(nameinput) {
						nameinput.value	=	file.name;
					}					
					
					if(fileext == "Image") {
						var catvalue = 'Photo';
						var catdri   = '/Category/Photo';
						var catid    = '831541643';					
					} else if(fileext == "Document") {
						var catvalue = 'Document';
						var catdri   = '/Category/Document';
						var catid    = '832325045';
					} else {
					   var catvalue = 'Photo';
					   var catdri   = '/Category/Photo';
						var catid    = '831541643';
					}
					
					catinput.value =  catvalue;
					catfield.setAttribute("id",catid);         
					catfield.setAttribute("code",catid);
					catfield.setAttribute("dri",catdri);
					catfield.setAttribute("url",catdri);
					catfield.setAttribute("field-value",catvalue);
															
		 		 }	
		   }).catch(function(e){				
				that.showErrorMessage("Some error occured.");
		  });
	 }
	},		
	tallyItemCount:	function(evt){
		var that	=	this;
		var elem	=	evt.target;
		var p1	=	elem.parentNode;
		var p2	=	p1.parentNode;
		if(typeof dom.getByClass("-item-counter",p1)[0] === "undefined") {
			var counterdiv	=	dom.create("div",p1,{
				className	:	"-item-counter -right-aligned",
			});

		} else {
			var counterdiv	=	dom.getByClass("-item-counter",p1)[0];
		}
		
		var lines 			= elem.value.split(/\r*\n/);
		var newArray 		= lines .filter(function(v){return v!==''});
		var no_of_items	= newArray.length;
		if(no_of_items > 0) {
			if(no_of_items > 1) {
				var countertext	=	no_of_items+" items to add";
			} else {
				var countertext	=	no_of_items+" item to add";
			}
			counterdiv.innerHTML	=	countertext;
		} else {
			counterdiv.innerHTML	=	"";
		}
	},
	
	clearFieldData:	function(eventss,evt){
		var that		=	this;
		var elem		=	evt.target;

		/*VV:06022016: Clear field not work reasion of svg
			//var parent	=	elem.parentNode;			
		var parent	=	elem.parentNode.parentNode;		
		if(typeof dom.getByTag("input", parent)[0] != 'undefined') {
			var element		=	dom.getByTag("input", parent)[0];
			element.value	= "";
		} else if(typeof dom.getByTag("textarea", parent)[0] != 'undefined') {
			var element		=	dom.getByTag("textarea", parent)[0];
			element.value	= "";
		}
		*/	
		
		// MD: Added to run the update form events on clear the field
		if(eventss) {
			for(var i=0; i<eventss.length; i++) {
				if(eventss[i]["Event"] == "On Get Value") {
					if(eventss[i]["Action"] == "Update Form") {
						var __url			=	eventss[i]["Source"];
							 __url			=	that.replacetheBBCode( encodeURIComponent( __url ) );
						var eventaction	=	eventss[i]["Action"];	
						var formname		=	eventss[i]["Form Name"];
						var typing			=	"Y";
						if(formname) {
							 formname		=	formname.replace(/\s+/g,"-");
							 formname		=	formname.replace(/([~!@#$%^&*()_+=`{}\[\]\|\\:;'<>,.\/? ])+/g, '-');
						}
						
						var parentnodee		=	elem.parentNode;
						var errormessagediv	=	dom.getByClass("-error-msg-div",parentnodee)[0];
						if(typeof errormessagediv !== "undefined") {
							dom.remove(errormessagediv);
						}
				 		if(eventaction == "Update Form") {
				 			var container	=	dom.getByClass(formname,that.main_ui_container)[0];				 		
				 			that.runUrlAndgetresponse(__url,"N",container)
				 		} else {
						}
					}
				}
			}
		}
		
	},
	
		handleEvents:	function(eventss, evt, supports){
		var that = this;
		var onselectarr	=	[],
			 onclickarr		=	[],
			 onkeyuparr		=	[],
			 eventt,
			 eventclick,
			 eventkeyup,
			 eventselect;			 

		for(var i=0; i<eventss.length; i++) {
			eventt				=	{};
			eventclick			=	{};
			eventkeyup			=	{};
			eventselect			=	{};
			eventt.name			=	eventss[i]["Name"];
			eventt.type			=	eventss[i]["Type"];
			eventt.Event		=	eventss[i]["Event"];
			eventt.Action		=	eventss[i]["Action"];
			eventt.Source		=	eventss[i]["Source"];
			eventt.DataType		=	eventss[i]["DataType"];
			eventt.FormName		=	eventss[i]["Form Name"];
			eventt.PostParam		=	eventss[i]["Post Parameter"];
			eventt.PostRef		=	eventss[i]["Post Reference"];
			eventt.LoaderMessage		=	eventss[i]["Loader Message"];
			eventt.CompleteMessage		=	eventss[i]["Complete Message"];
			eventt.ErrorMessage		=	eventss[i]["Error Message"];			
		
			if(eventt.Event == "On Click") {
				eventclick.name		=	eventss[i]["Name"];
				eventclick.type		=	eventss[i]["Type"];
				eventclick.Event		=	eventss[i]["Event"];
				eventclick.Action		=	eventss[i]["Action"];
				eventclick.Source		=	eventss[i]["Source"];
				eventclick.DataType	=	eventss[i]["DataType"];
				eventclick.FormName	=	eventss[i]["Form Name"];
				onclickarr.push(eventclick);
			}
			
			if(eventt.Event == "On Get Value") {
				eventkeyup.name		=	eventss[i]["Name"];
				eventkeyup.type		=	eventss[i]["Type"];
				eventkeyup.Event		=	eventss[i]["Event"];
				eventkeyup.Action		=	eventss[i]["Action"];
				eventkeyup.Source		=	eventss[i]["Source"];
				eventkeyup.DataType	=	eventss[i]["DataType"];
				eventkeyup.FormName	=	eventss[i]["Form Name"];
				onkeyuparr.push(eventkeyup);
			}
			
			if(eventt.Event == "On Select") {
				eventselect.name		=	eventss[i]["Name"];
				eventselect.type		=	eventss[i]["Type"];
				eventselect.Event		=	eventss[i]["Event"];
				eventselect.Action	=	eventss[i]["Action"];
				eventselect.Source	=	eventss[i]["Source"];
				eventselect.DataType	=	eventss[i]["DataType"];
				eventselect.FormName	=	eventss[i]["Form Name"];
				onselectarr.push(eventselect);
			}
			
			if(eventt.Event == "On Click") {				
				that.handleClickEvent(eventt,evt);
			}
			
			
			if(eventt.Event == "On Get Value") {			
				that.handleKeyUpEvent(eventt, evt, supports,onselectarr);				
			}
			
			if(eventt.Event == "On Select") {
				that.handleFieldclickevent(eventt, evt);
			}
			
			if(eventt.Event == "On Blur") {
				that.handleFieldblurevent(eventt, evt);
			}

			if(eventt.Event == "On Change") {			   
				that.handleFieldChangeevent(eventt, evt);
			}			

		}
		
	},
		
	handleFieldblurevent:	function(eventt, evt){
		var that = this;
		var __url	=	eventt.Source;				
		if(evt.type == 'blur') {
			that.updateData(__url);
		}
	},
	handleFieldChangeevent:	function(eventt, evt){
		var that = this;
		var __url	=	eventt.Source;						
		if(evt.type == 'change' || evt.type == 'drop') {			
			that.updateData(__url,eventt);
		}
	},	
	handleClickEvent:	function(eventt,evt){
		var that		=	this;
		var __url	=	that.replacetheBBCode( encodeURIComponent( eventt.Source ) );
		var elem			=	evt.target;
		var inputtype	=	elem.getAttribute("data-inputtype");
		if(inputtype == "Quicksearch") {
			return false;
		} else {
			if(eventt.type == "Event" || eventt.type == "Update UI") {
				if(eventt.name == "Redirect"){
					/*VV: Add global domain using the Replacebbcode
					if(that.enviornment == "extension") {
						var _url = that.globaldomain + eventt.Source;
					} else {
						var _url = eventt.Source;
					}	
					window.location.href = _url;
					*/
				   window.open(__url,'_blank');
				} else {				
				
				 	if(eventt.Action=="Update Form") {
				 	  var formname = eventt.FormName;
					  if(formname) {
							 formname		=	formname.replace(/\s+/g,"-");
							 formname		=	formname.replace(/([~!@#$%^&*()_+=`{}\[\]\|\\:;'<>,.\/? ])+/g, '-');
						}				 	  
					 	var container	=	dom.getByClass(formname,that.main_ui_container)[0];
						that.runUrlAndgetresponse(__url,"Y",container);						
					} else {
						that.runUrlAndgetresponse(__url,"Y");
					}	
				}	
			} else if(eventt.type == "Function") {
				that.handletheEventFunctions(eventt,evt);
			} else {
				that.AddOrupdate(eventt,evt);
			}
		}
	},
	
	handletheEventFunctions:	function(eventt,evt){
		var that				=	this;
		var eventaction	=	eventt.Action;
		var formname		=	eventt.FormName;
		var elem				=	evt.target;
		if(dom.hasClass(elem,"icon")) {
		} else {
			elem	=	elem.parentNode;
		}
		
		var p1				=	dom.getByClass("More-Text",that.main_ui_container)[0];
		var p2				=	dom.getByClass("-UIContainer-row-column-text",p1)[0];
		if(formname) {
			 formname		=	formname.replace(/\s+/g,"-");
			 formname		=	formname.replace(/([~!@#$%^&*()_+=`{}\[\]\|\\:;'<>,.\/? ])+/g, '-');
		}
		var container	=	dom.getByClass(formname,that.main_ui_container)[0];
		if(eventaction) {
		

			if(eventaction == "ToggleView") {				
				dom.toggleClasses(container, ['-show', '-hidden']);				
				
				/*VV:06302016: Container are not toggle add hashclass condition	*/				
				
				if(dom.hasClass(elem,"icon")) {
					dom.toggleClass(elem,'-transform180');						
										if(typeof p2 != "undefined") {
							if(p2.innerHTML == "more") {
								p2.innerHTML = "less";
							} else {
								p2.innerHTML = "more";
							}
						}									
									
				}else{
				
					var svgicon = dom.getEl('svg',elem);					
					if(svgicon) {
						dom.toggleClass(svgicon,'-transform180');
													if(typeof p2 != "undefined") {
								if(p2.innerHTML == "more") {
									p2.innerHTML = "less";
								} else {
									p2.innerHTML = "more";
								}
							}									
											}							
				}					
			}
		}

	},
	
	updateData:	function(url,eventt){
		var that  = this,postparameter,postpreference,form;
		url = url.replace("//", "/");		
		var __url = that.replacetheBBCode( encodeURIComponent( url ) );
				
		if(eventt) {		
			postparameter		=	eventt.PostParam;
			postpreference	=	eventt.PostRef;
		}		
		
		if(postparameter && postparameter !== "") {		

			postparameter	=	decodeURIComponent( that.replacetheBBCode( encodeURIComponent( postparameter ) ) );
			postpreference	=	that.replacetheBBCode( postpreference ) ;			
						
			if(postpreference.name) {
			
				if(typeof FormData == "undefined"){
					var data = [];
				} else {
					form	=	that.fileFormData	=	new FormData();
				}
				form.append("Name",postpreference.name);
				form.append("Files",postpreference,postpreference.name);
				form.append("Category","Asset");
				form.append("Asset Type","image/jpeg");
			}							
		}
		
		RunAjaxCall.runTheHTTPRequest({		
			method	:	"POST",
			url		: __url,
			data		:	form
		}).then(function(data){
		});	 
	},
	
	AddOrupdate:	function(eventt,evt){
		var that		=	this;

		var __url	=	eventt.Source,
			 __url	=	that.replacetheBBCode( encodeURIComponent( __url ) ),
			 loadermessage		=	decodeURIComponent( that.replacetheBBCode( encodeURIComponent( eventt.LoaderMessage), true) ),
			 completemessage	=	decodeURIComponent( that.replacetheBBCode( encodeURIComponent( eventt.CompleteMessage), true) ),
			 errormessage		=	decodeURIComponent( that.replacetheBBCode( encodeURIComponent( eventt.ErrorMessage), true) );
		// console.log("__url",__url);
		// return false;					
		if(typeof dom.getByClass("-error-msg-div", that.main_ui_container)[0] !== "undefined") {
			dom.remove(dom.getByClass("-error-msg-div", that.main_ui_container)[0]);
		}
		
		if(dom.getByClass("-UIContainer-area-forms", that.main_ui_container).length >0) {
			var formslength	=	dom.getByClass("-UIContainer-area-forms", that.main_ui_container).length;
			var fieldslength	=	dom.getByClass("-UIContainer-row-column-input", that.main_ui_container).length;
			for(var i=0; i<fieldslength; i++) {
				var field			=	dom.getByClass("-UIContainer-row-column-input", that.main_ui_container)[i];
				var fieldallowblank	=	field.getAttribute("allow-blank");
				
				//VV:07072016: This condition for the File input
				if(field.getAttribute('data-inputtype') && field.getAttribute('data-inputtype') == 'Uploader') field = dom.getByTag('input',field)[0].value;				
				
				if(fieldallowblank == "N") {					
					if(field.value == "" || field.value == "Select Option") {
						that.showhideErrorMessage(field);						
						var container = field.parentNode.parentNode.parentNode.parentNode;
						if(dom.hasClass(container,"-hidden")) {
							dom.toggleClasses(container, ['-show', '-hidden']);
						}								
						field.focus();
						return false;
					}
				}
					
			}
		}				
		var postparameter		=	eventt.PostParam;
		var postpreference	=	eventt.PostRef;
		var param;
		var form;
		

		if(postparameter && postparameter !== "") {		

			postparameter	=	decodeURIComponent( that.replacetheBBCode( encodeURIComponent( postparameter ) ) );
			postpreference	=	that.replacetheBBCode( postpreference ) ;			
			
			if(postpreference.name) {
									
				//form	=	that.fileFormData	=	new FormData();			
				if(typeof FormData == "undefined"){
					var data = [];
				} else {
					form	=	that.fileFormData	=	new FormData();
				}								
				form.append("Name",postpreference.name);
				form.append("Files",postpreference,postpreference.name);
				form.append("Category","Asset");
				form.append("Asset Type","image/jpeg");
			}							
		} else if(that.hasformData) {
				if(typeof FormData == "undefined"){
					var data = [];
				} else {
					form	=	that.fileFormData	=	new FormData();
				}
				var postpreference = that.LocalFiles;
				form.append("Name",postpreference.name);
				form.append("Files",postpreference,postpreference.name);
				form.append("Category","Asset");
				form.append("Asset Type","image/jpeg");		
		}
				
				/*that.Loader("show",loadermessage);*/
		utils.showLoader({"message":loadermessage});
		RunAjaxCall.runTheHTTPRequest({		
			method	:	"POST",
			url		: __url,
			data		:	form
		}).then(function(data){
			data					=	JSON.parse(data.responseText);
			data					=	data["Results"] || data["Result"] || data;
			if(data == "false" || !data) {
				that.showErrorMessage(errormessage);
				return false;
			}
			
						var newclouddri	=	data[0]["Intersection DRI"] || data[0]["DRI"];	
			var newcloudid		=	data[0]["Code"] || data[0]["Id"] || data[0]["ID"];
			/*that.Loader("show",completemessage);*/
			utils.showLoader({"message":completemessage});
			setTimeout(function(){
				if(that.redirection == "Y") {
				
					if(that.enviornment == "extension") {				
						utils.hideLoader();
						stuffitModule.builduiparsercall();
						return false;
					} 				
					
					if(newclouddri) {
						if(that.device == 'desktop') {
							/*window.location.href		=		newclouddri;*/
							if(data[0]["Intersection DRI"]) {
								if (that.cb && typeof that.cb === 'function') {
									that.cb();
								} else {
									var cb	=	function() {
										new types.CloudBanner_Details();
										rateStar = new runGetRatingsPlugin(); /* AKosti 08/25/2016 : Added this for showing rating stars on creating the item */
									}
								}
								utils.hideLoader();
								// Mdubey : 06/16/2016 - Added to refresh the current cloud while adding item in liveplatform - checking eith Route Type - as in LivePlatform it is "Hash" and in livestuff it is null
								if(sky.routeType) {
									if(sky.routeType == "hash") {
										sky.route(sky._currentCloud.hash, sky._currentCloud,null,null);
									} else {
										sky.routeItem(newclouddri,'',cb);
									}
								} else {
									sky.routeItem(newclouddri,'',cb);
								}
							} else {
								window.location.href		=		newclouddri;
							}
						} else {
							window.location.href		=		newclouddri+"/index.mhtml";
						}
					} else {
						utils.hideLoader();
					}
					that.closePopup();
				} else {
					utils.hideLoader();
					that.closePopup();
					if (that.cb && typeof that.cb === 'function'){
						var param = {};
						param.ObjectCode = newcloudid;
						param.CloudDRI   = newclouddri;
						that.cb(param);
					}
				}
				
			},800);
			
		}).catch(function(e){
			
			that.showErrorMessage(errormessage);
			
		});	
	},
	
	showErrorMessage:	function(errormessage){
		var that	=	this;
		var cont				=	dom.getByTag("body")[0];
		utils.hideLoader();
		if(typeof dom.getByClass("-error-page",cont)[0] !== "undefined") {
			var errorpop = 			dom.getByClass("-error-page",cont)[0];
			dom.remove(errorpop); 
		}
		
		if(!errormessage) {
			errormessage	=	"Some error,Try again.";
		}
		
		var errordiv		=	dom.create("div",cont,{
			className		:	"-error-page "
		});

		var e_overlay		=	dom.create("div",errordiv,{
			className		:	"-error-overlay"
		});
		
		var e_msg			=	dom.create("div",errordiv,{
			className		:	"-error-message primary-border tertiary-background",
			innerHTML		:	errormessage
		});
		
		if(that.device == 'desktop') {
			e_msg.style.left	=	"45%";
		}
		
		setTimeout(function(){
			if(typeof dom.getByClass("-error-page",cont)[0] !== "undefined") {
				var errorpop = dom.getByClass("-error-page",cont)[0];
				dom.remove(errorpop); 
			}
		},2500);
		
	},
	
	showhideErrorMessage:	function(field){
		var that	=	this;		
		var offsettop		=	field.offsetTop;
		var offsetleft		=	field.offsetLeft;
		var fieldparent	=	field.parentNode;
		var fieldparents	=	fieldparent.parentNode;
		var fieldlabel		=	dom.getByTag("label",fieldparents)[0];
		var dataType  	=  fieldparents.getAttribute('data-datatype');
		if(typeof fieldlabel !== "undefined") {
			if(dataType == 'File') {
				var msg_text	=	fieldlabel.innerText;
			} else {
				var msg_text	=	"*Enter the "+fieldlabel.innerHTML;
			}
		} else {
			var msg_text	=	"*Enter the value";
		}
		
		var msgdiv		=	dom.create("div",fieldparent,{
			className	:	"-error-msg-div -left-aligned",
			innerHTML	:	msg_text
		});
		
	},
	
	handleKeyUpEvent:	function(eventt, evt, supports, selectarr){
		var that 		=	this;
		var elem			=	evt.target;
		var inputtype	=	elem.getAttribute("data-inputtype");
		var keycodes		=	evt.keyCode;
					
		if(keycodes) {			
			if(keycodes == 9 || keycodes == 13 || keycodes == 37 || keycodes == 38 || keycodes == 39 || keycodes == 40) {
			} else {													
				that.CreateTheSearchoptions(eventt, evt, supports,selectarr);
			}
			
			if(keycodes  == 40) {						
				var dropdownelem 			= dom.nextSibling(elem);
				var dropdowncont			=	dom.nextSibling(dropdownelem);
				var preselectedoption 	=	dom.getByClass("-selected-option",dropdowncont)[0];
				//dom.removeClass(preselectedoption,"-selected-option");
				
				var nextselectedoption 	=	dom.nextSibling(preselectedoption);
				
				//AD: 08/22/2016 Added this check if next sibling will available and also above commented line inside the if block
				if(nextselectedoption){
					dom.removeClass(preselectedoption,"-selected-option");
					dom.addClass(nextselectedoption,"-selected-option");
				}
				
				if(dom.getByClass("dropdown-option-li-name",nextselectedoption)[0]) {				
					var result					=	dom.getByClass("dropdown-option-li-name",nextselectedoption)[0].innerHTML;
				} else {
					var result					=	dom.getByClass("dropdown-option-li-name-refine",nextselectedoption)[0].innerHTML;
				}
				elem.value			=	result;								
			}

			if(keycodes  == 38) {
				var dropdownelem 			=  dom.nextSibling(elem);
				var dropdowncont			=	dom.nextSibling(dropdownelem);
				var preselectedoption 	=	dom.getByClass("-selected-option",dropdowncont)[0];
				//dom.removeClass(preselectedoption,"-selected-option");
				
				var prevselectedoption 	=	dom.previousSibling(preselectedoption);
				
				//AD: 08/22/2016 Added this check if previous sibling will available and also above commented line inside the if block
				if(prevselectedoption){
					dom.removeClass(preselectedoption,"-selected-option");
					dom.addClass(prevselectedoption ,"-selected-option");
				}
				
				if(dom.getByClass("dropdown-option-li-name",prevselectedoption)[0]) {
					var result					=	dom.getByClass("dropdown-option-li-name",prevselectedoption)[0].innerHTML;
				} else {
					var result					=	dom.getByClass("dropdown-option-li-name-refine",prevselectedoption)[0].innerHTML;
				}	
				elem.value			=	result;
			}

			if(keycodes  == 13) {   			
				var dropdownelem 			=  dom.nextSibling(elem);
				var dropdowncont			=	dom.nextSibling(dropdownelem);
				var preselectedoption 	=	dom.getByClass("-selected-option",dropdowncont)[0];

				if(dom.getByClass("dropdown-option-li-name",preselectedoption)[0]) {
					var result					=	dom.getByClass("dropdown-option-li-name",preselectedoption)[0];
				} else {
					var result					=	dom.getByClass("dropdown-option-li-name-refine",preselectedoption)[0];
				}				
				result.click();
			}
		}
	},
	
	handleFieldclickevent:	function(eventt, evt){
		var that	=	this;
		var elem			=	evt.target;
		var inputtype	=	elem.getAttribute("data-inputtype");
		if(inputtype == "Quicksearch") {
			return false;
		} else {
			that.CreateTheoptions(eventt, evt);
		}
	},
	
	CreateTheoptions:	function(eventt, evt){
		var that			=	this;
		var elem			=	evt.target;
		var inputtype	=	elem.getAttribute("data-inputtype");
		var __url		=	eventt.Source;
			 __url		=	that.replacetheBBCode( encodeURIComponent( __url ) );
		
		var elemparent	=	elem.parentNode;
		var errormessagediv	=	dom.getByClass("-error-msg-div",elemparent)[0];
		if(typeof errormessagediv !== "undefined") {
			dom.remove(errormessagediv);
		}
		
		if(inputtype == "Dropdown") {
			if(dom.getByTag("option", elem).length > 1) {
				return false;
			}
		}
		
		if(dom.hasClass(elem,"dropdown-option")) {
		} else {
							RunAjaxCall.runTheHTTPRequest({
					method	:	"GET",
					url		: __url,
					params	:	{}
				}).then(function(data){
					data		=	JSON.parse(data.responseText);
					data		=	data["Results"] || data["Result"] || data;

					if(inputtype == "Dropdown") {
						var tagtocreate	=	"option";
					} else {
						var tagtocreate	=	"li";
					}

					var namee,thisselectvalue,options;				
					for(var i=0; i<data.length; i++) {
						namee					=	data[i]["Name"];
						thisselectvalue	=	elem.value;
						if(thisselectvalue !== namee) {
							options		=	dom.create(tagtocreate,elem,{
								className	:	"dropdown-option",
								innerHTML	:	namee
							});
						}

					}
					
				});

					}
	},
	
	CreateTheSearchoptions:	function(eventt, evt, supports,selectarr){
		var that				=	this;
		var elem				=	evt.target;
		var __url			=	eventt.Source;
			 __url			=	that.replacetheBBCode( encodeURIComponent( __url ) );
		var data_type		=	eventt.DataType || "Formatted Name";
		var eventaction	=	eventt.Action;	
		var formname		=	eventt.FormName;
		var typing			=	"Y";
		if(formname) {
			 formname		=	formname.replace(/\s+/g,"-");
			 formname		=	formname.replace(/([~!@#$%^&*()_+=`{}\[\]\|\\:;'<>,.\/? ])+/g, '-');
		}
		
		var parentnodee	=	elem.parentNode;
		var errormessagediv	=	dom.getByClass("-error-msg-div",parentnodee)[0];
		if(typeof errormessagediv !== "undefined") {
			dom.remove(errormessagediv);
		}
		
 		if(eventaction == "Update Form") {
 			
 			var container	=	dom.getByClass(formname,that.main_ui_container)[0];
 			that.runUrlAndgetresponse(__url,"N",container)
 		} else {

			if(typeof dom.getByClass("-error-msg-div", that.main_ui_container)[0] !== "undefined") {
				dom.remove(dom.getByClass("-error-msg-div", that.main_ui_container)[0]);
			}
			
			if(typeof dom.getByClass("dropdown-option-div", parentnodee)[0] == "undefined") {
				
				var optiondiv	=	dom.create("div",parentnodee,{
					className	:	"dropdown-option-div tertiary-background primary-border -hidden"
				});
				
				var optionul	=	dom.create("ul",optiondiv,{
					className	:	"dropdown-option-ul"
				});
				
				
			} else {
				dom.getByClass("dropdown-option-ul", parentnodee)[0].innerHTML = "";
				var optionul	=	dom.getByClass("dropdown-option-ul", parentnodee)[0];
				var optiondiv	=	dom.getByClass("dropdown-option-div", parentnodee)[0];
				dom.removeClass(optiondiv,"-show");
				dom.addClass(optiondiv,"-hidden")
			}
			

			if(elem.value == "") {
				dom.remove(optiondiv);
				return false;	
			}
			
			if(elem.value.length < 2) {
				return false;
			}
			
			
			/*
			if(abcd){
				that.ajaxrequest.abort();
			}
			*/
			
			
			/*if(that.ajaxrequest){
				that.ajaxrequest.abort();
			}*/
			
			/*if(typing == "Y") {
				clearTimeout(ajaxtime);
			}*/
			
			/*
			clearTimeout(a);

			var a = setTimeout(function(){
				typing =	"N";
			},400);
			
			
			clearInterval(ajaxtime);
			*/
			
						/*var ajaxtime = setInterval(function(){
				if(typing == "Y")	{
				} else {*/
							
					that.ajaxrequest = RunAjaxCall.runTheHTTPRequest({
						method	:	"GET",
						url		: __url
					}).then(function(data){
						data					=	JSON.parse(data.responseText);
						data					=	data["Results"] || data;
						if(data && data != "" && data.length > 0) {
						 	var namee,id,dri,isbrowseable,optionli,optionlidiv,optiondivs,supportname,supp_events,refinebutton,fieldindex,isVM;
							for(var i=0; i<data.length; i++) {
								
							  /*VV:06282016: Add || for the get names*/					  
								if(data[i]["Object"]) {
									fieldindex			=  data[i]["Object"] || data[i]["Intersection"];
									fieldindexfield	= fieldindex["Fields"] ? fieldindex["Fields"] : fieldindex;
									isVM           	=  fieldindex["VM"];
								} else {
									fieldindex			=  data[i];
								}
								
								namee			   =	data[i][data_type] || data[i]["Plural Name"] || fieldindex["Formatted Name"] || fieldindex["Name"] || fieldindexfield["Formatted Name"]; 								
								id				   =	data[i]["ID"] || data[i]["Id"] || fieldindex["Id"] || fieldindexfield["Id"];
								dri				=	data[i]["DRI"] || data[i]["dri"] || data[i]["URL"] || data[i]["url"] || fieldindex["Direct Resource Identifier"] || fieldindexfield["DRI"];
								isbrowseable	=	data[i]["Browseable"] || "N" ;
								if(isVM) {
									isbrowseable   =  fieldindex["VM"]["Browsable"];
								}
								
								optionli			=	dom.create("li",optionul,{
									className	:	"dropdown-option-li -click "								
								});
								
								optionlidiv		=	dom.create("div",optionli,{
									className	:	"dropdown-option-li-div -click item"								
								});
								
								optiondivs		=	dom.create("div",optionlidiv,{
									className	:	"dropdown-option-li-name -click content -truncate",
									innerHTML	:	namee
								});						
												
								if(supports) {									
 									supportname	=	supports[0]["Name"];
									supp_events	=	supports[0]["Events"];										
																	
									if(isbrowseable == "Y") {
																				dom.removeClass(optiondivs, 'dropdown-option-li-name');
										dom.addClass(optiondivs, 'dropdown-option-li-name-refine');
										
										refinebutton		=	dom.create("div",optionlidiv,{
											className		:	"dropdown-option-li-refine -click primary-border",
											innerHTML		:	supportname
										});
										
										refinebutton.setAttribute("id",id);
										refinebutton.setAttribute("code",id);
										
										refinebutton.setAttribute("dri",dri);
										refinebutton.setAttribute("url",dri);
										
										events.add( refinebutton, "click", function(event) {that.createRefinedCategories(event,eventt,supp_events,selectarr,supports); });
																			}
								}
								
								if(i==0) {
									dom.addClass(optionli,"-selected-option");
								}							
								optiondivs.setAttribute("id",id);
								optiondivs.setAttribute("code",id);
								
								optiondivs.setAttribute("dri",dri);
								optiondivs.setAttribute("url",dri);
								events.add( optiondivs, 'click', function(event) { that.optionselected(event); that.CalltheOtherevents(selectarr);} );
								
																
							
							}
							dom.toggleClass(optiondiv,"-show");
							
							
						}
						
					});
			/*	}
			},30);*/
					}	
		

	},
	
		CalltheOtherevents:	function(selectarr){
		var that			=	this;
		var formexist	=	"N";
		if(selectarr.length > 0) {
			var temp	=	0;
			if(typeof selectarr[temp]["Source"] !== "undefined") {
				function runfunctioninside(temp) {
					var eventtype	=	selectarr[temp]["Action"];
					var formname	=	selectarr[temp]["FormName"];
										if(formname) {
						 formname		=	formname.replace(/\s+/g,"-");
						 formname		=	formname.replace(/([~!@#$%^&*()_+=`{}\[\]\|\\:;'<>,.\/? ])+/g, '-');
					}
										var container	=	dom.getByClass(formname,that.main_ui_container)[0];
					if(eventtype	==	"Update Form") {
						var _url		=	selectarr[temp]["Source"];
							 _url		=	that.replacetheBBCode( encodeURIComponent( _url ) );
						
						/*
						if(_url.indexOf("GetFieldsData") > -1 ) {
							_url	=	_url.replace("GetFieldsData","GetCategoryFields");
							formexist	=	"Y";
						} else if(_url.indexOf("GetFields") > -1) {
							_url	=	_url.replace("GetFields","GetCategoryFields");
							formexist	=	"Y";
						} else if(_url.indexOf("GetCategoryFields") > -1) {
							formexist	=	"Y";
						} else {
							formexist	=	"N";
						}
						*/
						
						if(_url.indexOf("GetCategoryFields") > -1) {
							formexist	=	"Y";
						} else {
							formexist	=	"N";
						}
						RunAjaxCall.runTheHTTPRequest({
							method	:	"GET",
							url		:	_url,
							params	:	{}
						}).then(function(data){
							data					=	JSON.parse(data.responseText);
							data					=	data["Results"] || data;
							if(formexist == "Y") {
								that.parseTheResponse(data,"Y",container);
							} else {
								that.parseTheResponseAndUpdateHTML(data,container);
							}
							temp++;
							if(temp<selectarr.length){
								runfunctioninside(temp);
							}
						});


					} else {
						var _url		=	selectarr[temp]["Source"];
							 _url		=	that.replacetheBBCode( encodeURIComponent( _url ) );
							 
						RunAjaxCall.runTheHTTPRequest({
							method	:	"POST",
							url		:	_url,
							params	:	{}
						}).then(function(data){
							data					=	JSON.parse(data.responseText);
							data					=	data["Results"] || data;
							
							temp++;
							if(temp<selectarr.length){
								runfunctioninside(temp);
							}
						});

					}

				}
				runfunctioninside(temp);
				
			}
		}

		
	},
		
	createRefinedCategories:	function(evt,eventt,supports,selectarr,supportss){
		var that	=	this;
		var elem	=	evt.target;
		var p1	=	elem.parentNode;
		var p2	=	p1.parentNode;

		/*VV: Update the get Parent dynamically
			var p3	=	p2.parentNode;
			var p4	=	p3.parentNode;
			var p5	=	p4.parentNode;
			var p6	=	p5.parentNode;
		*/
		var p6 = that.findAncestor(elem,'.-UIContainer-table-td');
		
		var currentcode	=	elem.getAttribute("code");
		var currentdri		=	elem.getAttribute("dri");
		
		p6.setAttribute("code",currentcode);
		p6.setAttribute("dri",currentdri);
		var _url	=	supports[0]["Source"];
			 _url	=	that.replacetheBBCode( encodeURIComponent( _url ) );
		
		var data_type	=	supports[0]["DataType"];
		if(typeof dom.getByClass("dropdown-option-div-refine", p2)[0] == "undefined") {
			var optiondiv	=	dom.create("div",p2,{
				className	:	"dropdown-option-div-refine -hidden"
			});
				
			var optionul	=	dom.create("ul",optiondiv,{
				className	:	"dropdown-option-ul"
			});
		} else {
			/*dom.getByClass("dropdown-option-ul", p2)[0].innerHTML = "";*/
			var optionul	=	dom.getByClass("dropdown-option-ul", p2)[0];
			var optiondiv	=	dom.getByClass("dropdown-option-div-refine", p2)[0];
			dom.toggleClass(optiondiv,"-show");
			return false;

		}
		
		RunAjaxCall.runTheHTTPRequest({
			method	:	"GET",
			url		: _url,
			params	:	{}
		}).then(function(data){
			data	=	JSON.parse(data.responseText);
			data	=	data["Results"] || data;			
			var namee,id,dri,isbrowseable,optionli,optionlidiv,optiondivs,supportname,supp_events,refinebutton;
			for(var i=0; i<data.length; i++) {
				namee				=	data[i][data_type] || data[i]["Plural Name"] || data[i]["Formatted Name"];
				id					=	data[i]["ID"] || data[i]["Id"] || data[i]["Code"] || data[i]["code"];
				dri				=	data[i]["DRI"] || data[i]["dri"] || data[i]["URL"] || data[i]["url"];
				isbrowseable	=	data[i]["Browseable"] || "N";
			
				if(namee) {
					optionli			=	dom.create("li",optionul,{
						className	:	"dropdown-option-li -click item"								
					});
					
					optionlidiv		=	dom.create("div",optionli,{
						className	:	"dropdown-option-li-div -click item"								
					});
				
					optiondivs		=	dom.create("div",optionlidiv,{
						className	:	"dropdown-option-li-name -click content -truncate",
						innerHTML	:	namee
					});
					
					optiondivs.setAttribute("id",id);
					optiondivs.setAttribute("code",id);
					
					optiondivs.setAttribute("dri",dri);
					optiondivs.setAttribute("url",dri);

					optiondivs.addEventListener("click", function(event){							
						that.optionselected(event,p2);
						that.CalltheOtherevents(selectarr);					
					});
										
					if(supportss) {
						supportname	=	supportss[0]["Name"];
						supp_events	=	supportss[0]["Events"];	
					
						if(isbrowseable == "Y") {
														dom.removeClass(optiondivs, 'dropdown-option-li-name');
							dom.addClass(optiondivs, 'dropdown-option-li-name-refine');
																
							refinebutton		=	dom.create("div",optionlidiv,{
								className		:	"dropdown-option-li-refine -click primary-border",
								innerHTML		:	supportname
							});
							
							refinebutton.setAttribute("id",id);
							refinebutton.setAttribute("code",id);
							
							refinebutton.setAttribute("dri",dri);
							refinebutton.setAttribute("url",dri);
							refinebutton.addEventListener("click", function(event){							
								that.createRefinedCategories(event,eventt,supp_events,selectarr,supportss);
							});
							
						}
					}
				}

			}
			dom.toggleClass(optiondiv,"-show");

		});
		
		
	},
	
	optionselected:	function(evt,maincont){
		var that						=	this;
		var elem						=	evt.target;
		var elemcode				=	elem.getAttribute("code") || elem.getAttribute("id");
		var elemdri					=	elem.getAttribute("dri") || elem.getAttribute("url");
		var elemvalue				=	elem.textContent;
		
		
		var eleparent				=	elem.parentNode;
		
		var elemparents			=	eleparent.parentNode;
				if(maincont) {
			elemparents	=	maincont;
		}
				var elemmostparent			=	elemparents.parentNode;
		var elemmostmostparent		=	elemmostparent.parentNode;
		var elemmostmostmostparent	=	elemmostmostparent.parentNode;

		/*VV: get Parent 	
		var elemmostmostmostmostparent	=	elemmostmostmostparent.parentNode;	
		*/
		
		elemmostmostmostmostparent = that.findAncestor(elem,'.-UIContainer-table-td');	

		var eleminput					=	dom.getByTag("input",elemmostmostmostmostparent)[0];
		var refinebutton				=	dom.getByClass("dropdown-option-li-refine",elemmostparent)[0];

		elemmostmostmostmostparent.setAttribute("code",elemcode);
		elemmostmostmostmostparent.setAttribute("id",elemcode);
		
		elemmostmostmostmostparent.setAttribute("dri",elemdri);
		elemmostmostmostmostparent.setAttribute("url",elemdri);
		if(dom.hasClass(elem,"dropdown-option-li-refine") ) {
		}else{
			dom.remove(elemmostparent);
			eleminput.value	=	elemvalue;			
		}
	},
	
	getparentsNodeByClass:	function(className){
		var that	=	this;
		
		var parents = [];

		var p = el.parentNode;

		while (p !== null) {
			var o = p;
			parents.push(o);
			p = o.parentNode;
		}
		var el	=	dom.getByClass(className,that.main_ui_container)[0];
		parents.filter(function(el){
			return el;
		});
		
	},
	
	replacetheBBCode:	function(encodedUrl,isText){
		var that				=	this;
		var _url 			=	decodeURIComponent( encodedUrl );
		var updatedUrl		=	_url;
		var bbCodeParam	=	[];
		var pattern			=	/(\[[\s\S]*?)(?=])/g;
		var result			=	_url.match(pattern);
		var containerID	=	that.ContIdentifier;
		if(result == '' || result == null) {
			result	=	0;
		}
		
		for (var i = 0; i < result.length; i++) {
		   bbCodeParam.push(result[i].split("[")[1]);
		}
		
		for(var v= 0; v<bbCodeParam.length; v++) {
			var _param	=	bbCodeParam[v],
					firstPeriod	=	_param.indexOf("."),
					areaName,
					sourceProperty;
				
				if(firstPeriod != -1) {
					areaName	=	_param.substr(0, firstPeriod);
					sourceProperty	=	_param.substr(firstPeriod+1, _param.length);
				} else {
					areaName	=	_param;
					sourceProperty	=	"#content";
				}
				
				if(areaName.indexOf("#") == 0) {
					areaName	=	areaName.substr(1, areaName.length);
				}
			
				var areaNameVal 	=	"";
				sourceProperty	=	sourceProperty.toLowerCase();
			
				if(sourceProperty.indexOf("#content")>-1) {
					areaNameVal = that.getValueFromClassName( areaName );
					areaNameVal	= areaNameVal;
				} else if(sourceProperty.indexOf("#encoded")>-1) {
					areaNameVal = that.getValueFromClassName( areaName );
					areaNameVal	= encodeURIComponent( areaNameVal );
				} else if(sourceProperty.indexOf("#fieldsandvalues")>-1) {
					var DTdetails			=	[],
					    DTOwneritem		=	[],
						 fieldobject,
						 formClassName		=	areaName.replace(/\s+/g,"-"),
						 formClassName		=	formClassName.replace(/([~!@#$%^&*()_+=`{}\[\]\|\\:;'<>,.\/? ])+/g, '-'),
						 formslength		=	dom.getByClass(formClassName, that.main_ui_container).length,
						 thisform,fieldslength,thisfield,thisdt,thisvalue,thisdtclass,thisfieldname,thisOldValue,  
						 keyvalueobject,thisfieldusage,thisdatatype,thisfieldtype;
					for(var i=0; i<formslength; i++) {
						thisform			=	dom.getByClass(formClassName, that.main_ui_container)[i];
						fieldslength	=	dom.getByClass("-UIContainer-table-td",thisform).length;
						for(var j=0; j<fieldslength; j++) {
							thisfield	=	dom.getByClass("-UIContainer-table-td",thisform)[j];
							// thisdt		=	thisfield.getAttribute("data-datatype") || "";
							thisdt		=	thisfield.getAttribute("field-name") || "";
							thisfieldOldValue		=	thisfield.getAttribute("field-value");
							thisfieldname		=	thisfield.getAttribute("field-name") || "";
							thisfieldusage		=	thisfield.getAttribute("data-fieldusage") || "",
							thisdatatype		=	thisfield.getAttribute("data-datatype") || "";
							
							thisdtclass	=	thisdt.replace(/\s+/g,"-");
							thisdtclass	=	thisdtclass.replace(/([~!@#$%^&*()_+=`{}\[\]\|\\:;'<>,.\/? ])+/g, '-');
							
							thisfieldtype = thisfield.getAttribute("field-type");
							
							// thisvalue	=	that.getValueFromClassName( thisdt );
							thisvalue	=	that.getValueFromClassName( thisdt );
							// thisOldValue =  that.getValueFromClassName( thisfieldOldValue);
							thisOldValue =  thisfieldOldValue;
							keyvalueobject	=	{};							
							if(thisdatatype== "" || thisdatatype== "undefined" || typeof thisdatatype== "undefined" || thisdatatype== "IsConnectedContent" || thisdatatype== "IsSharedContent" || thisdatatype== "ConnectedContent" || thisdatatype== "Owns" || thisdatatype== "Owned" ) {
							} else {
								if(thisfieldname == "undefined" || typeof thisfieldname == "undefined") {
									
								} else {									
									if(thisfieldOldValue == thisvalue) {
									}else{
										keyvalueobject.Key		=	thisfieldname;
										if(thisfieldtype == "Linked Object") {
											keyvalueobject.Destination		=	thisvalue;
										} else {
											keyvalueobject.Value		=	thisvalue;
										}
										if(thisfieldusage == "Owner" || thisfieldusage == "Item") {
											DTOwneritem.push(keyvalueobject);
										} else {
											DTdetails.push(keyvalueobject);
										}
									}	
								}
							}
							
						}
						
						if(areaName == "Owner Fields" || areaName == "Item Fields") {
							fieldobject	=	'{"Fields":'+JSON.stringify(DTOwneritem)+'}';
						} else {
							fieldobject	=	'{"Fields":'+JSON.stringify(DTdetails)+'}';
						}
					}
					
					areaNameVal					=	encodeURIComponent(fieldobject);
				} else if(sourceProperty.indexOf("#count")>-1) {
					areaNameVal	= that.LocalFileCount;
				} else if(sourceProperty.indexOf("#value")>-1) {					
					areaNameVal	= that.LocalFiles;
					
				} else if(sourceProperty.indexOf("#url")>-1) {
				
					var _tempClass	=	areaName.replace(/\s+/g,"-"),
						_tempClass	=	_tempClass.replace(/([~!@#$%^&*()_+=`{}\[\]\|\\:;'<>,.\/? ])+/g, '-')
						_tempSource	=	sourceProperty.replace(/\s+/g,"-"),
						_prop			=	dom.getByClass(_tempClass, that.main_ui_container)[0].getAttribute('url');
						areaNameVal	= encodeURIComponent(_prop);

				}else {
					var _tempClass	=	areaName.replace(/\s+/g,"-"),
						_tempClass	=	_tempClass.replace(/([~!@#$%^&*()_+=`{}\[\]\|\\:;'<>,.\/? ])+/g, '-')
						_tempSource	=	sourceProperty.replace(/\s+/g,"-"),
						_prop			=	dom.getByClass(_tempClass, that.main_ui_container)[0].getAttribute(_tempSource); 						areaNameVal	= _prop;
				}
				
				if(areaNameVal == undefined || typeof areaNameVal === 'undefined') {
					areaNameVal	=	'';
				}
				
				if(_param.indexOf("#Value")>-1) {					
					updatedUrl = areaNameVal;
				} else {
					updatedUrl = updatedUrl.replace("["+_param.trim()+"]", areaNameVal);
				}
			}
			if(that.globaldomain !== "" && !isText) {
				return that.globaldomain+updatedUrl;
			} else {
				return updatedUrl;
			}
		
		
	},
	
	getValueFromClassName:	function(className){
		var that					=	this;
			 className			=	className.replace(/\s+/g,"-");
			 className			=	className.replace(/([~!@#$%^&*()_+=`{}\[\]\|\\:;'<>,.\/? ])+/g, '-');			 		 
			 			 
		var val 					=	'';
		var containertofind	=	dom.getByClass(className, that.main_ui_container)[0];
		
		
		
		if(typeof containertofind !== "undefined") {
			var thisfieldtype   = containertofind.getAttribute("field-type") || "text";
			if(typeof dom.getByTag("input", containertofind)[0] === 'undefined') {
				
				if(dom.getByTag("textarea", containertofind).length > 0) {
				
					val	=	dom.getByTag("textarea", containertofind)[0].value;
				} else {
				
					if(typeof dom.getByTag("select", containertofind)[0] !== 'undefined') {
						val	=	dom.getByTag("select", containertofind)[0].value;
						if(val == "Select Banner View" || val == "Select Items View" || val == "Select Option") {
							val = "";
						}
					} else {
						val = "";
					}
				}
			} else {
				
				if(dom.getByTag("textarea", containertofind).length > 0) {					
					val	=	dom.getByTag("textarea", containertofind)[0].value;
				} else {
					if(dom.getByTag("input", containertofind)[0].getAttribute("type").toLowerCase() == 'checkbox') {					
						if(dom.getByTag("input", containertofind)[0].checked) {
							val	=	"Y";
						} else {
							val	=	"N";
						}
					} else if(dom.getByTag("input", containertofind)[0].getAttribute("type").toLowerCase() == 'date' || dom.getByTag("input", containertofind)[0].getAttribute("type").toLowerCase() == 'datetime-local' || dom.getByTag("input", containertofind)[0].getAttribute("data-inputtype").toLowerCase() == "Date") {
						val	=	dom.getByTag("input", containertofind)[0].value;
						if(val == "" || val == 0){
							val	=	"";
						} else {
							var formatval =  moment(new Date(val)).utc().format("YYYY-MM-DD");
							var d = Date.parse(formatval); 
							var a = moment(d).format();
							var v = utils.unixToFloat(d);
							val	=	v;
						}
					} else if(dom.getByTag("input", containertofind)[0].getAttribute("type").toLowerCase() == 'time' || dom.getByTag("input", containertofind)[0].getAttribute("data-inputtype").toLowerCase() == "Time") {
						val	=	dom.getByTag("input", containertofind)[0].value;
						if(val == "" || val == 0){
							val	=	"";
						} else {
							var a = moment().format('L');	
							var formatval =  moment(new Date(a+' '+val)).utc().format("YYYY-MM-DD h:mm:ss");						
							var d = Date.parse(formatval); 
							var v = utils.unixToFloat(d);
							val	=	v;							
						}
					} else {
						val	=	dom.getByTag("input", containertofind)[0].value;
					}
					
					/*VV:06172016: old format code 
					if(dom.getByTag("input", containertofind)[0].getAttribute("type").toLowerCase() == 'checkbox') {
						if(dom.getByTag("input", containertofind)[0].checked) {
							val	=	"Y";
						} else {
							val	=	"N";
						}
					} else if(dom.hasClass(containertofind,"date-field")) {
						var dateinputs	=	dom.getByClass("-date-input",containertofind);
						if(dateinputs[0].value == "" || dateinputs[0].value == 0) {
							val	=	"";
						} else {
							if(dom.hasClass(containertofind,"time-field")) {
								var a = "01/01/2016 "+dateinputs[0].value+":"+dateinputs[1].value+" "+dateinputs[2].value;
							} else {
								var a = dateinputs[0].value+"/"+dateinputs[1].value+"/"+dateinputs[2].value;
							}
							var d = Date.parse(a);
							var v = utils.unixToFloat(d);
							val	=	v;
 						}
					} else {
						val	=	dom.getByTag("input", containertofind)[0].value;
					}
					*/
				}
			}
		} else {
			val	=	"";
		}
		
		if(thisfieldtype == "Number" || thisfieldtype == "number") {
			if(val !== "") {
				val = parseFloat(val); 
			}
		}
		
		if(thisfieldtype == "Linked Object") {
			if(val !== "") {
				val = containertofind.getAttribute("destination");
			}
		} 
		
		if(!val) {
			val = "";
		}
		
		// Mdubey : 08/05/2016 Added below lines to add support for delimiter
		
		//console.log('Delimeter',containertofind.getAttribute("delimiter"));
		if (containertofind && containertofind.getAttribute("delimiter")) {
		  if(encodeURIComponent(containertofind.getAttribute("delimiterfor")) === "%5Cn") {
		    var spliter = "\n"; 		
		  } else {
		    var spliter = containertofind.getAttribute("delimiterfor");
		  }
		  /*VV:08302016: if spliter and delimiterreplacewith are found 
			  val = val.split(spliter).join(containertofind.getAttribute("delimiterreplacewith"));
			*/
			 var delreplacewith = containertofind.getAttribute("delimiterreplacewith");
			 if(spliter && spliter !='undefined' && delreplacewith && delreplacewith !='undefined')	val = val.split(spliter).join(delreplacewith);			  
		}

		return val;
	},
	
	AdjustMatrixaccordingToWidth:	function(){
		var that	=	this;
		
		if(dom.getEl('[form-name="Matrix Form"]') && dom.getEl('[form-name="Matrix Form"]').innerHTML != "") {
			var matcont						=	dom.getEl('[form-name="Matrix Form"]');			
			
			//VV:05202016: Add Condition for Device 
		 	if(that.device=='desktop') {
			 	var windowwidth				=	400;
		 	} else {
			 	var windowwidth				=	window.innerWidth;
		 	} 	
			var totalicons 				=	dom.getByClass("-UIContainer-table-td",matcont).length;// 4 5 6 7
			var no_of_rows					=	Math.ceil(totalicons/3); // 2 2 2 3
			var remaining_fit_icon		=	parseFloat( (no_of_rows  * 3) ); // 6 6 6 9
			var remaining_unfit_icon	=	parseFloat( remaining_fit_icon - totalicons ); // 2 1 0 2
			 
			/*MD: TO DO : Updated with new logic - but need to review  
			if(windowwidth <= 380){
				var dimension	=	90;
			}else if(windowwidth > 380 && windowwidth <=650){
				var dimension	=	110;
			}else if(windowwidth > 650){
				var dimension	=	150;
			}
			*/
			
			if(windowwidth > 420) {
				var ratio	=	0;
			} else {
				var ratio	=	24;
			}
			
			var dimension			=	parseFloat( parseFloat( windowwidth / 3 ) - parseFloat( ratio ) ) ;
			if(windowwidth > 420) {
				dimension	=	dimension - 30;
			}
			
			if(windowwidth > 520) {
				dimension	=	dimension - 5;
			}
			
			if(windowwidth == 768) {
				dimension	=	dimension - 5;
			}
			
			if(windowwidth == 1024) {
				dimension	=	dimension - 15;
			}
			
			var margintoset	=	(dimension/8) / 2;
 			
			var element;
			for(var i = 0; i < totalicons ; i++) {
				element	=	dom.getByClass("-UIContainer-table-td",matcont)[i];
				element.style.width	=	dimension+"px";	
				element.style.height	=	dimension+"px";
				element.style.margin	=	margintoset+"px"+" "+margintoset+"px"+" "+margintoset+"px"+" "+margintoset+"px";
				
				var svg = dom.getEl("svg",element);
				if(svg) {
					//VV:08172016:Comment according to new changes: dom.addClass(svg,"-inverted");	
					svg.style.width	=	"100%";	
					svg.style.height	=	"80%";
				}	
				
				if(i==totalicons-1) {
					/*VV:06012016: 
						element.style.backgroundColor =	"#95AFC8";
					*/
					if(remaining_unfit_icon > 0) {
						var lasticonwidth		=	parseFloat( (dimension * (remaining_unfit_icon + 1) ) + ( parseFloat(remaining_unfit_icon) * ( margintoset *2 )) );
						element.style.width	=	lasticonwidth+"px";
					}
				}
			}
		}
	},
	
	/*
	Loader:	function(state,message){
		var that				=	this;
		var stateclass;
		if(!state) {
			state			=	"hide";
			stateclass	=	"-hidden";
		} else if(state == "hide") {
			state			=	"hide";
			stateclass	=	"-hidden";
		} else {
			state			=	"show";
			stateclass	=	"-show";
		}
		
		var cont				=	dom.getByTag("body")[0];
		if(typeof dom.getByClass("-loader-page",cont)[0] !== "undefined") {
			dom.remove(dom.getByClass("-loader-page",cont)[0]);
		}
		
		var loaderdiv		=	dom.create("div",cont,{
			className		:	"-loader-page "+stateclass
		});

		var L_overlay		=	dom.create("div",loaderdiv,{
			className		:	"loader__overlay"
		});
		
		if(message) {
			var L_withmess		=	dom.create("section",loaderdiv,{
							className		:	"-loader-div tertiary-background primary-border"
			});
			
			var L_loader		=	dom.create("div",L_withmess,{
				className		:	"-spinner-loader"
			});
			
			var L_loader_mes		=	dom.create("div",L_withmess,{
				className		:	"-loader-message",
				innerHTML		:	decodeURIComponent(message)
			});
		} else {
			var L_withmess		=	dom.create("div",loaderdiv,{
				className		:	"loader__spinner"
			});
		}

	},
	*/
	
	CreateMapandStaticFields: function(elem){
		var that 	= this,
		elemparent	=	elem.parentNode.parentNode,
		mapdiv 		= dom.getByClass('map_canvas_address'),				
		elemVal		= elem.value;		
		if ( mapdiv.length > 0 ) {
		
				dom.removeClass(mapdiv[0],'-hidden');
				dom.addClass(mapdiv[0],'-show');			
			
		} else {
			var addressmap   = dom.create('div', elemparent, {
											className			: 'Row clear map_canvas_address -show',
											'id'					: 'map_canvas_address'
										});
			events.add(elem, 'keyup', function(event) {
				var elemvalue	=	this.value;				
				if(elemvalue.length > 5){
					var keycode	=	event.keyCode;
					if(keycode	== 13 || keycode == 37 || keycode == 38 || keycode == 39 || keycode == 40 || keycode == 9){
					}else{
						navigator.geolocation.getCurrentPosition(function (position) {
								that.codeAddress(this);
							},function (e){								
								that.codeAddress(elemvalue);								
						});		
					}
				}				
			});																		
		}
		
		if(elemVal == "") {	
			if (navigator.geolocation) {
				navigator.geolocation.getCurrentPosition(function (position) {
						latitude	 =   position.coords.latitude;
						longitude =  position.coords.longitude;							
						that.GetCurrentLocation(latitude,longitude,elem);					
						that.UpdateAddress(latitude,longitude,elem ); 
						
				},function (e){					
					/*VV:To Do
					that.showErrorMessage('Please switch to secure url to load the map');										
	 				var mapelem = dom.getByClass('map_canvas_address');
					if(mapelem.length > 0) {
						dom.removeClass(mapelem[0],'-show');
						dom.addClass(mapelem[0],'-hidden');
					}
					*/					
					latitude	  =  39.992;//position.coords.latitude;
					longitude  =  -100.374;//position.coords.longitude;							
					that.GetCurrentLocation(latitude,longitude,elem);					
					that.UpdateAddress(latitude,longitude,elem );					
				});			
			}	
		} else {			
		}
	},
	
	GetCurrentLocation: function(latitude,longitude,elem){
		var that = this;		
		//var map, marker;
		geocoder 		= new google.maps.Geocoder();			
		var position 	= new google.maps.LatLng(latitude, longitude);
		var myOptions 	= {
			zoom		: 13,
			center	: position
		};
		map = new google.maps.Map(document.getElementById("map_canvas_address"), myOptions);
		var location = new google.maps.LatLng(latitude, longitude);
		
		map.setCenter(location);
		marker = new google.maps.Marker({
			position		: location,
			map			: map,
			draggable	: true,
			title			: "Your location! Drag the marker to the desired location"			
		});
			
		google.maps.event.addListener(marker, 'dragend', function() {				
			that.UpdateAddress(this.position.lat(), this.position.lng(),elem);
			infowindow.close();
		});
		var infowindow = new google.maps.InfoWindow({
	  		content:"Your location! Drag the marker to the desired location"
	 	});
		google.maps.event.addListener(marker, 'click', function() {
			 infowindow.setContent(elem.value);
			 infowindow.open(map,marker);
		});
					
		map.addListener('click', function(event){              
		marker.setPosition(event.latLng);
			that.UpdateAddress(event.latLng.lat(), event.latLng.lng(),elem); 
		   infowindow.close();
		});		   		
	},	
	codeAddress : function (elem) {
			var that = this;
			marker.setMap(null);
			var address = elem.value;
			if(!address) address = elem;
			geocoder.geocode( { 'address': address}, function(results, status) {
				if (status == google.maps.GeocoderStatus.OK) {
					map.setCenter(results[0].geometry.location);
					marker = new google.maps.Marker({
						map			: 	map,
						position		: 	results[0].geometry.location,
						draggable	:	true,
						title			:	"Your location! Drag the marker to the desired location"
					});
					
					var location	=	results[0].geometry.location;
					
					google.maps.event.addListener(marker, 'dragend', function() {						
						that.UpdateAddress(this.position.lat(), this.position.lng(),elem); 
						infowindow.close();
					});
					
					var infowindow = new google.maps.InfoWindow({
  						content:elem.value
 					});
					google.maps.event.addListener(marker, 'click', function() {
						 infowindow.setContent(address);
						 infowindow.open(map,marker);
					});
					
				} else {					
				}
			});
	},		
	UpdateAddress : function(lat,lng,elem){
		var that = this;
		var latlng = new google.maps.LatLng(lat, lng);
            var geocoder = geocoder = new google.maps.Geocoder();
            geocoder.geocode({ 'latLng': latlng }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                       elem.value	= results[0].formatted_address;
                    }else if(results[1]){
                    		elem.val = results[1].formatted_address;
                    }
                }
       });
	},
	/*VV:06292016: Select option selected */
	changeSelected: function(elem) {
	
		var that = this;			
		var elemcode  = elem.options[elem.selectedIndex].getAttribute('code') || elem.options[elem.selectedIndex].getAttribute('id');
		var elemdri   = elem.options[elem.selectedIndex].getAttribute('dri') || elem.options[elem.selectedIndex].getAttribute('url');
		var eleparent = elem.parentNode;
		var elemmostmostmostmostparent = eleparent.parentNode;
		
		elemmostmostmostmostparent.setAttribute("code",elemcode);
		elemmostmostmostmostparent.setAttribute("id",elemcode);
		
		elemmostmostmostmostparent.setAttribute("dri",elemdri);
		elemmostmostmostmostparent.setAttribute("url",elemdri);				
	},
	/*VV: get parent node according to the child */
	findAncestor: function(el, sel) {
    var that = this;
    /*VV:Old Working code but for the IE it is update
    	while ((el = el.parentElement) && !((el.matches || el.matchesSelector).call(el,sel)));
    	return el;
    */
	    var all = document.querySelectorAll(sel);
	    var cur = el.parentNode;
	    while(cur && !that.collectionHas(all, cur)) {
	        cur = cur.parentNode;
	    }
	    return cur;
	},
	collectionHas: function(a, b) {
	    for(var i = 0, len = a.length; i < len; i ++) {
	        if(a[i] == b) return true;
	    }
	    return false;
	} 		
};	