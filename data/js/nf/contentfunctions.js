var obj = {}, EventHandalerLogo, EventHandlarUserLogin, EventHandlarverContainer,EventHandlarSelectverContainer ;
var stuffitModule = {
				init : function(_url,currentTabTitle,domain,fVersion,fVertype,uname,udri,code){
					var that = this;					 								 
					obj.name 		= uname;
					obj.dri  		= udri;
					obj.logindri	= udri;		
					obj.domain 		= domain;
					obj.title  		= currentTabTitle;
					obj.url    		= _url;
					obj.fver   		= fVersion;
					obj.fverType 	= fVertype;
					obj.code   		= code;
					
					that.builduiparsercall();
					
					//VV: attach events 											
					var uhlogo 		= document.getElementsByClassName("-UIContainer-header-logo")[0];
					EventHandalerLogo = function(evt) {
						var cogdiv = document.getElementsByClassName('cogoptions')[0];						
						cogdiv.style.display = cogdiv.style.display == "none" ? "block" : "none";
					}					
					uhlogo.removeEventListener("click", EventHandalerLogo ,false);					
					uhlogo.addEventListener("click", EventHandalerLogo ,false);
					
					var uhClose = document.getElementsByClassName("-UIContainer-close")[0];
					uhClose.addEventListener("click", function(evt) {
						var stuffit = document.getElementById("-ui-parser");	
						if(stuffit) stuffit.parentNode.removeChild(stuffit);						
					} ,false);
					
					var logoutbtn = document.getElementById("LogoutButton");
					logoutbtn.addEventListener("click", function(evt) {
						that.logout();
					} ,false);
					
					if(obj.fver.length==7){
						var pointver = obj.fver.substring(0,3)+"."+obj.fver.substring(3,4)+"."+obj.fver.substring(4,7);							
					}else{
						var pointver = obj.fver.substring(0,3)+"."+obj.fver.substring(3,5)+"."+obj.fver.substring(5,8);
					}					
					
					//VV: user selectd vesrion and name
					var version = document.getElementById("version");
					version.innerHTML = obj.fverType+": "+pointver;
					
					that.setusername(obj.name);
					
					//VV: get version detail
					that.getAllVersions();
					
					//VV: open entity
					EventHandlarUserLogin = function(evt) {	that.mopenEntities();}
					var lguser = document.getElementsByClassName("loggedUser-Login-user")[0];
					lguser.removeEventListener("click",EventHandlarUserLogin,false);
					lguser.addEventListener("click",EventHandlarUserLogin,false);					
				},				
				setusername: function(name){
					var lguser = document.getElementsByClassName("loggedUser-Login-user")[0];
					lguser.innerHTML = name;					
				},
				popupshow : function(){										
					var stuffit = document.getElementsByClassName("stuffit")[0];
					if(stuffit) stuffit.style.display = "block";					
				},
				popuphide : function(){					
					var uhlogo 		= document.getElementsByClassName("-UIContainer-header-logo")[0];												
					uhlogo.removeEventListener("click",EventHandalerLogo,false);
					
					var stuffit = document.getElementById("-ui-parser");	
					if(stuffit) stuffit.parentNode.removeChild(stuffit);
				},				
				logout : function (){
					var that = this;					
					utils.showLoader();					
					var _url = "https://" + obj.domain +"/logout.do";
					ajax.get({
						url: _url,
						params:{}
					}).then(function(result){						
						var data = JSON.parse(result.responseText);													
						if(data.Success) {							
							
							var uhlogo 		= document.getElementsByClassName("-UIContainer-header-logo")[0];														
							uhlogo.removeEventListener("click",EventHandalerLogo,false);
							
							var lguser = document.getElementsByClassName("loggedUser-Login-user")[0];
							lguser.removeEventListener("click", EventHandlarUserLogin ,false);							
							
							var coogoption = document.getElementsByClassName("loggedUser-Login-user")[0];
							coogoption.style.display = "none";							
							that.popuphide();
							chrome.runtime.sendMessage({versions:"logout"}, function(response) {								
							});								
						}
					});
				},
				getAllVersions: function() {		
				    var that = this; 				
					var _url = "https://" + obj.domain + "/GetReleases.do";					
					var str = "";
					ajax.get({
						url: _url												
					}).then(function(result){
						var data = JSON.parse(result.responseText);													
						var response 	= data["Results"];													
						if(response !='') {
							for(var m = 0; m < response.length; m++){
									var thisver  	= response[m]["Version"],
										thisfver 	= response[m]["LivePlatform Version"],
										thisname 	= response[m]["Name"],
										thisid	 	= response[m]["Id"],
										thisdomain 	= response[m]["PrimaryDomain"];
										if(typeof response[m]["Name"] === 'undefined' || thisname == '' || thisname == null){
											thisname = response[m]["Environment"];
										}				
									str += '<div class="select_version" ver-type="'+thisname+'" ver-val="'+thisver+'" fver-val="'+thisfver+'" ver-domain="'+thisdomain+'">'+thisname+' : '+thisver+'</div>';					
								}
 								
								var other_versions = document.getElementsByClassName("other_versions")[0];			
								other_versions.innerHTML = str;
								
								var current_version = document.getElementsByClassName("current_version")[0];								
								EventHandlarverContainer = function(evt) {
									other_versions.style.display = other_versions.style.display == "none" ? "block" : "none";
								} 
								current_version.addEventListener("click", EventHandlarverContainer ,false);
								
 								EventHandlarSelectverContainer = function(evt){									
									current_version.click();
									utils.showLoader();
									var cogimage = document.getElementsByClassName("cogimage")[0];
									if(cogimage) cogimage.click();
									var verselected 	= fVersion = this.getAttribute("ver-val"),
										fverselected 	= fLivePlatformVersion = this.getAttribute("fver-val"),
										vertype 		= this.getAttribute("ver-type"),
										verdomain 		= this.getAttribute("ver-domain");						
										
									if(verselected.length==7){
										var pointver = verselected.substring(0,3)+"."+verselected.substring(3,4)+"."+verselected.substring(4,7);							
									}else{
										var pointver = verselected.substring(0,3)+"."+verselected.substring(3,5)+"."+verselected.substring(5,8);
									}									
									var vercontainer = document.getElementById("version");					
									vercontainer.textContent = vertype+": "+pointver;				 									
									obj.domain = verdomain;
									
									var cogoptions = document.getElementsByClassName("cogoptions")[0];
									cogoptions.style.display = "none";
									
									var vers = verselected+"~"+fverselected+"~"+vertype+"~"+verdomain;																						
									that.getSessionInfo(vers);
									
								}								
								var select_version = document.getElementsByClassName("select_version");
								for (i = 0; i < select_version.length; i++) {									
									select_version[i].addEventListener("click", EventHandlarSelectverContainer, false);
								}																
						}	
					});	 
				},
				getSessionInfo : function(vers) {			
					var that = this;
					ajax.get({
					  url: "https://"+obj.domain+'/GetSessionInfo.do',
					}).then(function(xhr) {
					  var data		= JSON.parse(xhr.responseText),login;
					  login 		= data.Results[0].session.user;
					  if(login)	{		
						chrome.runtime.sendMessage({versions:vers}, function(response) {								
						});							
						that.builduiparsercall();
					  }else{					
						chrome.runtime.sendMessage({versions:vers}, function(response) {								
						});	
						chrome.runtime.sendMessage({versions: 'openlogin'}, function(response) {								
						}); 
						that.popuphide();
					  }	 
						utils.hideLoader();						 																	
					});					
				},
				builduiparsercall : function() {					
					var that = this;
					var containers  		= 	document.getElementsByClassName('createhtml')[0];																		 
					if(containers) containers.innerHTML = '';
					utils.showLoader();					
					newUrl	 =	new BuildUIParser({"url":obj.dri+obj.url,"container":containers,"device": 'Chrome',"Header":"N","enviornment":"extension","title":obj.title,"globaldomain":"https://"+obj.domain});												
					that.popupshow();					
				},
				mopenEntities: function() {					
						var that = this;
						var oids = 'owner-Entities';
						if(dom.getById(oids)){
							dom.remove(dom.getById(oids));
							dom.getByClass('stuffit')[0].style.overflow = "auto";
							var uicontainer = dom.getById('-UIContainer');
							if(uicontainer) dom.removeClass(uicontainer,'-hide');
							if(uicontainer) dom.addClass(uicontainer,'-show');									
						}else{		
							var parentDri = "https://"+obj.domain+obj.logindri;
							var _url = parentDri+"/GetManagedObjects.do";						
							ajax.get({
									url: _url,
									params:{}
								}).then(function(data){			
									var result  = JSON.parse(data.responseText);
										result   =	result["Results"];
									if(result!=0){
										var menucontainerele = dom.create("div",'',{class:"owner-container"});
										menucontainerele.style.overflow = "auto";
									
											var ownerdiv = dom.create("div",menucontainerele,{class:"owner-container"});
																														
											var activediv = dom.create("div",ownerdiv);
												activediv.className = "active-entity primary-color clear";
												dom.addText(activediv,"Active Entity");
																				
											var manucontainerdiv = dom.create("div",ownerdiv);
												manucontainerdiv.className	= "menuContainer primary-color";
																											
											
											var ownermenucontainer = dom.create("div",manucontainerdiv,{id:"ownermenu-content"});
												
												ownermenucontainer.className = "menuContent menuListItem itemDRI="+obj.dri;
												ownermenucontainer.setAttribute("UserId",obj.code);
												ownermenucontainer.addEventListener("click", function(){
													var thisid	= this.getAttribute("UserId");
													that.managedLoginAs(thisid);
												});
															
												var homeimage = dom.create("img",ownermenucontainer,{src:"https://"+obj.domain+"/Icons/Home/Icon32x32.png"});
												homeimage.className = "owner-home left";	
																	
												var spannameowner = dom.create("span",ownermenucontainer,{id:'listItemName'});
												spannameowner.className = "listItemName primary-color";									
											
												dom.addText(spannameowner,obj.name);
												
												var externalimage = dom.create("img",ownermenucontainer,{src:"https://"+obj.domain+"/Icons/ExternalLink/Icon32x32.png"});
												externalimage.className = "owner-expaneto-move right";
											
											var change = "0";
										for(i=0;i<result.length;i++){
												if(result[i]['Account User HierarchyPosition'] != ""){
													var userId="";
													var dri="";
													var accountHpos="";
													userId = result[i]['Account Id'];
													dri = result[i]['Account URL'];
													accountHpos = "'"+result[i]['Account HierarchyPosition']+"'";													
													if(obj.code==userId){
													}else{
														var mainlistdiv = dom.create("div",menucontainerele,{id:"otherentity"});
															mainlistdiv.className = "other-entity clear";

														if(change=="0"){
															var para = dom.create("div",mainlistdiv,{id:"changed-entity"});
																para.className = "change-entity primary-color clear";
																dom.addText(para,"Change Entity to");
															change++;
														}									
														var manucontainer = dom.create("div",mainlistdiv);
															manucontainer.className = "menuContainer primary-color";
															
														var othermenucontainer = dom.create("div",mainlistdiv,{id:"othermenu-content"});
															othermenucontainer.className = "menuContent menuListItem itemDRI="+dri;
															othermenucontainer.setAttribute("UserId",userId);
															othermenucontainer.addEventListener("click", function(){
																var thisid	= this.getAttribute("UserId");
																that.managedLoginAs(thisid);
															});
															
														var otherimage = dom.create("img",othermenucontainer,{src:"https://"+obj.domain+"/Icons/User/Icon32x32.png"});
															otherimage.className = "other-home left";
																								
														var spanname = dom.create("span",othermenucontainer,{id:"listItemName"});
															spanname.className = "listItemName primary-color";
															dom.addText(spanname,result[i]["Account Name"]);
													
													}							
												}
										}					
									  that._mCommonPopup(oids,menucontainerele);																			}			
								});	
						}		
				},
				_mCommonPopup: function(id,content,showheader) {							
						var parentelem = document.getElementsByClassName('createhtml')[0];
						var uicontainer = dom.getById('-UIContainer');
						dom.getByClass('stuffit')[0].style.overflow = "hidden";
						if(uicontainer) dom.removeClass(uicontainer,'-show');
						if(uicontainer) dom.addClass(uicontainer,'-hide');
						parentelem.style.overflow = "hidden";						
						var elemex = dom.getById(id);
						if(elemex) dom.remove(elemex);
						
						var commonpopup = dom.create('div', parentelem, {
							className: 'tertiary-background user-accounts-menu userAccountdisplaynone  slide-up cmnp -common-mobile-popup grid',
							id:id
						});
						dom.append(commonpopup , content);
						if(showheader == "N"){
							commonpopup.style.top	=	"0px";
						}
						var vh = 500;						
						//commonpopup.style.height = (vh)+"px";
						commonpopup.children[0].style.width  =  "100%";
						//commonpopup.children[0].style.height = (vh)+"px";						
				},
				managedLoginAs: function(userid){
					var that = this;
					if(!userid)
						  return;
					  
					   if(obj.code==userid){
							window.location = DRI+"/index.mhtml";	
					   }else {
							var _url = "https://"+obj.domain+"/LoginAs.do?userid="+userid; 
							ajax.get({
								url: _url,
								params:{}
							}).then(function(data){								
								var result  = JSON.parse(data.responseText);								
								if(result['Result']){
											obj.dri  = '/'+result['Result']["Direct Resource Identifier"];
											obj.name =  result['Result']["Formatted Name"];
											obj.code =  result['Result']["ObjectCode"];
											that.setusername(obj.name);
											that.builduiparsercall();
										  }
							});	
						}
				}				
}  